﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MobileMockupXAML
{
    /// <summary>
    /// C'est un classe d'interface.
    /// Définir les méthodes sans implémentation.
    /// Dans cet exemple, on définit la méthode DisplayMessage.
    /// Dans iOS, Android, Windows Phone, il faut créer une classe
    /// qui implémente cette interface.
    /// Voir exemple fichier Actions.cs dans le projet MobileMockupXAML.Droid
    /// </summary>
    public interface ActionInterface
    {
        /// <summary>
        /// Methode pour démontrer l'interface pour cette méthode
        /// implémentée sur iOS et Android
        /// </summary>
        /// <param name="msg"></param>
        void DisplayMessage(string msg);
        Task<JsonValue> SendMessage(string url);
    }
}
