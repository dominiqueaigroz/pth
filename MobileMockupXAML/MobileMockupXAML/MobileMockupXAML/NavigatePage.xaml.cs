﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MobileMockupXAML
{
    public partial class NavigatePage : ContentPage
    {
        public NavigatePage()
        {
            InitializeComponent();
        }
        void OnQRCodeButtonClicked(object sender, EventArgs e)
        {
            //DisplayAlert("QRCode", "Vous allez être déplacé à QRCode", "OK");
            Navigation.PushAsync(new QRCode());
        }
        void OnProjetButtonClicked(object sender, EventArgs e)
        {
            //DisplayAlert("QRCode", "Vous allez être déplacé à QRCode", "OK");
            Navigation.PushAsync(new Projet());
        }
        void OnNotificationsButtonClicked(object sender, EventArgs e)
        {
            //DisplayAlert("QRCode", "Vous allez être déplacé à QRCode", "OK");
            Navigation.PushAsync(new Notifications());
        }
        void OnProfilButtonClicked(object sender, EventArgs e)
        {
            //DisplayAlert("QRCode", "Vous allez être déplacé à QRCode", "OK");
            Navigation.PushAsync(new Profil());
        }
        void onLogoutButtonClicked(object sender, EventArgs e)
        {
            DisplayAlert("QRCode", "Vous vous êtes bien déconnecté", "OK");
            Application.Current.MainPage = new MainPage();
        }
    }
}
