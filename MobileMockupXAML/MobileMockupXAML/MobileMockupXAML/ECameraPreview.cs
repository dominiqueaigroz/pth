﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MobileMockupXAML
{
    /// <summary>
    /// Cette classe sert de controle custom pour afficher le contenu de la caméra
    /// @aee https://developer.xamarin.com/guides/xamarin-forms/application-fundamentals/custom-renderer/view/
    /// </summary>
    public class ECameraPreview : View
    {
        /// <summary>
        /// On crée une propriété qui permet de capturer en utilisant la caméra de devant ou derrière.
        /// Par défaut, on utilise celle de derrière.
        /// </summary>
        public static readonly BindableProperty CameraProperty = BindableProperty.Create(
        propertyName: "CameraRear",
        returnType: typeof(bool),
        declaringType: typeof(bool),
        defaultValue: true);

        public bool CameraRear
        {
            get { return (bool)GetValue(CameraProperty); }
            set { SetValue(CameraProperty, value); }
        }
    }
}
*/