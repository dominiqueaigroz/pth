﻿using JavaScriptCore;
using System;
using System.Diagnostics;
using System.IO;
using System.Json;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using Xamarin.Forms;

namespace MobileMockupXAML
{
    /// <summary>
    /// Cette classe
    /// </summary>
    public partial class MainPage : ContentPage
    {
        XElement conf;
        ActionInterface actions = DependencyService.Get<ActionInterface>();

        public MainPage()
        {
            InitializeComponent();
            conf = getConf();
        }
        void OnTestButtonClicked(object sender, EventArgs e)
        {
            if (actions != null)
                actions.DisplayMessage("Hello !!!");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">L'objet initiateur de l'événement</param>
        /// <param name="e">Contient des infos sur l'évément</param>
        async void OnConnectionButtonClicked(object sender, EventArgs e)
        {
            string email = Email.Text;
            string password = Password.Text;
            //Regex pour vérifier que l'adresse email soit valide

            Regex regexEmail = new Regex(@"^(([^<>()[\]\\.,;:\s@\""]+(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$");
            if (regexEmail.Match(email).Success && password.Length > 3)
            {
                lblTest.IsVisible = true;
                lblTest.Text = "Email:" + Email.Text + " Mdp:" + Password.Text;
                //Application.Current.MainPage = new NavigationPage(new QRCode());
                lblLogErreur.IsVisible = false;
                string url = "http://" + conf.Element("serverIP").Value + conf.Element("testPageURL").Value + email;
                try
                {
                    JsonValue json = await actions.SendMessage(url);
                    Debug.WriteLine(json["Data"].ToString());
                }
                catch (WebException ex)
                {
                    
                    Debug.WriteLine("Response: {0}", ex.Message);
                }
            }
            else
            {
                lblTest.IsVisible = true;
                lblLogErreur.IsVisible = true;
            }
        }
        private XElement getConf()
        {
            XDocument configuration;
            var type = this.GetType();
            var resource = type.Namespace + ".config.xml";
            var assembly = type.GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(resource);
            StreamReader reader = new StreamReader(stream);
            configuration = XDocument.Parse(reader.ReadToEnd());
            return configuration.Element("config");
        }
    }

}
