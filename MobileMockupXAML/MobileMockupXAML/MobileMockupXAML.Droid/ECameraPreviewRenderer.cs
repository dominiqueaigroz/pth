﻿/*using System;
using Android.Hardware;
using MobileMockupXAML.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;


[assembly: ExportRenderer(typeof(MobileMockupXAML.ECameraPreview), typeof(ECameraPreviewRenderer))]
namespace MobileMockupXAML.Droid
{
    /// <summary>
    /// Cette class implémente une Texture View.
    /// Elle permet d'encapsuler la camera dans un TextureView
    /// @see https://github.com/xamarin/xamarin-forms-samples/blob/master/CustomRenderers/View/Droid/CameraPreviewRenderer.cs
    /// </summary>    
    public class ECameraPreviewRenderer : ViewRenderer<MobileMockupXAML.ECameraPreview, MobileMockupXAML.Droid.ECameraPreview>
    {
        ECameraPreview cameraPreview;

        protected override void OnElementChanged(ElementChangedEventArgs<MobileMockupXAML.ECameraPreview> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                cameraPreview = new ECameraPreview(Context);
                SetNativeControl(cameraPreview);
            }

            if (e.OldElement != null)
            {
                // Unsubscribe
                cameraPreview.Click -= OnCameraPreviewClicked;
            }
            if (e.NewElement != null)
            {
                try
                {
                    Control.Preview = Camera.Open((e.NewElement.CameraRear) ? 1 : 0);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(@"			ERROR: ", ex.Message);
                }

                // Subscribe
                cameraPreview.Click += OnCameraPreviewClicked;
            }
        }

        void OnCameraPreviewClicked(object sender, EventArgs e)
        {
            if (cameraPreview.IsPreviewing)
            {
                cameraPreview.Preview.StopPreview();
                cameraPreview.IsPreviewing = false;
            }
            else
            {
                cameraPreview.Preview.StartPreview();
                cameraPreview.IsPreviewing = true;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Control.Preview.Release();
            }
            base.Dispose(disposing);

        }
    } // #end class
} // #end namespace*/