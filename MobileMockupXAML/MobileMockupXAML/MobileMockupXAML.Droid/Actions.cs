using Xamarin.Forms;
using MobileMockupXAML.Droid;
using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Net;
using System.IO;
using System.Json;
using System.Threading.Tasks;

/// Cette ligne enregistre la classe Actions qui impl�mente les m�thodes de l'interface au sein de DependencyService
/// Ce qui permet ensuite de r�cup�rer, en fonction du syst�me cibl� (iOS, Android, Windows Phone)
/// Ie.
/// var actions = DependencyService.Get<ActionInterface>();
/// if (actions != null)
///     actions.DisplayMessage("Hello !!!");
///     
/// Sans cette ligne, le syst�me ne peut retrouver cette classe pour Android
[assembly: Dependency(typeof(Actions))]
namespace MobileMockupXAML.Droid
{
    /// <summary>
    /// Cette classe impl�mente les m�thodes d�finies de l'interface ActionInterface
    /// </summary>
    public class Actions : ActionInterface
    {
        public void DisplayMessage(string msg)
        {

        }

        public void Connection()
        {
            //string queryString = String.Format("https://www.google.com/accounts/ClientLogin?accountType=HOSTED_OR_GOOGLE&Email={0}&Passwd={1}&service=cloudprint&source={2}", UserName, Password, Source);
            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(queryString);

            //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            //string responseContent = new StreamReader(response.GetResponseStream()).ReadToEnd();

            //string[] split = responseContent.Split('\n');
            //foreach (string s in split)
            //{
            //    string[] nvsplit = s.Split('=');
            //    if (nvsplit.Length == 2)
            //    {
            //        if (nvsplit[0] == "Auth")
            //        {
            //            authCode = nvsplit[1];
            //            result = true;
            //        }
            //    }
            //}

            //return result;
        }

        public async Task<JsonValue> SendMessage(string url)
        {
            // Create an HTTP web request using the URL:
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = "GET";

            try
            {
                // Send the request to the server and wait for the response:
                WebResponse response = await request.GetResponseAsync();
                Stream stream = response.GetResponseStream();
                // Use this stream to build a JSON document object:
                JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                //Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());
                // Return the JSON document:
                return jsonDoc;
            }
            catch (WebException e)
            {
                Console.Out.WriteLine("Response: {0}", e.Message);
                throw;
            }
        }

    }
}