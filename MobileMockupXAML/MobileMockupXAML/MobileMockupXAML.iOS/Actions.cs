using MobileMockupXAML.iOS;
using System.Json;
using Foundation;
using UIKit;
using System.Threading.Tasks;
using Xamarin.Forms;

/// Cette ligne enregistre la classe Actions qui impl�mente les m�thodes de l'interface au sein de DependencyService
/// Ce qui permet ensuite de r�cup�rer, en fonction du syst�me cibl� (iOS, Android, Windows Phone)
/// Ie.
/// var actions = DependencyService.Get<ActionInterface>();
/// if (actions != null)
///     actions.DisplayMessage("Hello !!!");
///     
/// Sans cette ligne, le syst�me ne peut retrouver cette classe pour Android
[assembly: Dependency(typeof(Actions))]
namespace MobileMockupXAML.iOS
{
    /// <summary>
    /// Cette classe impl�mente les m�thodes d�finies de l'interface ActionInterface
    /// </summary>
    class Actions : ActionInterface
    {
        public void DisplayMessage(string msg)
        {

        }
        
        public Task<JsonValue> SendMessage(string url)
        {
            return null;
        }
    }
}