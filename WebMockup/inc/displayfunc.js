/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @brief Affiche un message 
 * @param id    L'identifiant du tag pour afficher le message
 * @param msg   Le message
 * @param tm    Le temps que le message reste affiché en seconde
 */
function displayMessage(id, msg, tm = 10){
    $('#'+id).html(msg);
    // Vide la zone après le temps tm
    window.setTimeout(function(){
      $('#'+id).html("");  
    }, tm*1000);
}

/**
 * @brief Affiche le message d'erreur correspondant au code
 * @param id    L'identifiant du tag pour afficher le message
 * @param code  Le code d'erreur
 * @param tm    Le temps que le message reste affiché
 */
function displayErrorMessage(id, code, tm = 10){
    switch (code) {
    case RCA_SUCCESS: break;
    case RCA_INVALIDPARAM:
        displayMessage(id, "Paramètre invalide", tm);
        break;
    case RCA_DATABASEFAILURE:
        displayMessage(id, "Paramètre invalide", tm);
        break;
        
    case ERA_INVALIDPAGE:
        displayMessage(id, "La page ajax n'existe pas sur le serveur", tm);
        break;
    default:
        displayMessage(id, "Code d'erreur invalide", tm);
    }
}


