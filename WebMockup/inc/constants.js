/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* @brief Code de retour Ajax */
const RCA_SUCCESS = 0;
const RCA_INVALIDPARAM = 1;
const RCA_DATABASEFAILURE = 2;


/* @brief Code de retour HTTP suite aux appels Ajax */
const ERA_INVALIDPAGE = 1000;




/* @brief Code de status des projets */
const STP_INPROGRESS = 0;
const STP_COMPLETED = 1;
const STP_CANCELED = 2;
