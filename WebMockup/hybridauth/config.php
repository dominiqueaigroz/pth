<?php
/**
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 */
// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

//On inclut la configuration de base 
require_once('globalConfig.php');

return
    array(
          "base_url" => HYBRIDAUTH_BASE_URL,
          "providers" => array(
                "Google" => array(
                        "enabled" => true,
                        "keys" => array("id" => GOOGLE_CLIENT_ID, "secret" => GOOGLE_SECRET_CODE),
                ),
          ),
          // If you want to enable logging, set 'debug_mode' to true.
          // You can also set it to
          // - "error" To log only error messages. Useful in production
          // - "info" To log info and error messages (ignore debug messages)
          "debug_mode" => true,
          // Path to file writable by the web server. Required if 'debug_mode' is not false
          "debug_file" => "log.txt",
);