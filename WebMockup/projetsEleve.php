<!--
Kilian PERISSET
I.IN-P3A
02.05.2016
Atelier Travaux École

@revision Cugni Zoé, Wikberg Sven, Oliveira Ricardo
            30.11.2016
-->
<?php
    require_once '/php/class.inc.all.php';
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Projets Eleves</title>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <style>
        .page_title {
            text-align: left;
        }

        .table th {
            text-align: center;
        }
    </style>

    <body>
        <nav id='nav' class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Course</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Mes projets <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="projetsProf.html">Listes de mes projets</a></li>
                                <li><a href="creationProjets.html">Créer un projet</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Mon compte<span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">

                                        <li><a href="#">Déconnexion</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="notifications_prof.html">Notifications</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container">

            <div class="starter-template">
                <h1 class="page_title">Projets en cours</h1>
                <div class="table_context_projects">
                    <table id="projectsInProgressTable" class="table table-hover table-responsive table-striped tablesorter">
                        <thead>
                            <tr>
                                <th>Nom du projet</th>
                                <th>Description</th>
                                <th>Nombre d'heures total</th>
                                <th>Nombre d'heures passé</th>
                                <th>Nombre d'heures restantes</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            foreach (EProjectManager::getInstance()->getAllProjects() as $row) {
                                echo '<tr>';
                                echo "<td>" . $row->getName() . "</td>";
                                echo "<td>" . $row->getDescription() . "</td>";
                                echo "<td>" . $row->getTotalHour() . "</td>";
                                echo "<td>" . $row->getSpentHour() . "</td>";
                                echo "<td>" . $row->getRemainingHour() . "</td>";
                                echo '<td><input type="checkbox" name="selectRow" value="1"></td>';
                                echo "</tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                    <a href="codeQR.html" class="pull-right btn btn-primary">Générer le code QR</a>
                    <br>
                    <h1 class="page_title">Projets terminé</h1>
                    <table id="projectsFinishedTable" class="table table-hover table-responsive table-striped tablesorter">
                        <thead>
                            <tr>
                                <th>Nom du projet</th>
                                <th>Description</th>
                                <th>Nombre d'heures total</th>
                                <th>Nombre d'heures passé</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <footer class="footer">
        </footer>
        <script>
            $('#btn').click(function (event) {
                var user = $('#user').val();
                var psw = $('#psw').val();
                $.ajax({
                    method: 'POST',
                    url: 'validate.php',
                    data: {'user': user, 'psw': psw},
                    dataType: 'json',
                    success: function (data) {
                        if (data.ReturnCode == "OK") {
                            alert("login");
                        }
                        else if (data.ReturnCode == "ERROR")
                        {
                            alert("erreur");
                        }
                    },
                    error: function (jqXHR) {

                    }
                })
            });
        </script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="http://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" type="text/css" charset="utf-8">
        <script>
            $(document).ready(function () {
                $('#projectsInProgressTable').DataTable({
                    "language": {
                        "url": "http://cdn.datatables.net/plug-ins/1.10.11/i18n/French.json"
                    },
                    "paging": false,
                    "ordering": true,
                    "searching": false,
                });

                $('#projectsFinishedTable').DataTable({
                    "language": {
                        "url": "http://cdn.datatables.net/plug-ins/1.10.11/i18n/French.json"
                    },
                    "paging": false,
                    "ordering": true,
                    "searching": false,
                });
            });
        </script>
    </body>
</html>