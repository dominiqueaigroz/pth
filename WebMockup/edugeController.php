<?php

/**
 * Auteur : Valentin Hutter
 * Date : 03.12.15
 * Version : 1.0
 * Description : Controller de l'application, il permet de se connecter à eduge.ch
 */
session_start();

require_once('globalConfig.php');

//On inclut la librairie - hybridauth
//et on définit le fichier de configuration hybridauth
require_once('hybridauth/Hybrid/Auth.php');
$config = 'hybridauth/config.php';

//Si l'utiliseur souhaite se connecter
//et que le provider est google, alors...
if (isset($_GET['provider']) && $_GET['provider'] == "Google") {
    try {
        //On instancie la librairie et on définit le provider, ici "Google"
        $hybridauth = new Hybrid_Auth($config);
        $authProvider = $hybridauth->authenticate($_GET['provider']);
        //On récupère le profil utilisateur
        //Le profil contient toutes les informations du profil google+
        $userData = $authProvider->getUserProfile();
        $contains = MAIL_FILTER;

        //On vérifie que le tableau $userData existe
        //afin de savoir si une connexion à google a bien été établie
        if ($userData && isset($userData->identifier)) {
            //On vérifie que le mail utilisé est bien
            //un mail provenant de eduge.ch, sinon on déconnecte l'utilisateur et on le redirige
            if (strpos($userData->email, $contains) == false) {
                setcookie('edugeError', ERROR_MESSAGE_MAIL_FILTER, (time() + 40));
                $authProvider->logout();
                header('Location: logout.php');
                exit();
            }
        }

        //Si l'utilisateur n'a pas été redirigé,
        //c'est qu'il s'est connecté avec un mail eduge.ch alors on affiche son profil...
        if ($userData && isset($userData->identifier)) {
            echo "<b>Email : </b>$userData->email<br>";
            echo "<b>Photo de profil :</b><br><img src=\"$userData->photoURL\" width=\"64\" alt=\"photo profil google\">";
            echo '<br><a href=\'logout.php\'>Logout</a>';
        }
    } catch (Exception $e) {
        //En cas d'erreur, on récupère le code de l'erreur
        //et on affiche l'erreur...
        switch ($e->getCode()) {
            case 0:
                echo "Erreur non spécifiée.";
                break;
            case 1:
                echo "Hybriauth - erreur de configuration.";
                break;
            case 2:
                echo "Le fournisseur n'est pas correctement configuré.";
                break;
            case 3:
                echo "Ce fournisseur n'est pas reconnu ou est désactivé.";
                break;
            case 4:
                echo "Des références d'application de fournisseur sont manquantes.";
                break;
            case 5:
                echo "L'authentification a échouée. L'utilisateur a annulé l'authentification ou le fournisseur a refusé la connexion.";
                break;
            case 6:
                echo "La demande de profil utilisateur a écouée. Ceci est sûrement dû au fait que l'utilisateur ne sait pas correctement connecté, veuillez réessayer !";
                $authProvider->logout();
                break;
            case 7:
                echo "L'utilisateur n'est pas connecté au fournisseur.";
                $authProvider->logout();
                break;
        }

        echo "<b>Message d'erreur :</b> " . $e->getMessage();
        echo "<hr /><h3>Trace</h3><pre>" . $e->getTraceAsString() . "</pre>";
    }
}
?>