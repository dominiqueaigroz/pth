<!DOCTYPE html>
<!--
# auteur ; Christophe Sadowski
-->
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Connection</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>

    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="container">
                    <div class="card card-container">
                        <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
                        <div id="titreLogin">École d'horlogerie de Genève</div>
                        <img id="logoHorlogerie" src="./images/logoHorlogerie.jpg" />
                        <p id="profile-name" class="profile-name-card"></p>
                        <form class="form-signin">
                            <?php
                            //Affiche une erreur si le mail n'est pas un mail eduge.ch
                            //Le cookie a une durée de vie de 40 secondes...
                            if (isset($_COOKIE['edugeError'])) {
                                echo $_COOKIE['edugeError'] . "<br>";
                            }
                            ?>
                            <button id="btnSignIn" value="btnSignIn">Sign in</button>
                            <button id="btnLogout" value="btnLogout">Logout</button>
                            <!--<a href="edugeController.php?provider=Google"><img src="http://siteapps.com/apps/img/468/logo" width="200"></a>-->
                        </form>
                        <!-- /form -->
                    </div>
                    <!-- /card-container -->
                </div>
                <!-- /container -->
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <p class="text-muted">&copy; 2017 - CFPT Informatique </p>
            </div>
        </footer>

        <script type="text/javascript" src="../js/eelauth.js"></script>
        <script id="apiGAuth" type="text/javascript" async defer src="https://apis.google.com/js/api.js" ></script>
        <script type="text/javascript" src="../js/manageEELAuth.js"></script>
    </body>

</html>
