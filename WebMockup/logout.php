<?php
/**
 * Auteur : Valentin Hutter
 * Date : 03.12.15
 * Version : 1.0
 * Description : Permet de se déconnecter de eduge.ch
 */

require_once('globalConfig.php');

// Initialisation de la session
session_start();

//Détruit toutes les variables de session
$_SESSION = array();

// Détruit le cookie de session
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

//Détruit la session
session_destroy();

//Redirige l'utilisateur sur le service de logout de Google
//Puis le re-redirige vers notre application
header('Location: https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue='.REDIRECT_URL_LOGOUT);