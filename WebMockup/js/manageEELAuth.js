
$(document).ready(function () {
    $("#btnSignIn").on("click", handleAuthClick);
    $("#btnLogout").on("click", handleLogoutClick);
    var script = document.getElementById("apiGAuth");
    script.onload = script.onreadystatechange = function () {
        // if(script.readyState === "complete"){
        EELAuth.EELClientInitialize(updateSigninStatus);
        // } 
    }

    function updateSigninStatus(isSignedIn) {
        if (isSignedIn) {
            EELAuth.getUserInfo(onReceiveUserInfo);
        } else {
            EELAuth.signIn();
        }
    }
    function handleAuthClick(event) {
        updateSigninStatus(EELAuth.isSignedIn());
    }

    function handleLogoutClick(event) {
        var delete_cookie = function () {
            document.cookie = "G_AUTHUSER_H=0" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        };
        delete_cookie("G_AUTHUSER_H=0");
        EELAuth.revokeAllScopes();
        //document.location.href = 'https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout';
    }

    function onReceiveUserInfo(info) {
        var email = info.email;
        var img = info.image;
        alert(email);
        window.location.href="php/functions.php?email=" + email; 
//                $.ajax({
//                    method: 'POST',
//                    url: '../Controller/get_users.php',
//                    //Passe en paramètre l'utilisateur connecté 
//                    data: {'user': email},
//                    dataType: 'json',
//                    success: function (data) {
//                        var msg = '';
//
//                        switch (data.ReturnCode) {
//                            case 0 : // tout bon
//                                displayUser(data.Data, img);
//                                break;
//                            case 2 : // problème récup données
//                            case 3 : // problème encodage sur serveur
//                            default:
//                                msg = data.Message;
//                                break;
//                        }
//                        if (msg.length > 0)
//                            $("#info").html(msg);
//                    },
//                    error: function (jqXHR) {
//                        var msg = '';
//                        switch (jqXHR.status) {
//                            case 404 :
//                                msg = "page pas trouvée. 404";
//                                break;
//                            case 200 :
//                                msg = "probleme avec json. 200";
//                                break;
//
//                        } // End switch
//                        if (msg.length > 0)
//                            $("#info").html(msg);
//                    }
//                });
//            }
//    }
//,

        function displayUser(arData, img) {
            var user = [arData];
            if (user.length > 0) {
                var email = arData.email.trim();
                var role = arData.role;
                var name = arData.lastName;
                $.ajax({
                    method: 'POST',
                    url: '../Controller/set_user.php',
                    //Passe en paramètre l'utilisateur connecté 
                    data: {'email': email, 'role': role, 'name': name, 'img': img},
                });
                $.redirect('./index.php');
            }
        }
    }
}
);