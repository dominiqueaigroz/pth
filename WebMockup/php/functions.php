<?php

require 'database.php';
$email = $_GET['email'];

function getRoles($emailUser) {
    $conn = EDatabase::getInstance();
    $stmt = $conn->prepare("SELECT idRole, nomRole FROM tbluser u, tblrole r where r.idRole = u.tblrole_idRole and emailUser = :emailUser");
    $stmt->execute(array(
        "emailUser" => $emailUser
    ));
    $result = $stmt->fetchAll();
    $tbl = array(
        "idRole" => $result[0]['idRole'],
        "nomRole" => $result[0]['nomRole']
    );
    return $tbl;
}

echo "<strong>".substr($email, 0, -9)."</strong> est un <span style=\"color:red;\">".getRoles($email)["nomRole"] . "</span> Il porte l'ID : <i>" . getRoles($email)["idRole"]. "</i>";
