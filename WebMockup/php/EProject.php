<!--
Cugni Zoé, Wikberg Sven, Oliveira Ricardo
IFA-P3C
Rentrée 2016 - Début décembre 2016
Atelier Travaux École
-->

<?php

class EProject
    {
		private $id;
        private $name;
        private $description;
		private $totalHour;
		private $spentHour;
        private $deadline;
		private $QRCode;
		private $managers;
		private $students;

        public function __construct($id, $name, $description, $totalHour, $spentHour=null, $deadline, $QRCode, $managers=null, $students=null)
        {
			$this->id = $id;
            $this->name = $name;
            $this->description = $description;
			$this->totalHour = $totalHour;
			$this->spentHour = $spentHour;			
            $this->deadline = $deadline;
			$this->managers = $managers;
			$this->QRCode = $QRCode;
			$this->students = $students;
        }
		
		public function getId()
        {
            return $this->id;
        }

        public function getName()
        {
            return $this->name;
        }

        public function getDescription()
        {
            return $this->description;
        }
		
		public function getTotalHour()
        {
            return $this->totalHour;
        }
		
		public function getSpentHour()
        {
            return $this->spentHour;
        }

        public function getDeadline()
        {
            return $this->deadline;
        }
		
		public function getManagers()
        {
            return $this->managers;
        }
		
		public function getQRCode()
        {
            return $this->QRCode;
        }
		
		public function getStudents()
        {
            return $this->students;
        }

        // À FAIRE
        public function getRemainingHour()
        {
            return null;
        }
		
		private function toArray()
		{
			return array('Id' => self::getId(), 'Name' => self::getName(), 'Description' => self::getDescription(), 'TotalHour' => self::getTotalHour(), 'SpentHour' => self::getSpentHour(), 'Deadline' => self::getDeadline(), 'Managers' => self::getManagers(), 'QRCode' => self::getQRCode(), 'Students' => self::getStudents());
		}
        /**
         * @brief Retourne cet objet sous forme json
         * @return La chaîne de caractères
         */
        public function toJson(){
            return json_encode($this->toArray());
        }
    }
