<!--
Cugni Zoé, Wikberg Sven, Oliveira Ricardo
IFA-P3C
Rentrée 2016 - Début décembre 2016
Atelier Travaux École
-->

<?php

require_once './php/database.php';
require_once './php/EProject.php';

class EProjectManager {
    private $projects;
	private static $objInstance;
	/**
	 * @brief	Class Constructor - Create a new database connection if one doesn't exist
	 * 			Set to private so no-one can create a new instance via ' = new KDatabase();'
	 */
	private function __construct() {
         $this->projects = array();
    }
	/**
	 * @brief	Like the constructor, we make __clone private so nobody can clone the instance
	 */
	private function __clone() {}
	/**
	 * @brief	Retourne une instance de mon project manager ou la crée
	 * @return $objInstance;
	 */
	public static function getInstance() {
		if(!self::$objInstance){
			try{
				self::$objInstance = new EProjectManager();	
			}catch(Exception  $e ){
				echo "EProjectManager Exception Error: ".$e;
			}
		}
		return self::$objInstance;
	} # end method


	/**
	 * @brief	Récupère le project en fonction de son identifiant
	 * @return 	Retourne un object EProject or FALSE si pas trouvé 
	 */
    public function getProject($id) 
    {
        // Recherche si le projet est déjà chargé
        foreach ($this->projects as $proj){
            if ($proj->getId() == $id)
                return $proj;
        }
        // Pas trouvé
        $query = EDatabase::prepare("SELECT idProject, NAME, DESCRIPTION, estimatedTime, SPENTHOUR, DEADLINE, QRCODE FROM PROJECT WHERE ID = :i");
        try{
            $query->execute(array(':i' => $id));
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) > 0){
                $proj = new EProject($result[0]['ID'], $row[0]['NAME'], $row[0]['DESCRIPTION'],  $row[0]['estimatedTime'],  $row[0]['SPENTHOUR'],  $row[0]['DEADLINE'],  $row[0]['QRCODE']);
                array_push($this->projects, $proj);
                return $proj;
            }
        }catch(PDOException  $e ){
            echo "getProject Error: ".$e;
        }
        // Fail
        return FALSE;
    }

	/**
	 * @brief	Récupère tous les projects
	 * @return 	Retourne tableau d'objects EProject 
	 */
    public function getAllProjects() 
    {
        // SpentHour doit être calculé  
        $this->projects = array();

        $query = EDatabase::prepare("SELECT idProject, NAME, DESCRIPTION, estimatedTime, DEADLINE, QRCODE FROM PROJECT");
        try{
            $query->execute();
            //echo '<pre>';
            while($row=$query->fetch(PDO::FETCH_OBJ)) {
                $proj = new EProject($row->idProject, $row->NAME, $row->DESCRIPTION,  $row->estimatedTime, null, $row->DEADLINE,  $row->QRCODE, null, null);
                array_push($this->projects, $proj);
                }
               // echo '</pre>';
        }catch(PDOException  $e ){
            echo "getAllProjects Error: ".$e;
        }
        return $this->projects;
    }
}