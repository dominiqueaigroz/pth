<?php
/**
 * Auteur : Valentin Hutter
 * Date : 03.12.15
 * Version : 1.0
 * Description : Fichier de configuration générale de l'application eduge.ch
 */

define('GOOGLE_CLIENT_ID', '816830446506-ijtjjo9a5oltaanelo2jhidmr4dvv86d.apps.googleusercontent.com');
define('GOOGLE_SECRET_CODE', 'xzV1W3xlE6eueVWrhrsVgDmQ');

define('HYBRIDAUTH_BASE_URL', 'http://localhost/hybridauth/index.php');
define('REDIRECT_URL_LOGOUT', 'http://localhost/index.php');

define('MAIL_FILTER', '@eduge.ch');
define('ERROR_MESSAGE_MAIL_FILTER', 'Veuillez vous connecter avec une adresse email " @eduge.ch"');