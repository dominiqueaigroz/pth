﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms;
using EEMPointeuse.iOS;

[assembly: Dependency(typeof(Connection))]
namespace EEMPointeuse.iOS
{
    /// <summary>
    /// On défini un objet Connection qui dérive de l'interface IEConnectionInterface
    /// </summary>
    public class Connection : IEConnectionInterface
    {
        /// <summary>
        /// Methode qui lorsque on est connecté prend en paramètre une callback, créer un objet EELAuth et passe la callback en paramètre à la methode doAuth de notre objet EELAuth
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public bool OnConnection(OnUserConnectedHandler callback)
        {
            EELAuth connect = new EELAuth();
            connect.doOAuth(callback);

            return true;
        }
    }
}