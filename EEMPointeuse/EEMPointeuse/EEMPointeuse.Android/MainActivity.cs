﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using EEMPointeuse.Droid;
using Xamarin.Forms;
using Android.Content;

[assembly: Dependency(typeof(MainActivity))]
namespace EEMPointeuse.Droid
{
    [Activity(Label = "EEMPointeuse", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity, IEMainActivityInterface
    {
        static private Intent _intent;

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());

            _intent = Intent;
        }
        /// <summary>
        /// Active l'activité principale
        /// </summary>
        /// <returns></returns>
        public bool ActivateMainActivity()
        {
            if (_intent != null)
            {
                _intent.SetFlags(ActivityFlags.ReorderToFront);

                Android.App.Application.Context.StartActivity(_intent);
                return true;
            }
            return false;
        }
    }
}

