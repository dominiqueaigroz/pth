﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EEMPointeuse
{
	public partial class MainPage : ContentPage
	{
        /// <summary>
        /// Récupère les dépendances de nos interfaces et les stock afin de pouvoir les utiliser
        /// </summary>
        IEConnectionInterface actionInterfaces = DependencyService.Get<IEConnectionInterface>();
        IEActivityInterface activityInterfaces = DependencyService.Get<IEActivityInterface>();
        public MainPage()
		{
			InitializeComponent();
		}
        /// <summary>
        /// Lorsque l'on clic sur le bouton de connexion, vérifie si notre interface n'est pas nul puis appel la methode OnConnection en lui passant la callback.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async void OnConnectionButtonClicked(object sender, EventArgs e)
        {
            if (actionInterfaces != null)
                actionInterfaces.OnConnection(OnConnectionCompleted);
        }
        /// <summary>
        /// Il s'agit de la callback utilisé pour track quand la connection a été effectuée.
        /// </summary>
        void OnConnectionCompleted()
        {
            EUserInfo testInfo = EUserManager.Instance.GetUserInfo();
            EUserManager.Instance.SetUserMail((EUserManager.Instance.GetUserInfo().family_name + "@eduge.ch").ToLower());
            sendInfoToServer();
            Navigation.PushAsync(new SecondaryPage());

            IEMainActivityInterface mainActivityInterfaces = DependencyService.Get<IEMainActivityInterface>();
            if (mainActivityInterfaces != null)
                mainActivityInterfaces.ActivateMainActivity();

        }
        /// <summary>
        /// Fonction envoyant en POST les données nécessaires au serveur
        /// </summary>
        public async void sendInfoToServer()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("email", EUserManager.Instance.GetUserInfo().email);
            dict.Add("projectCode", "JAOSMI8923NA78");

            string url = "http://sterne.lab.ecinf.ch/EEPointeuse/server/ajax/adduserevent.php";
            JsonPost jsonPost = new JsonPost(url);
            jsonPost.postData(dict);
        }
    }
}
