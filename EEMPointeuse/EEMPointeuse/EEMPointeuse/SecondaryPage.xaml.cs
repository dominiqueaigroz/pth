﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EEMPointeuse
{
	public partial class SecondaryPage : ContentPage
	{
		public SecondaryPage ()
		{
            InitializeComponent();
            StackLayout layout = new StackLayout { };
            this.Content = layout;
            Label email = new Label { Text = EUserManager.Instance.GetUserInfo().email };
            layout.Children.Add(email);
        }
	}
}