﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EEMPointeuse
{
    /// <summary>
    /// Signature méthode pour notifier un utilisateur connecté
    /// </summary>
    public delegate void OnUserConnectedHandler();

    public interface IEConnectionInterface
    {
        /// <summary>
        /// Methode qui lorsque on est connecté prend en paramètre une callback, créer un objet EELAuth et passe la callback en paramètre à la methode doAuth de notre objet EELAuth
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        bool OnConnection(OnUserConnectedHandler callback);
    }
}
