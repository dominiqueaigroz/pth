﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EEMPointeuse
{
    /// <summary>
    /// Interface pour l'activation de l'activité principale
    /// </summary>
    public interface IEMainActivityInterface
    {
        /// <summary>
        /// Active l'activité principale
        /// </summary>
        /// <returns></returns>
        bool ActivateMainActivity();
    }
}
