﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EEMPointeuse
{
    class EUserInfo
    {
        /// <summary>
        /// Attribut de l'objet EUserInfo stockant l'id sous forme de string
        /// </summary>
        public string sub;
        /// <summary>
        /// Attribut de l'objet EUserInfo stockant le given_name et le family_name sous forme de string au format "given_name family_name"
        /// </summary>
        public string name;
        /// <summary>
        /// Attribut de l'objet EUserInfo stockant le prénom sous forme de string au format "PRENOM"
        /// </summary>
        public string given_name;
        /// <summary>
        /// Attribut de l'objet EUserInfo stockant le prénom et le nom de famille sous forme de string au format "PRENOM.NOMFAMILLE"
        /// </summary>
        public string family_name;
        /// <summary>
        /// Attribut de l'objet EUserInfo stockant l'url de l'image de profil de l'utilisateur sous forme de string
        /// </summary>
        public string picture;
        /// <summary>
        /// Attribut de l'objet EUserInfo stockant la langue de l'utilisateur sous forme de string au format "fr"
        /// </summary>
        public string locale;
        /// <summary>
        /// Attribut de l'objet EUserInfo stockant l'email sous forme de string
        /// </summary>
        public string email;

    }
}
