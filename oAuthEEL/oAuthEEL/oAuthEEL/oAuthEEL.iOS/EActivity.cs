﻿using Xamarin.Forms;
using oAuthEEL.iOS;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using Foundation;


[assembly: Dependency(typeof(EActivity))]
namespace oAuthEEL.iOS
{
    public class EActivity : IEActivityInterface
    {
        public bool BringToFrontLastActivity()
        {
            NSUrl nurl = new NSUrl("oAuthEEL://");
            UIApplication.SharedApplication.OpenUrl(nurl);
            return false;
        }


        public void OpenUrl(string url)
        {
            if (string.IsNullOrEmpty(url)) return;

            if (!url.StartsWith("http://") && !url.StartsWith("https://"))
                url = "http://" + url;

            NSUrl nurl = new NSUrl(url);

            //var activityViewController = new UIActivityViewController(new NSObject[] { nurl }, null);
            //UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(activityViewController, true, null);
            UIApplication.SharedApplication.OpenUrl(nurl);
        }
    }
}
