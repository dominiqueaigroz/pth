﻿using oAuthEEL.Droid;
using System;
using Xamarin.Forms;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using Android.Content;
using System.Json;
using System.Collections.Generic;

[assembly: Dependency(typeof(MainActivity))]
namespace oAuthEEL.Droid
{
    [Activity(Label = "oAuthEEL", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    //L'objet MainActivity dérive également de IEMainActivityInterface
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity, IEMainActivityInterface
    {
        static private Intent _intent;

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;
            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());

            _intent = Intent;
        }
        /// <summary>
        /// Active l'activité principale
        /// </summary>
        /// <returns></returns>
        public bool ActivateMainActivity()
        {
            if (_intent != null)
            {
                _intent.SetFlags(ActivityFlags.ReorderToFront);

                Android.App.Application.Context.StartActivity(_intent);
                return true;
            }
            return false;
        }
    }

}



