﻿using Xamarin.Forms;
using oAuthEEL.Droid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

[assembly: Dependency(typeof(Connection))]
namespace oAuthEEL.Droid
{
    public class Connection : IEConnectionInterface
    {
        /// <summary>
        /// Methode qui lorsque on est connecté prend en paramètre une callback, créer un objet EELAuth et passe la callback en paramètre à la methode doAuth de notre objet EELAuth
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public bool OnConnection(OnUserConnectedHandler callback)
        {
            EELAuth connect = new EELAuth();
            connect.doOAuth(callback);

            return true;
        }
    }
}