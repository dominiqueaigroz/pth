﻿using Xamarin.Forms;
using oAuthEEL.Droid;
using Android.Content;

[assembly: Dependency(typeof(EActivity))]
namespace oAuthEEL.Droid
{
    public class EActivity : IEActivityInterface
    {
        private Intent _lastIntent;
        /// <summary>
        /// Nous affiche l'activité précédemment ouverte 
        /// </summary>
        /// <returns></returns>
        public bool BringToFrontLastActivity()
        {
            if (_lastIntent != null)
            {
                _lastIntent.SetFlags(ActivityFlags.ReorderToFront);

                Android.App.Application.Context.StartActivity(_lastIntent);
                // done
                return true;
            }
            return false;
        }

        /// <summary>
        /// Permet d'ouvrir une page dans le navigateur à partir d'une url recue en paramètre
        /// </summary>
        /// <param name="url">L'url de la page à ouvrir</param>
        public void OpenUrl(string url)
        {
            if (string.IsNullOrEmpty(url)) return;

            if (!url.StartsWith("http://") && !url.StartsWith("https://"))
                url = "http://" + url;

            var uri = Android.Net.Uri.Parse(url);
            _lastIntent = new Intent(Intent.ActionView, uri);
            _lastIntent.AddFlags(ActivityFlags.NewTask);
            Android.App.Application.Context.StartActivity(_lastIntent);

        }

    }
}