﻿namespace oAuthEEL
{
    /// <summary>
    /// Interface pour l'activation de l'activité principale
    /// </summary>
    public interface IEMainActivityInterface
    {
        /// <summary>
        /// Active l'activité principale
        /// </summary>
        /// <returns></returns>
        bool ActivateMainActivity();
    }
}
