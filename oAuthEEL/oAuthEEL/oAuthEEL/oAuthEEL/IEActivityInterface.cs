﻿namespace oAuthEEL
{
    /// <summary>
    /// Interface pour les activités déclenchées sur l'os
    /// </summary>
    public interface IEActivityInterface
    {
        /// <summary>
        /// Ouvrir une url, en utilisant les activités, dans le navigateur par défaut 
        /// </summary>
        /// <param name="url">l'url à ouvrir</param>
        void OpenUrl(string url);
        /// <summary>
        /// Ramène l'utilisateur à l'activité précédente
        /// </summary>
        /// <returns></returns>
        bool BringToFrontLastActivity();

    }
}
