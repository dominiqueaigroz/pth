﻿namespace oAuthEEL
{
    class EUserManager
    {
        /// <summary>
        /// Les informations de l'utilisateur connecté
        /// </summary>
        private EUserInfo info;
        /// <summary>
        /// Récupère l'objet de l'utilisateur connecté
        /// </summary>
        /// <param name="InInfo">Notre object contenant les infos de l'utilisateur</param>
        public void SetUserInfo(EUserInfo InInfo)
        {
            info = InInfo;
        }
        /// <summary>
        /// Fonction affectant le mail de l'utilisateur à la propriété email de notre objet info
        /// </summary>
        /// <param name="mail">Le mail de l'utilisateur</param>
        public void SetUserMail(string mail)
        {
            info.email = mail;
        }
        /// <summary>
        /// Récupère l'objet contenant les informations de l'utilisateur connecté
        /// </summary>
        /// <returns>EUserInfo ou null si pas d'utilisateur connecté</returns>
        public EUserInfo GetUserInfo()
        {
            return info;
        }
        /// <summary>
        /// Est-ce qu'on a un utilisateur connecté
        /// </summary>
        /// <returns>True si connecté</returns>
        public bool IsConnected()
        {
            return info != null;
        }
        /// <summary>
        /// Efface les informations de l'utilisateur connecté
        /// </summary>
        public void ClearConnection()
        {
            info = null;
        }
        /// <summary>
        /// Créer une instance contenant les informations de l'utilisateur connecté uniquement si il n'est pas déjà log dans l'application
        /// </summary>
        public static EUserManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new EUserManager();
                }
                return _instance;
            }
        }
        /// <summary>
        /// C'est l'instance de cette classe qu'on garanti unique
        /// </summary>
        private static EUserManager _instance;
        /// <summary>
        /// Pour éviter qu'on puisse créer cet object avec new
        /// </summary>
        private EUserManager()
        {
            info = null;
        }

    }
}
