﻿using System;
using System.Collections.Generic;
using System.Net;

namespace oAuthEEL
{
    class JsonPost
    {
        private string urlToPost = "";
        /// <summary>
        /// On affecte l'url recue dans le constructeur à notre propriété urlToPost
        /// </summary>
        /// <param name="urlToPost">L'url cible de notre POST</param>
        public JsonPost(string urlToPost)
        {
            this.urlToPost = urlToPost;
        }
        /// <summary>
        /// Methode recevant un dictionnaire de données contenant les infos à envoyer et les envoie sous forme de POST
        /// </summary>
        /// <param name="dictData">Dictionnaire de données avec les informations à envoyer</param>
        /// <returns></returns>
        public bool postData(Dictionary<string, string> dictData)
        {
            WebClient webClient = new WebClient();
            byte[] resByte;
            string resString;
            byte[] reqString;
            string parameters = "";
            bool bFirst = true;
            //Créer la chaine avec les données du Dictionnaire afin de les envoyer en POST
            foreach (KeyValuePair<string, string> entry in dictData)
            {
                if (bFirst == false)
                    parameters += "&";
                parameters += (entry.Key + "=" + entry.Value);
                bFirst = false;
            }
            try
            {
                // Envoie les données en POST et récupère une réponse du serveur
                webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                resString = webClient.UploadString(this.urlToPost, parameters);
                webClient.Dispose();
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }

            return false;
        }
    }
}
