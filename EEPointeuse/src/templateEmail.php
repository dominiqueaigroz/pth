<?php
//define("_BYPASS_CHECKSESSION_", TRUE);
require_once $_SERVER['DOCUMENT_ROOT'] . '/server/inc.all.php';
/** @remark On doit être au minimum Project Manager pour visualiser cette page */
define('ROLE', EROLE_PM);
require_once './server/check.role.php';

$email = ESession::getInstance()->getEmail();
$fullname = ESession::getInstance()->getFullname();
$templateTypes = EEmailManager::getTemplateTypes();

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Consultation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script
            src="http://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="main">
            
            <?php getNavbar('edu-aigrozd@eduge.ch');?>
            <h1 class="titlePage"> Gestion des emails </h1>
            <form>
                <table class="mainTable" id="tableEmail">
                    <tr>
                        <td>
                            <label for="name">Nom du template </label>
                        </td>
                        <td>
                            <input type="text" id="name" />   
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="subject"> Sujet de l'email </label>
                        </td>
                        <td>
                            <input type="text" id="subject" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="body"> Le template </label>
                        </td>
                        <td>
                            <textarea id="body"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="templatesType"> Le type du template </label>
                        </td>
                        <td>
                            <select id="templatesType">
                                <?php
                                foreach ($templateTypes as $type)
                                {
                                    echo "<option value=" . $type["code"] . "> " . $type["label"];
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button id="save" class="buttons"> sauvegarder </button></td>
                    </tr>
                </table>
            </form>
        </div>
        <script>
            $(document).ready(function(){
                
                showTemplateByType($("#templatesType").val());
                
                $("#templatesType").change(function(){
                    showTemplateByType(this.value);
                });
                
                $("#save").click(function(){
                    $.get(
                        "server/ajax/setTemplates.php",
                        {
                            type : $("#templatesType").val(),
                            name : $("#name").val(),
                            subject : $("#subject").val(),
                            body : $("#body").val()
                        }
                    ); 
                });
            });
            
            function showTemplateByType(type)
            {
                $.get(
                    "server/ajax/getTemplates.php",
                    {
                        type : type
                    },
                    function(data){
                        var template = JSON.parse(data);
                        if(template !== null)
                        {
                            $('#body').text(template['body']);
                            $('#subject').val(template['subject']);
                            $('#name').val(template['name']);
                        }
                        else
                        {
                            $('#body').text('');
                            $('#subject').val('');
                            $('#name').val('');
                        }
                     }
                );
            }
           
        </script>
    </body>
</html>