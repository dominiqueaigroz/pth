<?php
require_once './server/inc.all.php';
/** @remark On doit être au minimum User pour visualiser cette page */
define('ROLE', EROLE_USER);
require_once './server/check.role.php';

$email = (isset($_GET[VARS_URL_EMAIL])) ? filter_input(INPUT_GET, VARS_URL_EMAIL) : ESession::getInstance()->getEmail();
$projectCode = (isset($_GET[VARS_URL_PROJECT_CODE])) ? filter_input(INPUT_GET, VARS_URL_PROJECT_CODE) : null;
$day = (isset($_GET[VARS_URL_DAY])) ? filter_input(INPUT_GET, VARS_URL_DAY) : null;
$to = null;
$from = null;
if (isset($day)) {
    $to = $day;
    $from = $day;
}
$result = EEventManager::getRecordEvents($projectCode, $email, $from, $to);
$cpt = 0;
?>

<!DOCTYPE html>

<html>
    <head>
        <title>Consultation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body>
        <?php
        getNavbar();
        ?>
        <div class="main">
            <h1 class="titlePage">Récap' d'un jour</h1>

            <h1 class="titleTable"><?= (strlen($email) > 0) ? "Nombre d'heures faites par ". $email . " pour le ". strftime("%A %d %B %Y", strtotime($from)) : "Nombre d'heures faites par tous les élèves";?></h1>
            <table class="mainTable" >
                <thead>
                <td>Pointages</td>  
                </thead>
                <tbody class="tableBody">
                    <?php
                    if (isset($email) && isset($to) && isset($from)) {
                        foreach ($result as $ev) {
                            echo "<tr><td>" . $ev->dt->format('H:i:s') . "</td></tr>";
                        }
                    }
                    ?>

                </tbody>
                <tfoot class="tableFooter">

                </tfoot>
            </table>
        </div>
    </body>
</html>

