<?php
$filters = EHelperFilter::getFilters();
$email = ESession::getInstance()->getEmail();
?>
<nav class="topnav">
    <ul id="topnavleft" class="left">
        <li><a href="RecordByDay.php?email=<?=$email?>&day=<?= date('Y-m-d')?>">Today</a></li>
        <li><a href="RecapEleveMois.php?email=<?=$email?>&month=<?=date('Y-m')?>">Mois en cours</a></li>
        <li><a href="RecapDeltaEleve.php?email=<?=$email?>">Récap</a></li>
        <li><a href="studentProject.php">Projet</a></li>
        
    </ul>

    <ul id="topnavright" class="right">

        <li> <span id="signOut" ><a href="#">Déconnexion</a></span></li>
        <li> <img class="userlogo" src="<?= ESession::getInstance()->getImage()?>" alt="Logo" /></li>
        
        <li><?= ESession::getInstance()->getFullname();?></li>
        <li>
            <div class="toolTip">
                <span class="toolTipText"></span>

            </div>
        </li>
        <li class="navbug">
        </li>
        
    </ul>

</nav>
<script src="https://apis.google.com/js/platform.js"></script>        
<script type="text/javascript" src="./js/eelauth.js"></script>
<script type="text/javascript" src="./js/constants.js"></script>
<script type="text/javascript">
    var popupDisconnectWindow = null;
    /**
     * Call-back quand on click sur le bouton login
     * @param {type} event
     */
    $("#signOut").on("click", function(event){
        event.preventDefault();
        // alwaysLowered est mis à 1 afin de placer la fenêtre de déconnexion derrière
        // une fois qu'on remet le focus sur la fenêtre principale
        var strWindowFeatures = "menubar=no,location=no,resizable=no,scrollbars=no,status=no,alwaysLowered=1";
        popupDisconnectWindow = window.open('https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout', "LogoutEEL", strWindowFeatures);        
        // Fermer la fenêtre de déconnexion dans 2 secondes
        setTimeout(closeDisconnectWindow, 1000);
    });

    /**
     * Call-back quand on click sur le bouton bug report
     * @param {type} event
     */
    $("li.navbug").on("click", function(event){
        event.preventDefault();
        showPopUp();
    });

    /**
     * On ferme la fenêtre de déconnexion EEL
     * et on remet le focus sur la fenêtre principale
     */
    function closeDisconnectWindow() {
        if (popupDisconnectWindow != null && !popupDisconnectWindow.closed) {
            popupDisconnectWindow.close();
            popupDisconnectWindow = null;
        }
        window.location.href = "disconnect.php";
        
    }
    
    function showPopUp(){
        window.open("./templates/bugReport.html", "", "width=500,height=300");
    }
</script>