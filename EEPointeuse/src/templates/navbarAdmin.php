<?php
$filters = EHelperFilter::getFilters();
?>
<nav class="topnav">
    <ul id="topnavleft" class="left">
        <li><a href="EventDay.php">En cours</a></li>
        <li><a href="ErrorTimeChecking.php">Erreurs</a></li>
        <li><a href="nbheuresMoisEleves.php">Nombre d'heure</a></li>
        <li><a href="filterSettings.php">Filtres</a></li>
        <li><a href="teacherProject.php">Projets</a></li>
        <li><a href="WorkingDays.php">Horaires</a></li>
        <li><a href="Holidays.php">Vacances</a></li>
        <!--li><a href="#">Notifications</a></li-->
        <li><a href="rolesManagement.php">Gestion des rôles</a></li>
    </ul>

    <ul id="topnavright" class="right">

        <li> <span id="signOut" ><a href="#">Déconnexion</a></span></li>
        <li> <img class="userlogo" src="<?= ESession::getInstance()->getImage() ?>" alt="Logo"/></li>

        <li><?= ESession::getInstance()->getFullname(); ?></li>
        <li>
            <div class="toolTip">
                <select name="navFilters" class="filterSelect">
                    <option></option>
                    <?php foreach ($filters as $filter): ?>
                        <option><?= $filter->name ?></option>
                    <?php endforeach; ?>
                </select>
                <span class="toolTipText"></span>

            </div>
        </li>
        <li>
            <input type="text" name="searchBar" placeholder="Rechercher..."/>
        </li>
        <li class="navbug">
        </li>
    </ul>

</nav>
<script src="https://apis.google.com/js/platform.js"></script>        
<script type="text/javascript" src="./js/eelauth.js"></script>
<script type="text/javascript" src="./js/constants.js"></script>
<script type="text/javascript">

    var popupDisconnectWindow = null;
    if (document.readyState === "complete") {
        /**
         * Call-back quand on click sur le bouton login
         * @param {type} event
         */
        $("#signOut").on("click", function (event) {
            event.preventDefault();
            // alwaysLowered est mis à 1 afin de placer la fenêtre de déconnexion derrière
            // une fois qu'on remet le focus sur la fenêtre principale
            var strWindowFeatures = "menubar=no,location=no,resizable=no,scrollbars=no,status=no,alwaysLowered=1";
            popupDisconnectWindow = window.open('https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout', "LogoutEEL", strWindowFeatures);
            // Fermer la fenêtre de déconnexion dans 2 secondes
            setTimeout(closeDisconnectWindow, 1000);
        });
        /**
         * Call-back quand on click sur le bouton bug report
         * @param {type} event
         */
        $("li.navbug").on("click", function (event) {
            event.preventDefault();
            showPopUp();
        });
    }

    /**
     * On ferme la fenêtre de déconnexion EEL
     * et on remet le focus sur la fenêtre principale
     */
    function closeDisconnectWindow() {
        if (popupDisconnectWindow != null && !popupDisconnectWindow.closed) {
            popupDisconnectWindow.close();
            popupDisconnectWindow = null;
        }
        window.location.href = "disconnect.php";

    }

    /**
     * Ouvre la page pour reporter les bugs
     */
    function showPopUp() {
        window.open("./templates/bugReport.html", "", "width=500,height=300");
    }
</script>