<?php
/** @remark Commenter cette ligne pour vérifier la session. */
//define('_BYPASS_CHECKSESSION_', TRUE);

require_once './server/inc.all.php';
/** @remark On doit être au minimum Project Manager pour visualiser cette page */
define('ROLE', EROLE_ADMIN);
require_once './server/check.role.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Jour de travail</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://apis.google.com/js/platform.js"></script>        
        <script type="text/javascript" src="./js/eelauth.js"></script>
        <script type="text/javascript" src="./js/constants.js"></script>

    </head>
    <?php
    $workingDaysForAll = EHelperDay::GetWorkingDays()->Weekdays;
    $daytest = new DateTime("2018-03-26");
    eventCountOnDay($daytest);
    ?>


    <body>
        <div id="infoMsg"></div>

        <?php
        getNavbar();
        ?>
        <div class="main">
            <h1 class="titlePage">Administration</h1>


            <div id="filtersTableHeader">
                <h1 class="titleTable" id="titleLeft">Plage horaire de présence</h1>
                <div class="period">
                    <ul>
                        <li class="square NoInteractiveSquareAll"  ></li>
                        <li class="square NoInteractiveSquarePeriod" >Matin</li>
                        <li class="square NoInteractiveSquarePeriod" >Après-midi</li>
                    </ul>
                </div>
                <div id="mainarea">
                    <ul>
                        <li data-day="0" class="square NoInteractiveSquareDay">Lundi</li>
                        <li data-day="1" class="square NoInteractiveSquareDay">Mardi</li>
                        <li data-day="2" class="square NoInteractiveSquareDay">Mercredi</li>
                        <li data-day="3" class="square NoInteractiveSquareDay">Jeudi</li>
                        <li data-day="4" class="square NoInteractiveSquareDay" >Vendredi</li>
                    </ul>

                    <div id="morning">
                        <ul>
                            <li data-day="0" class="square <?php if ($workingDaysForAll[EWD_MONDAY] == EWD_MORNING || $workingDaysForAll[EWD_MONDAY] == EWD_FULLDAY) echo "selectedDays"; ?>"></li>
                            <li data-day="1" class="square <?php if ($workingDaysForAll[EWD_TUESDAY] == EWD_MORNING || $workingDaysForAll[EWD_TUESDAY] == EWD_FULLDAY) echo "selectedDays"; ?>"></li>
                            <li data-day="2" class="square <?php if ($workingDaysForAll[EWD_WEDNESDAY] == EWD_MORNING || $workingDaysForAll[EWD_WEDNESDAY] == EWD_FULLDAY) echo "selectedDays"; ?>"></li>
                            <li data-day="3" class="square <?php if ($workingDaysForAll[EWD_THURSDAY] == EWD_MORNING || $workingDaysForAll[EWD_THURSDAY] == EWD_FULLDAY) echo "selectedDays"; ?>"></li>
                            <li data-day="4" class="square <?php if ($workingDaysForAll[EWD_FRIDAY] == EWD_MORNING || $workingDaysForAll[EWD_FRIDAY] == EWD_FULLDAY) echo "selectedDays"; ?>"></li>
                        </ul>
                    </div>
                    <div id="afternoon">
                        <ul>
                            <li data-day="0" class="square <?php if ($workingDaysForAll[EWD_MONDAY] == EWD_AFTERNOON || $workingDaysForAll[EWD_MONDAY] == EWD_FULLDAY) echo "selectedDays"; ?>"></li>
                            <li data-day="1" class="square <?php if ($workingDaysForAll[EWD_TUESDAY] == EWD_AFTERNOON || $workingDaysForAll[EWD_TUESDAY] == EWD_FULLDAY) echo "selectedDays"; ?>"></li>
                            <li data-day="2" class="square <?php if ($workingDaysForAll[EWD_WEDNESDAY] == EWD_AFTERNOON || $workingDaysForAll[EWD_WEDNESDAY] == EWD_FULLDAY) echo "selectedDays"; ?>"></li>
                            <li data-day="3" class="square <?php if ($workingDaysForAll[EWD_THURSDAY] == EWD_AFTERNOON || $workingDaysForAll[EWD_THURSDAY] == EWD_FULLDAY) echo "selectedDays"; ?>"></li>
                            <li data-day="4" class="square <?php if ($workingDaysForAll[EWD_FRIDAY] == EWD_AFTERNOON || $workingDaysForAll[EWD_FRIDAY] == EWD_FULLDAY) echo "selectedDays"; ?>"></li>
                    </div>
                    <button id="btnSave">Save</button>
                </div>
                <select>
                    <?php
                    echo "<option value='" . EWD_ALLUSERS . "'>*</option>";
                    foreach (EUserManager::getAllUserEmails() as $value) {
                        echo "<option value='" . $value . "'>" . $value . "</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <script>
            $('document').ready(function () {
                $("#morning li.square, #afternoon li.square").click(function () {
                    $(this).toggleClass('selectedDays');
                });
                $("#mainarea li.NoInteractiveSquareDay").click(function () {
                    day = $(this).attr("data-day");
                    $("#morning");
                });
                $("#btnSave").click(function () {
                    // L'utilisateur en question
                    /* @todo récupérer l'email du combo-box utilisateur */
                    var useremail = $("select option:selected").text();
                    // Crée un tableau qui contiendra les jours
                    var weekDays = [];
                    var elMorning = $("#morning ul li.square");
                    var elAfternoon = $("#afternoon ul li.square");
                    if (useremail === "*") {
                     useremail="-";   
                    }
                    // On parcoure les li du matin, correspondants aux jours
                    elMorning.each(function () {
                        // Crée l'objet qui contient les information sur le jour
                        // et mets des valeurs par défaut
                        var Day = new Object();
                        Day.morning = false;
                        Day.afternoon = false;
                        Day.index = parseInt($(this).attr("data-day"));
                        
                        if ($(this).is(".selectedDays")) {
                            Day.morning = true;
                        }
                        weekDays.push(Day);
                    }); // :End each Morning
                    // On parcoure les li du après-midi, correspondants aux jours
                    
                    elAfternoon.each(function () {
                        var dayIndex = parseInt($(this).attr("data-day"));
                        
                        if ($(this).is(".selectedDays")) {
                            weekDays[dayIndex].afternoon = true;
                        }
                        
                    }); // :End each Afternoon
                    
                    $.ajax({
                        method: 'POST',
                        url: 'server/ajax/saveworkingdays.php',
                        // Passe en paramètre l'utilisateur concerné et
                        // le tableau des jours travaillés
                        data: {'user': useremail, 'days': weekDays},
                        dataType: 'json',
                        success: function (data) {
                            var msg = '';
                            
                            switch (data.ReturnCode) {
                                case 0 : // tout bon
                                    break;
                                case 1 : // problème de paramètre
                                case 2 : // problème de sauvegarde
                                default:
                                    msg = data.Message;
                                    break;
                            }
                            
                            if (msg.length > 0)
                                $("#infoMsg").html(msg);
                        },
                        error: function (jqXHR) {
                            var msg = '';
                            switch (jqXHR.status) {
                                case 404 :
                                    msg = "page pas trouvée. 404";
                                    break;
                                case 200 :
                                    msg = "probleme avec json. 200";
                                    msg += "</BR>réponse: " + jqXHR.responseText;
                                    break;
                                    
                            } // End switch
                            
                            if (msg.length > 0)
                                $("#infoMsg").html(msg);
                        }
                    }); // :End of ajax
                    
                }); // :End of click
                
                $("select").change(function () {
                    var thisName = $(this).val();
                    $.ajax({
                        method: 'POST',
                        url: 'server/ajax/getWorkingDays.php',
                        // Passe en paramètre l'utilisateur concerné et
                        // le tableau des jours travaillés
                        data: {'user': thisName},
                        dataType: 'json',
                        success: function (data) {
                            var msg = '';
                            
                            switch (data.ReturnCode) {
                                case 0 : // tout bon
                                    displayWorkingDays(data.Result);
                                    break;
                                case 1 : // problème de paramètre
                                case 2 : // problème de sauvegarde
                                default:
                                    msg = data.Message;
                                    break;
                            }
                            
                            if (msg.length > 0)
                                $("#infoMsg").html(msg);
                        },
                        error: function (jqXHR) {
                            var msg = '';
                            switch (jqXHR.status) {
                                case 404 :
                                    msg = "page pas trouvée. 404";
                                    break;
                                case 200 :
                                    msg = "probleme avec json. 200";
                                    msg += "</BR>réponse: " + jqXHR.responseText;
                                    break;
                                    
                            } // End switch
                            
                            if (msg.length > 0)
                                $("#infoMsg").html(msg);
                        }
                    }); // :End of ajax
                    
                    
                });
            });// Fin Document Ready
            
            
            function    displayWorkingDays(w) {
                //Enleve le css de toutes les cases
                $("#morning ul li").toggleClass('selectedDays', false);
                $("#afternoon ul li").toggleClass('selectedDays', false);
                var tblWeekDays = w.Weekdays;
                var i = 0;
                //Ajoute le css sur les cases corréspondante au tableau.
                tblWeekDays.forEach(function (value) {
                    
                    if (value === 1) {
                        $("#morning ul li[data-day='" + i + "']").toggleClass('selectedDays', true);
                    }
                    if (value === 2) {
                        $("#afternoon ul li[data-day='" + i + "']").toggleClass('selectedDays', true);
                    }
                    if (value === 3) {
                        $("#morning ul li[data-day='" + i + "']").toggleClass('selectedDays', true);
                        $("#afternoon ul li[data-day='" + i + "']").toggleClass('selectedDays', true);
                    }
                    i++;
                }); //Fin foreach
            }
            ;
            
            
            
            
        </script>
    </body>

</html>
