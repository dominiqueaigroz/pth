<?php
//define('_BYPASS_CHECKSESSION_', TRUE);

require_once './server/inc.all.php';
/** @remark On doit être au minimum Project Manager pour visualiser cette page */
define('ROLE', EROLE_PM);
require_once './server/check.role.php';

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Administration</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body onload="loadData()">
                    <?php
            getNavbar();
            ?>
        <div class="main">
            <h1 class="titlePage">Administration</h1>

            <div id="filtersTableHeader">
                <h1 class="titleTable" id="titleLeft">Paramètres des filtres</h1>
                De <input type="date" id="from"/> à <input type="date" id="to">
            </div>
            <div class="filterLayout filterLayoutButtons">
                <div class="buttons firstButtons">
                    <button class="threeButtons plus" onclick="addFilter()"></button>
                    <button class="threeButtons minus" onclick="deleteFilter()"></button>
                    <button class="threeButtons edit" onclick="addOrRenameFilter('<?=METHOD_RENAME_FILTER?>')"></button>
                </div>
                <div class="buttons">
                    <button class="singleButton rightArrow" onclick="removeUsersFromFilter()"></button>
                </div>
                <div class="buttons">
                    <button class="singleButton leftArrow" onclick="addUsersToFilter()"></button>
                </div>
            </div>
            <div class="filterLayout">

                <table class="mainTable smallTable" id="filtersNameTable">
                    <thead class="tableHead">
                    <th>Filtres</th> 
                    </thead>
                    <tbody class="tableBody">

                    </tbody>

                </table>
                <table class="mainTable smallTable" id="filtersUsersInFilter">
                    <thead class="tableHead">
                    <th id="filterName">{Nom du filtre}</th> 
                    </thead>
                    <tbody class="tableBody">

                    </tbody>

                </table>
                <table class="mainTable smallTable" id="filterAllUsersTable">
                    <thead class="tableHead">
                    <th>Élèves</th> 
                    </thead>
                    <tbody class="tableBody">
                    </tbody>

                </table>
            </div>

        </div>
        <script>
                        const OWNER = '<?= ESession::getInstance()->getEmail() ?>';
                        var canSelectFilter = false;
                        /**
                         * Charge les filtres
                         */
                        function loadData() {
                            $('.filterSelect').change(function () {
                                var index = $(this)[0].selectedIndex - 1;
                                if (index < 0) {
                                    index = 0;
                                }
                                $('.filterSelect').attr('title', 'De ' + window.filters[index].from.toString().substring(0, 10) + ' à ' + window.filters[index].to.toString().substring(0, 10));
                            });

                            $.ajax({url: '/server/ajax/filtersData.php',
                                success: function (data) {
                                    window.filters = JSON.parse(data);
                                    for (var i = 0, len = filters.length; i < len; i++) {
                                        var rowId = 'filtersNameRow' + (i + 1);
                                        appendToTable('#filtersNameTable', filters[i].name, rowId);
                                    }
                                    $('#filtersNameTable tr td').on('click', function (e) {
                                        selectRowFilter('#' + e.target.id);
                                    });
                                }});
                            canSelectFilter = true;
                            $.ajax({url: '/server/ajax/filtersData.php',
                                data: {<?= VARS_URL_METHOD ?>: '<?= METHOD_GET_ALL_EMAIL ?>'},
                                success: function (data) {
                                    window.notInFilterEmails = JSON.parse(data);
                                    window.allUsersEmails = JSON.parse(data);
                                }});
                            clearTable('filterAllUsersTable');
                        }
                        /**
                         * Ajoute une ligne à une table
                         * @param {string} tableSelector
                         * @param {string} textToAppend
                         * @param {string} id
                         */
                        function appendToTable(tableSelector, textToAppend, id = null) {
                            if (id !== null) {
                                id = 'id="' + id + '"';
                            }
                            $(tableSelector).append('<tr><td ' + id + '><span>' + textToAppend + '</span></td></tr>');
                        }
                        /**
                         * Selectionne un filtre
                         * @param {string} rowToSelect
                         */
                        function selectRowFilter(rowToSelect) {
                            if (canSelectFilter) {
                                if ($('.selected').parent().index() !== -1)
                                    updateDateRange($('.selected').parent().index());
                                $('.selected').text($('.selected').text().substring(1));
                                $('.selected').removeClass('selected');
                                $(rowToSelect).addClass('selected');
                                $(rowToSelect).prepend('>');
                                getUsersInFilter($(rowToSelect).text().substring(1), OWNER);
                                var index = parseInt($(rowToSelect).parent().index());
                                refreshDateRange(index)
                            }
                        }
                        /**
                         * Cette fonction raffraichit la plage de dates
                         * @param {string} index
                         */
                        function refreshDateRange(index) {
                            //yyyy-mm-dd = 10 char
                            if ($('.selected').parent().index() < window.filters.length) {
                                var from = window.filters[index].from.toString().substring(0, 10);
                                var to = window.filters[index].to.toString().substring(0, 10)
                                $('#from').val(from);
                                $('#to').val(to);
                            }

                        }
                        /**
                         * Ajoute les utilisateurs dans le filtre
                         * @param {json} users
                         */
                        function refreshUsersInFilterTable(users) {
                            clearTable('filtersUsersInFilter');
                            for (var i = 0, len = users.length; i < len; i++) {
                                appendToTable('#filtersUsersInFilter', users[i], 'userInFilterRowID' + i);
                            }
                            $('#filtersUsersInFilter tr td').on('click', function (e) {
                                selectUser('#' + e.target.id);
                            });
                            $('#filterName').text($('.selected').text().substring(1));

                        }
                        /**
                         * Retourne les utilisateurs dans un filtre
                         * @param {string} filterName
                         * @param {string} filterOwner
                         */
                        function getUsersInFilter(filterName, filterOwner) {
                            $.ajax({url: '/server/ajax/filtersData.php',
                                type: 'GET',
                                data: {<?= VARS_URL_FILTER_NAME ?>: filterName, <?= VARS_URL_FILTER_OWNER ?>: filterOwner},
                                success: function (data) {
                                    window.notInFilterEmails = window.allUsersEmails.slice();
                                    window.usersInFilter = JSON.parse(data);
                                    refreshUsersInFilterTable(window.usersInFilter);
                                    window.usersInFilter.forEach(function (element) {
                                        var index = window.notInFilterEmails.indexOf(element);
                                        if (index > -1) {
                                            window.notInFilterEmails.splice(index, 1);
                                        }
                                    });
                                    clearTable('filterAllUsersTable');
                                    window.notInFilterEmails.forEach(function (email) {
                                        appendToTable('#filterAllUsersTable', email, 'allUsersRow' + window.notInFilterEmails.indexOf(email));
                                    });
                                    $('#filterAllUsersTable tr td').on('click', function (e) {
                                        selectUser('#' + e.target.id);
                                    });


                                }});
                        }
                        /**
                         * Supprime toutes les lignes d'un tableau
                         * @param {string} tableID
                         */
                        function clearTable(tableID) {
                            $('#' + tableID + ' > tbody').empty();
                        }

                        /**
                         * Cette fonction renomme le filtre séléctionné
                         * @param {string} method
                         */
                        function addOrRenameFilter(method) {
                            window.filterName = $('.selected').text().substring(1);
                            var idRow = $('.selected').attr('id');
                            $('.selected').replaceWith('<input type="text" class="newName" id="' + idRow + '" value="' + window.filterName + '"/>').removeClass('selected');
                            $('.newName').addClass('selected');
                            $('.newName').select();
                            $('.newName').on('blur keyup', function (e) {
                                canSelectFilter = false;
                                if (e.type === 'blur' || e.keyCode === 13) {
                                    var filterFrom = ($('#from').val().length === 8) ? $('#from').val() : '2016-01-01';
                                    var filterTo = ($('#to').val().length === 8) ? $('#to').val() : '2018-01-01';
                                    var newName = $('.newName').val();
                                    var params = {<?= VARS_URL_FILTER_OWNER ?>: OWNER, <?= VARS_URL_METHOD ?>: method, <?= VARS_URL_NEW_NAME ?>: newName, <?= VARS_URL_FROM ?>: filterFrom, <?= VARS_URL_TO ?>: filterTo};
                                    if(method == '<?=METHOD_RENAME_FILTER?>'){
                                        params.<?=VARS_URL_FILTER_NAME?> = window.filterName;
                                        window.filterName = null;
                                    }
                                    onRenameFilter(params);
                                }
                            });
                        }
                        /**
                         * Cette fonction supprime le filtre séléctionné
                         */
                        function deleteFilter() {
                            var filterName = $('.selected').text().substring(1);
                            $.ajax({url: '/server/ajax/filtersData.php',
                                type: 'GET',
                                data: {<?= VARS_URL_FILTER_NAME ?>: filterName, <?= VARS_URL_FILTER_OWNER ?>: OWNER, <?= VARS_URL_METHOD ?>: '<?= METHOD_REMOVE_FILTER ?>'},
                                success: function (data) {
                                    var result = JSON.parse(data);
                                    if (result.code) {
                                        $('.selected').parent().remove();
                                    }
                                }});
                        }
                        /**
                         * Cette fonction ajoute un filtre
                         */
                        function addFilter() {
                            if ($('#filtersNameTable tr').length > 1) {
                                var lastRowID = parseInt($('#filtersNameTable tr:last').children().attr('id').toString().replace(/[^\d.,]+/, ''));
                                var rowNumber = "filtersNameRow" + (lastRowID + 1);
                            } else {
                                var rowNumber = 0;
                            }
                            appendToTable('#filtersNameTable', '', rowNumber);
                            $('#' + rowNumber).on('click', function (e) {
                                selectRowFilter('#' + e.target.id);
                            });
                            selectRowFilter('#' + rowNumber);
                            addOrRenameFilter('<?=METHOD_ADD_FILTER?>');
                        }

                        /**
                         * Cette fonction update le nom du filtre dans la base
                         * @param {array} params
                         */
                        function onRenameFilter(params) {
                            if (params.<?=VARS_URL_NEW_NAME?>.length > 1) {
                                $.ajax({url: '/server/ajax/filtersData.php',
                                    type: 'GET',
                                    data: params,
                                    success: function (data) {
                                        var result = JSON.parse(data)
                                        if (result.code) {
                                            var filterFrom = ($('#from').val().length === 8) ? $('#from').val() : '2016-01-01';
                                            var filterTo = ($('#to').val().length === 8) ? $('#to').val() : '2018-01-01';
                                            var newName = $('.newName').val();
                                            var idRow = $('.newName').attr('id');
                                            $('.newName').replaceWith('<td id="' + idRow + '"><span>' + newName + '</span></td>').addClass('selected');
                                            var newFilter = {<?= VARS_URL_NEW_NAME ?>: newName, <?= VARS_URL_FILTER_OWNER ?>: OWNER, <?= VARS_URL_FROM ?>: filterFrom, <?= VARS_URL_TO ?>: filterTo};
                                            window.filters.push(newFilter)
                                            $('#' + idRow).on('click', function (e) {
                                                selectRowFilter('#' + e.target.id);
                                            });
                                            canSelectFilter = true;
                                        }
                                    }});
                            } else {
                                $('.newName').select();
                                $('.newName').addClass('selected');
                            }
                        }
                        /**
                         * Cette fonction update la plage de dates dans la base
                         * @param {int} index
                         */
                        function updateDateRange(index) {
                            var filterName = $('.selected').text().substring(1);
                            var fromFilter = $('#from').val();
                            var toFilter = $('#to').val();
                            if (fromFilter !== window.filters[index].from || toFilter !== window.filters[index].to) {
                                $.ajax({url: '/server/ajax/filtersData.php',
                                    type: 'GET',
                                    data: {<?= VARS_URL_FILTER_NAME ?>: filterName, <?= VARS_URL_FILTER_OWNER ?>: OWNER, <?= VARS_URL_FROM ?>: fromFilter, <?= VARS_URL_TO ?>: toFilter},
                                    success: function () {
                                        window.filters[index].from = fromFilter;
                                        window.filters[index].to = toFilter;
                                    }
                                });
                            }
                        }
                        /**
                         * Cette fonction sélectionne un user
                         * @param {string} rowToSelect Le sélécteur
                         **/
                        function selectUser(rowToSelect) {
                            $(rowToSelect).toggleClass('selectedUser');
                        }
                        /**
                         * Cette fonction retire les élèves séléctionnés du filtre
                         **/
                        function removeUsersFromFilter() {
                            $('#filtersUsersInFilter').find('.selectedUser').each(function () {
                                var RowId = '#' + $(this).attr('id');
                                var userEmail = $(this).text();
                                var userInFilter = $('.selected').text().substring(1);
                                $.ajax({url: '/server/ajax/filtersData.php',
                                    type: 'GET',
                                    data: {<?= VARS_URL_EMAIL ?>: userEmail, <?= VARS_URL_FILTER_NAME ?>: userInFilter, <?= VARS_URL_FILTER_OWNER ?>: OWNER, <?= VARS_URL_METHOD ?>: '<?= METHOD_REMOVE_USER ?>'},
                                    success: function () {
                                        window.notInFilterEmails.push(userEmail);
                                        window.usersInFilter.splice(userEmail, 1);
                                        var id = $('#filterAllUsersTable tr td:last').parent().index() + 1;
                                        appendToTable('#filterAllUsersTable', userEmail, 'allUsersRow' + id);
                                        $('#allUsersRow' + id).on('click', function (e) {
                                            selectUser('#' + e.target.id);
                                        });
                                        $(RowId).parent().remove();
                                    }});
                            });
                        }

                        function addUsersToFilter() {
                            $('#filterAllUsersTable').find('.selectedUser').each(function () {
                                var RowId = '#' + $(this).attr('id');
                                var userEmail = $(this).text();
                                var userInFilter = $('.selected').text().substring(1);
                                $.ajax({url: '/server/ajax/filtersData.php',
                                    type: 'GET',
                                    data: {<?= VARS_URL_EMAIL ?>: userEmail, <?= VARS_URL_FILTER_NAME ?>: userInFilter, <?= VARS_URL_FILTER_OWNER ?>: OWNER, <?= VARS_URL_METHOD ?>: '<?= METHOD_ADD_USER ?>'},
                                    success: function () {
                                        window.usersInFilter.push(userEmail);
                                        window.notInFilterEmails.splice(userEmail, 1);
                                        var id = $('#filtersUsersInFilter tr td:last').parent().index() + 1;
                                        appendToTable('#filtersUsersInFilter', userEmail, 'userInFilterRowID' + id);
                                        $('#userInFilterRowID' + id).on('click', function (e) {
                                            selectUser('#' + e.target.id);
                                        });
                                        $(RowId).parent().remove();

                                    }});

                            });
                        }
                        function refreshTablesID() {
                            var i = 0;
                            $('#filtersUsersInFilter').find('td').each(function () {
                                $(this).attr('id', 'userInFilterRowID' + i);
                                i++;
                            });
                            i = 0;
                            $('#filterAllUsersTable').find('td').each(function () {
                                $(this).attr('id', 'userInFilterRowID' + i);
                                i++;
                            });

                        }

        </script>
    </body>
</html>
