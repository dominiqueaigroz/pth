<?php
/** @remark Commenter cette ligne pour vérifier la session. */
//define('_BYPASS_CHECKSESSION_', TRUE);

require_once './server/inc.all.php';
/** @remark On doit être au minimum Project Manager pour visualiser cette page */
define('ROLE', EROLE_ADMIN);
require_once './server/check.role.php';

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Jours de travail</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <link href="jsDatepick/css/jquery.datepick.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
        <script src="jsDatepick/js/jquery.plugin.js"></script>
        <script src="jsDatepick/js/jquery.datepick.js"></script>
        <script type="text/javascript" src="jsDatepick/js/jquery.datepick-fr.js"></script>
        <style>
            #calendars div.datepick{
                width: 655px !important; 
            }

        </style>
    </head>


    <body>


            <?php
            getNavbar();
            ?>
        <div class="main">
            <h1 class="titlePage">Administration</h1>

            

            <h1 class="titleTable" id="titleLeft">Plage horaire des vacances</h1>
            <div id="calendars" class="inlinePicker"></div>
            <button id="btnSave">Sauvegarder</button>
            <button id="btnClear">Clear</button>

        </div>


        <script>
            function SetDates(dates) {

                var ar = new Array();
                // 
                dates.forEach(function (value) {
                    var d = new Date(value.date);
                    ar.push(d);
                });
                if (ar.length > 0)
                    $("#calendars").datepick('setDate', ar);

            }
            $(document).ready(function () {
                // Création du calendrier
                $("#calendars").datepick({
                    monthsToShow: [2, 3],
                    multiSelect: 100,
                    showOnFocus: false,
                    minDate: new Date(2017, 1 - 1, 1),
                    maxDate: new Date(2019, 12 - 1, 31)

                });

                //ajax
                $.ajax({
                    method: 'POST',
                    url: 'server/ajax/GetHolidays.php',
                    // Passe en paramètre l'utilisateur concerné et
                    // le tableau des jours travaillés
                    //data: {'user': useremail, 'days': weekDays},
                    dataType: 'json',
                    success: function (data) {
                        var msg = '';

                        switch (data.ReturnCode) {
                            case 0 : // tout bon
                                SetDates(data.Result);
                                break;
                            case 1 : // problème de paramètre
                            case 2 : // problème de sauvegarde
                            default:
                                msg = data.Message;
                                break;
                        }

                        if (msg.length > 0)
                            $("#infoMsg").html(msg);
                    },
                    error: function (jqXHR) {
                        var msg = '';
                        switch (jqXHR.status) {
                            case 404 :
                                msg = "page pas trouvée. 404";
                                break;
                            case 200 :
                                msg = "probleme avec json. 200";
                                msg += "</BR>réponse: " + jqXHR.responseText;
                                break;

                        } // End switch

                        if (msg.length > 0)
                            $("#infoMsg").html(msg);
                    }
                }); // :End of ajax 






                $("#btnSave").click(function () {
                    var ar = new Array();
                    var dates = $("#calendars").datepick('getDate');
                    // 
                    dates.forEach(function (item) {
                        var y = item.getFullYear();
                        var m = item.getMonth() + 1;
                        var d = item.getDate();
                        var s = y + "-" + ((m <= 9) ? "0" : "") + m + "-" + d;
                        ar.push(s);
                    });

                    //ajax
                    $.ajax({
                        method: 'POST',
                        url: 'server/ajax/setHolidays.php',
                        // Passe en paramètre l'utilisateur concerné et
                        // le tableau des jours travaillés
                        data: {'days': ar},
                        dataType: 'json',
                        success: function (data) {
                            var msg = '';

                            switch (data.ReturnCode) {
                                case 0 : // tout bon
                                    SetDates(data.Result);
                                    break;
                                case 1 : // problème de paramètre
                                case 2 : // problème de sauvegarde
                                default:
                                    msg = data.Message;
                                    break;
                            }

                            if (msg.length > 0)
                                $("#infoMsg").html(msg);
                        },
                        error: function (jqXHR) {
                            var msg = '';
                            switch (jqXHR.status) {
                                case 404 :
                                    msg = "page pas trouvée. 404";
                                    break;
                                case 200 :
                                    msg = "probleme avec json. 200";
                                    msg += "</BR>réponse: " + jqXHR.responseText;
                                    break;

                            } // End switch

                            if (msg.length > 0)
                                $("#infoMsg").html(msg);
                        }
                    }); // :End of ajax 

                });

                $("#btnClear").click(function () {
                     $.ajax({
                        method: 'POST',
                        url: 'server/ajax/clearHolidays.php',
                        // Passe en paramètre l'utilisateur concerné et
                        // le tableau des jours travaillés
                        data: {},
                        dataType: 'json',
                        success: function (data) {
                            
                            var msg = '';

                            switch (data.ReturnCode) {
                                case 0 : // tout bon
                                    SetDates(data.Result);
                                    break;
                                case 1 : // problème de paramètre
                                case 2 : // problème de sauvegarde
                                default:
                                    msg = data.Message;
                                    break;
                            }

                            if (msg.length > 0)
                                $("#infoMsg").html(msg);
                        },
                        error: function (jqXHR) {
                            var msg = '';
                            switch (jqXHR.status) {
                                case 404 :
                                    msg = "page pas trouvée. 404";
                                    break;
                                case 200 :
                                    msg = "probleme avec json. 200";
                                    msg += "</BR>réponse: " + jqXHR.responseText;
                                    break;

                            } // End switch

                            if (msg.length > 0)
                                $("#infoMsg").html(msg);
                        }
                    }); // :End of ajax 
                   location.reload();
                });


            });




        </script>

    </body>
</html>
