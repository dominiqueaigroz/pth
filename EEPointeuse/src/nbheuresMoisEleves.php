<?php
//define('_BYPASS_CHECKSESSION_', TRUE);
require_once './server/inc.all.php';
/** @remark On doit être au minimum Project Manager pour visualiser cette page */
define('ROLE', EROLE_PM);
require_once './server/check.role.php';

$result = EDeltaManager::getDeltasByMonthForUser(ECTS_USERTIMESHEET_PROJECTCODE, null, null, null);
$cpt = 0;
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Nombre d'heures par élèves</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body>
                    <?php
                getNavbar();
            ?>
        <div class="main">
            <h1 class="titlePage">Nombre d'heures par élèves</h1>

            <h1 class="titleTable">Nombre d'heures faites par mois</h1>
            <table class="mainTable">
                <thead class="tableHead">
                    <tr>
                        <td>Email</td> 
                        <td>Mois</td> 
                        <td>Delta Δ</td>
                    </tr>
                </thead>
                <tbody class="tableBody">
                    <?php
                    if (count($result) > 0) :
                        foreach ($result as $delta):
                            $cpt++;
                            $date = $delta->date;
                            ?>
                            <tr>
                                <td><?= '<a href="recapEleveMois.php?email=' . $delta->email . "\"" ?>><?= $delta->email ?></a></td>
                                <td><?= '<a href="recapEleveMois.php?month=' . $date->format('Y-m') . "&email=" . $delta->email . "\"" ?>><?= date('F', mktime(0, 0, 0, $date->format('m'))) ?></a></td>
                                <td> 
                                    <?php
                                    if ($delta->deltas != false) {
                                        echo ($delta->deltas->invert) ? '-' : '+';
                                        echo $delta->deltas->h + $delta->deltas->d * 24 . ':' . $delta->deltas->i . ':' . $delta->deltas->s;
                                    }
                                    ?>
                                </td>
                            </tr> 
                            <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
                <tfoot class="tableFooter">

                </tfoot>
            </table>
        </div>
    </body>
</html>
