<?php
require_once './server/inc.all.php';
if (isset($_GET[VARS_URL_PROJECT_CODE])) {
    $projectCodes = filter_input(INPUT_GET, VARS_URL_PROJECT_CODE);
    $projCodes = explode(',', $projectCodes);
    $projects = array();
    foreach ($projCodes as $projCode) {
        $currentProjectCode = EProjectManager::getProjectInternalKey($projCode);
        array_push($projects, $currentProjectCode);
    }
} else {
    $projects = EProjectManager::getAllActiveProjects();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Liste projets</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://apis.google.com/js/platform.js"></script>        
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    </head>
    <body>
        <div id="infoMsg"></div>
        <?php getNavbar(); ?>
        <div class="main">
            <h1 class="titlePage">Administration</h1>
            <div id="from"></div>
            <div id="head">
                <h1 class="titleTable">Liste projets</h1>
            </div>
            <div id="listProjects">
                <?php
                foreach ($projects as $project) {
                    $firstPart = substr($project->internal_project_key, 0, 8);
                    $secondPart = substr($project->internal_project_key, 8, 15);
                    ?>
                    <fieldset class="project">
                        <legend><?= $project->label ?></legend>
                        <p class="projKey"><?= $firstPart ?></p>
                        <p class="projKey"><?= $secondPart ?></p>
                    </fieldset>
                <?php } ?>
            </div>
        </div>

    </body>
</html>
