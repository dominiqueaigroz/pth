<?php
require_once './server/inc.all.php';

$code = "";
$hours = 0;
$projectName = "";
$desc = "";
$dateEnd = "";

// Est-ce qu'on a cliqué sur le bouton annuler?
if (isset($_POST['cancel'])) {
    // Rediriger vers la page des projets
    header('refresh:2;url=teacherProject.php');
    exit();
} else
// Est-ce qu'on a cliqué sur le bouton save?
if (isset($_POST['save'])) {
    if (isset($_POST['ProjectCode']))
        $code = $_POST['ProjectCode'];
    if (isset($_POST['ProjectName']))
        $projectName = $_POST['ProjectName'];
    if (isset($_POST['description']))
        $desc = $_POST['description'];
    if (isset($_POST['Hours']) > 0)
        $hours = intval($_POST['Hours']);
    if (isset($_POST['finishDate']))
        $dateEnd = $_POST['finishDate'];
    // On va tester que tout est ok avant de demander la sauvegarde.
    if (strlen($code) > 0 &&
            strlen($projectName) > 0 &&
            strlen($desc) > 0 &&
            strlen($dateEnd) > 0 &&
            $hours > 0) {
        if (EProjectManager::modifyProject($code, $projectName, $desc, $hours, $dateEnd)) {
            // Redirect sur la page des projets
            header('refresh:2;url=teacherProject.php');
            exit();
        }
        // Afficher l'erreur sur la modif
    }
    // Afficher une erreur
} else {
    if (isset($_GET[VARS_URL_PROJECT_CODE]))
        $code = $_GET[VARS_URL_PROJECT_CODE];
    if (strlen($code) > 0) {
        // On va chercher les infos du projet
        $proj = EProjectManager::getInfoProject($code);
        if ($proj === false) {
            // Signaler l'erreur
            echo "A implémenter l'erreur";
            exit();
        }
        $projectName = $proj->label;
        $hours = $proj->forecastedHour;
        $dateEnd = substr($proj->forecastedEndDate, 0, 10);
        $desc = $proj->description;
    } else {
        // problème
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Edition</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <?php
    getNavbar();
    ?> 
    <body>

        <div class="main">
            <h1 class="titlePage">Administration</h1> 
            <div class="middleTableDiv">
                <div class="middleTable">

                    <h1 class="titleTable">Edition :</h1>

                    <form action="" method="POST">
                        <input name="ProjectCode" type="hidden" value="<?php echo $code; ?>"/>
                        <table cellspacing="10" cellpadding="10">
                            <tr>
                                <td>Nom du projet(*):</td>
                                <td class="rightTD"><input name="ProjectName" type="text" value="<?php echo $projectName; ?>"></td>
                            </tr>
                            <tr>
                                <td>Heures prévues(*):</td>
                                <td class="rightTD"><input name="Hours" type="number" value="<?php echo $hours; ?>"></td>
                            </tr>
                            <tr>
                                <td><a id="studentList" href="addUserToProject.php?projectCode=<?php echo $code; ?>&role=2">Listes des élèves(*)</a></td>
                                <td class="rightTD"><a id="managerList" href="addUserToProject.php?projectCode=<?php echo $code; ?>&role=4">Professeurs</a></td>
                            </tr>
                            <tr>
                                <td>Date de rendue(*):</td>
                                <td class="rightTD"><input name="finishDate" type="date" value="<?php echo $dateEnd; ?>"></td>
                            </tr>
                            <tr>
                                <td colspan="2">Description(*):</td>
                            </tr>
                            <tr>
                                <td colspan="2"><textarea name="description"><?php echo $desc; ?></textarea></td>
                            </tr>
                            <tr>
                                <td class="rightTD">
                                    <input type="submit" name="save" value="Enregistrer">                          
                                    <input type="submit" name="cancel" value="Annuler">
                                </td>
                            </tr>
                        </table>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            var popupStudentWindow = null;
            var popupmanagerWindow = null;

            $(document).ready(function () {


                $('#studentList').click(function (e) {
                    if (popupStudentWindow != null && !popupStudentWindow.closed) {
                        popupStudentWindow.focus()
                    } else {
                        popupStudentWindow = window.open($(this).attr('href'), 'Ajouter des élèves...', 'height=450px,width=500px');
                        if (window.focus) {
                            if (popupStudentWindow)
                                popupStudentWindow.focus()
                        }
                    }
                    // Ne pas exécuter le lien du href
                    return false;
                });
                $('#managerList').click(function (e) {
                    if (popupmanagerWindow != null && !popupmanagerWindow.closed) {
                        popupmanagerWindow.focus()
                    } else {
                        popupmanagerWindow = window.open($(this).attr('href'), 'Ajouter des professeurs...', 'height=450px,width=500px');
                        if (window.focus) {
                            if (popupmanagerWindow)
                                popupmanagerWindow.focus()
                        }
                    }
                    // Ne pas exécuter le lien du href
                    return false;
                });

            });
        </script>


    </body>
</html>
