<?php
require_once './server/inc.all.php';
/** @remark On doit être au minimum Project Manager pour visualiser cette page */
define('ROLE', EROLE_USER);
require_once './server/check.role.php';

$email = ESession::getInstance()->getEmail();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Consultation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body>
        <div id="infoMsg"></div>
        <?php
        getNavbar();
        ?> 
        <div class="main">
            <h1 class="titlePage">Administration</h1>

            <h1 class="titleTable">Gestionnaire d'erreur</h1>
            <table class="mainTable" >
                <thead>
                    <tr>
                        <td>Nom du projet</td> 
                        <td>Description</td> 
                        <td>Manageur</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody class="tableBody">
                    <?php
                    $cpt = 0;
                    $project = EProjectManager::getProjects($email);
                    foreach ($project as $value) {
                        echo '<tr><td><a href="ProjectModification.php">' . $value->label . '</a></td><td>' . $value->description . '</td><td>';
                        foreach (EProjectManager::getManagers($value->code) as $value2) {
                            $cpt++;
                            if (sizeof(EProjectManager::getManagers($value->code)) <= $cpt) {
                                echo $value2;
                            } else {
                                echo $value2 . ", ";
                            }
                        }
                        echo '</td><td> <input type="checkbox" class="checkBoxProject" value="' . $value->code . '"></td></tr>';
                    }
                    ?>
                </tbody>
                <tfoot class="tableFooter">

                </tfoot>
            </table>
        </div>
         <button  style="float: right; margin-right: 100px;" id="btnSumary">Information</button>
    
    </body>
</html>
<script>
            $('document').ready(function () {
                $('#btnSumary').click(function () {
                    var urlParam = "detailsProject.php?";
                    var oldPage = window.location.pathname;
                    var oldPage2 = oldPage.substring(1, oldPage.length);
                    var cpt = 0;
                        var urlParam = urlParam + "oldPage=" + oldPage2;
                    
                    $('input[type=checkbox]').each(function () {
                        
                        if (this.checked) {
                            urlParam += "&projectCode" + cpt + "=" + $(this).val() ;
                            cpt++;
                        }
                        
                    });
                    
                    
                    window.location = urlParam;
                });
        


            });
</script>

