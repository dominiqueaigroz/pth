<?php
/** @remark On ne check pas la session car on veut se déconnecter */
define('_BYPASS_CHECKSESSION_', TRUE);

require_once './server/inc.all.php';


ESession::getInstance()->destroy();

header('Location: '. getRootURL() .URL_AUTENTIFICATION);
