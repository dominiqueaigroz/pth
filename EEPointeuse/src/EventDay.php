<?php
require_once './server/inc.all.php';
/** @remark On doit être au minimum Project Manager pour visualiser cette page */
define('ROLE', EROLE_PM);
require_once './server/check.role.php';

// Par défaut on prend la date du jour
$dtEvents = date("Y-m-d");
// On regarde si dans l'url on a un paramètre pour la date demandée
if (isset($_POST['dtFrom'])) {
    $dtEvents = $_POST['dtFrom'];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Event day</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://apis.google.com/js/platform.js"></script>        
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    </head>
    <body>
        <?php
        getNavbar();
        ?>
        <div class="main">
            <div id="infoMsg"></div>
            <h1 class="titlePage">Administration</h1>
            <button type="button" id="btnShow">Choisir une date</button>
            <div id="from"></div>
            <div id="head">
                <h1 class="titleTable">Affichage des heures de pointage pour le 
                    <span id="dtDay"><?= strftime("%A %d %B %Y", strtotime($dtEvents)) ?></span>
                </h1>
            </div>
            <table class="mainTable" >
                <thead>
                    <tr>
                        <th>Email</th> 
                        <th>Entrée matin</th> 
                        <th>Sortie matin</th> 
                        <th>Entrée après-midi</th> 
                        <th>Sortie après-midi</th> 
                    </tr>
                </thead>
                <tbody class="tableBody">
                    <tr>
                        <td>En cours de chargement ....</td> 
                        <td>&nbsp;</td> 
                        <td>&nbsp;</td> 
                        <td>&nbsp;</td> 
                        <td>&nbsp;</td> 
                    </tr>
                </tbody>
                <tfoot class="tableFooter">
                </tfoot>
            </table>
        </div>
    </body>
    <script>
                // Pour savoir si on a déjà envoyé une requête
                var inProcess = false;
                $(document).ready(function () {

        var today = new Date();
                $("#from").hide();
                var from = $("#from")
                .datepicker({
                defaultDate: today,
                        changeMonth: true,
                        dateFormat: "yy-mm-dd",
                        numberOfMonths: 1,
                        showButtonPanel: true
                });
                console.log(from.val());
                var optionsDate = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
                $("#from").on("change", function () {
        $("#dtDay").empty();
                var d = new Date($("#from").val());
                options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                dtEventDay = d.toLocaleDateString("fr-CH", options);
                $("#dtDay").append(dtEventDay);
                loadEvents(from.val());
        }); //fin onChange


                $("#btnShow").on("click", function () {
        console.log(from.val());
                $("#from").show();
        });
                // On charge les événements du jour
                var d = today.getDate();
                var m = today.getMonth() + 1;
                var y = today.getFullYear();
                var str = y + '-' + m + '-' + d;
                loadEvents(str);
        }); //fin document ready



                function count(array) {
                var i = 0;
                        var count = 0;
                        while (array[i] !== "undefined" && i <= array.length + 1) {
                i++;
                        if (typeof array[i] == "undefined")
                        continue;
                        count++;
                }
                return count;
                }

        function closeTable(out, c = 0) {
        while (c > 0) {
        out += "<td>&nbsp;</td>";
                c--;
        }
        out += "</tr>";
                return out;
        }
        function getDate(element) {
        var date;
                try {
                date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
        date = null;
        }

        return date;
        }

        /**
         * Affiche les événements dans le tableau
         * @param {type} ar Le tableau contenant les événements du projet
         * @returns {boolean} True si ok.
         */
        function displayEvents(ar){

        $("tbody.tableBody tr").remove();
                var output = "";
                var previousEmail = "";
                var cpt = 0;
                var maxCol = 4;
                if (count(ar) > 0) {
        var dtEventDay = new Date();
                var dtEventTime = new Date();
                ar.forEach(function (v) {
                if (v.email !== previousEmail) {
                if (previousEmail.length > 0) {
                output = closeTable(output, maxCol - cpt);
                }
                output += "<tr><td>" + v.email + "</td>";
                        cpt = 0;
                        //var date = v.dt.date;
                }
                var d = new Date(v.dt.date);
                        //Heures, minutes et secondes des pointages
                        var options = { hour: '2-digit', minute: '2-digit', second: '2-digit' };
                        dtEventTime = d.toLocaleString("fr-CH", options);
                        //Jour, mois, année
                        options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                        dtEventDay = d.toLocaleDateString("fr-CH", options);
                        if (cpt < maxCol) {
                output += "<td>" + dtEventTime + "</td>";
                }
                cpt++;
                        previousEmail = v.email;
                });
                output = closeTable(output, maxCol - cpt);
        }
        else{
        output = '<tr><td>Aucun enregistrement</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
        }

        $(".tableBody").append(output);
        }

        /**
         * Charge les événements pour la date t donnée
         * @param {string} t Contient la date sous la forme YYYY-MM-DD
         * @returns {boolean} True ok
         */
        function loadEvents(t){
        // On relance la requête ajax si on est déjà en attente.
        if (inProcess)
                return;
                // Flag comme quoi on a envoyé la requête
                inProcess = true;
                $.ajax({
                method: 'POST',
                        url: 'server/ajax/geteventsforday.php',
                        data: {'dtFrom': t}, //on envoie la même date 2 fois car on veut les record d'un jour
                        dataType: 'json',
                        success: function (d) {
                        // Reset du flag comme quoi on a envoyé la requête
                        inProcess = false;
                                var msg = '';
                                switch (d.ReturnCode) {
                        case 0 : // tout bon
                                displayEvents(d.Data);
                                break;
                        }
                        /*     case 1 : // problème de paramètre
                         case 2 : // problème d'insertion
                         default:
                         msg = d.Message;
                         break;
                         if (msg.length > 0)
                         $("#infoMsg").html(msg); */
                        },
                        error: function (jqXHR) {
                        // Reset du flag comme quoi on a envoyé la requête
                        inProcess = false;
                                var msg = '';
                                switch (jqXHR.status) {
                        case 404 :
                                msg = "page pas trouvée. 404";
                                break;
                                case 200 :
                                msg = "probleme avec json. 200";
                                msg += "</BR>réponse: " + jqXHR.responseText;
                                break;
                        } // End switch
                        if (msg.length > 0)
                                $("#infoMsg").html(msg);
                        }
                });
                //fin ajax

                // Done
                return true;
        }
    </script>
</html>

