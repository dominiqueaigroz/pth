<?php
//define('_BYPASS_CHECKSESSION_', TRUE);
require_once './server/inc.all.php';
define('ROLE', EROLE_PM);
$isAdmin = (ESession::getInstance()->getRole() == EROLE_PM);
$email = ESession::getInstance()->getEmail();

//$email = "gawen.ackrm@eduge.ch";
//$projectCode = "JAOSMI8923NA78";
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Consultation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body>
        <div id="infoMsg"></div>
        <?php
        getNavbar();
        ?> 
        <div class="main">
            <h1 class="titlePage">Administration</h1>
            <?php
            $arrayProject = array();
            if (sizeof($_GET) == 1) {
                if ($_GET["oldPage"] == "teacherProject.php") {
                    $projects = EProjectManager::getAllActiveProjects();
                    if ($isAdmin) {
                        $projects= EProjectManager::getManagerProject($email);
                    }
                } else if ($_GET["oldPage"] == "studentProject.php") {
                    $projects = EProjectManager::getUserProjects($email);
                }
                foreach ($projects as $value) {
                    array_push($arrayProject, $value->code);
                }
            }

            foreach ($_GET as $value) {
                if ($value == "teacherProject.php" || $value == "studentProject.php") {
                    //rien
                } else {
                    array_push($arrayProject, $value);
                }
            }



            $cpt = 1;
            foreach ($arrayProject as $value) {
                $projectInfo = EProjectManager::getInfoProject($value);
                $event4project = EDeltaManager::getRecordFromProject($value);
                $totalDelta = EDeltaManager::getTotalDeltaEvents($event4project);
                if ($event4project === false || count($event4project) == 0) {
                    $none = "Pas d'enregistrement";
                } else {
                    $forecast = new DateInterval("PT" . $projectInfo->forecastedHour . "H");
                    $forc = new DateTime("00:00");
                    // On clone notre temps initial
                    $real = clone $forc;
                    $forc->add($forecast);
                    $real->add($totalDelta);
                    $remainingTime = $real->diff($forc, false);
                }


                echo "<div class='projectBox'>"
                . "<table><tr><td>" . $projectInfo->label . "</td></tr><tr><td>";
                foreach (EProjectManager::getManagers($projectInfo->code) as $value2) {
                    if (sizeof(EProjectManager::getManagers($projectInfo->code)) == $cpt) {
                        echo $value2;
                    } else {
                        echo $value2 . ", ";
                    }
                    $cpt++;
                }

                echo "</td></tr></table>"
                . "<hr width='100%' color='blue'>"
                . "<table><tr><td>Temps pour réaliser le projet : " . $projectInfo->forecastedHour . " H</td></tr>" ;
                if ($event4project === false || count($event4project) == 0){
                   echo "<tr><td>". $none ."</td></tr>";
                }
                else {
                    // On a des événements
                    echo $totalDelta->format('Temps passé : %d jours, %H heures, %I min, %S secs');
                    if($remainingTime->invert == 0 ){
                        $s = $remainingTime->format('%r%d jours, %H heures, %I min, %S secs');
                        echo "<tr><td>Temps restant : " .$s."</td></tr>";
                    }
                    else {
                        $s = $remainingTime->format('%d jours, %H heures, %I min, %S secs');
                        echo "<tr><td>Dépasser de : ".$s."</td></tr>";
                     }
                }
                echo  "</table></div>";
            }

            /*
              $email = "jonathan.brljq@eduge.ch";

              $project = EProjectManager::getProjects($email);
              foreach ($project as $value) {
              $event4project = EDeltaManager::getRecordFromProject($value->code);
              $totalDelta = EDeltaManager::getTotalDeltaEvents($event4project);
              echo "<div class='projectBox'>"
              . "<table><tr><td>" . $value->label . "</td></tr><tr><td>";
              foreach (EProjectManager::getManagers($value->code) as $value2) {
              if (sizeof(EProjectManager::getManagers($value->code)) == $cpt) {
              echo $value2;
              } else {
              echo $value2 . ", ";
              }
              $cpt++;
              }

              echo "</td></tr></table>"
              . "<hr width='100%' color='blue'>"
              . "<table><tr><td>Temps pour réaliser le projet </br>" . $value->forecastedHour . " H</td></tr><tr><td>Temps passé: " . $totalDelta->format('%d jours, %H heures, %I minutes, %S secondes') . " </td></tr><tr><td>Présence</td></tr><tr><td>Présence</td></tr></table>"
              . "</div>";
              }
             */
            ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    var searchParams = new URLSearchParams(location.search);
                    var oldPage = "";
                    if (searchParams.has("oldPage")) {
                        oldPage = searchParams.get("oldPage");
                    }
                    if (oldPage.length > 0) {
                        $urlOldPage = oldPage;
                    }
                });
            </script>
            <button onclick="window.location = $urlOldPage" style="float: left" id="btnBack">Retour</button>
        </div>




    </body>
</html>

