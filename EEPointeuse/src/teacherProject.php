<?php
require_once './server/inc.all.php';
define('ROLE', EROLE_PM);
require_once './server/check.role.php';
$email = ESession::getInstance()->getEmail();

$isAdmin = (ESession::getInstance()->getRole() == EROLE_ADMIN);
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Consultation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://apis.google.com/js/platform.js"></script> 
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="infoMsg"></div>
        <?php
        getNavbar();
        ?> 
        <div class="main">
            <h1 class="titlePage">Administration</h1>

            <h1 class="titleTable">Mes projets</h1>
            <table class="mainTable" >
                <thead>
                    <tr>
                        <td>Nom du projet</td> 
                        <td>Description</td> 
                        <td>Manageur</td>
                        <td>Action</td>
                    </tr>
                </thead>
                <tbody class="tableBody">

                    <?php
                    $cpt = 0;
                    if (ESession::getInstance()->getRole() == EROLE_PM) {
                        $project = EProjectManager::getManagerProject($email);
                    }
                    else if(ESession::getInstance()->getRole() == EROLE_ADMIN){
                        $project = EProjectManager::getAllActiveProjects();
                    }
                    if ($project !== null) {
    

                    foreach ($project as $proj) {
                        echo '<tr><td><a href="ProjectModification.php">' . $proj->label . '</a></td><td>' . $proj->description . '</td><td>';
                        $s = "";
                        foreach (EProjectManager::getManagers($proj->code) as $email) {
                            if (strlen($s)>0)
                                $s .= ' , ';
                            $s .= $email;
                        }
                        echo $s;
                        echo '</td><td> <input type="checkbox" class="checkBoxProject" value="' . $proj->code . '"></td></tr>';
                    }
                    }
                    ?>

                </tbody>
                <tfoot class="tableFooter">

                </tfoot>
            </table>
        </div>
        <button style="float: right;margin-right: 100px;" id="btnCode">Code</button>
        <button  style="float: right" id="btnSumary">Information</button>
        <button style="margin-left: 100px;" onclick="window.location = 'ProjectModification.php';">Créer un Projet</button>
    </body>
</html>

<script>
            $('document').ready(function () {
                $('#btnSumary').click(function () {
                    var urlParam = "detailsProject.php?";
                    var oldPage = window.location.pathname;
                    var oldPage2 = oldPage.substring(1, oldPage.length);
                    var cpt = 0;
                    var urlParam = urlParam + "oldPage=" + oldPage2;
                    
                    $('input[type=checkbox]').each(function () {
                        
                        if (this.checked) {
                            urlParam += "&projectCode" + cpt + "=" + $(this).val();
                            cpt++;
                        }
                        
                    });
                    
                    
                    window.location = urlParam;
                });
                $('#btnCode').click(function () {
                    var urlParam = "";
                    var url = 'projectsList.php';
                    var cpt = 0;
                    $('input[type=checkbox]').each(function () {
                        if (this.checked) {
                            urlParam += $(this).val() + ",";
                        }
                        cpt++;
                    });
                    if (urlParam.length > 0) {
                        urlParam = url + '?projectCode=' + urlParam.substring(0, urlParam.length - 1);
                        window.location = urlParam;
                    } else {
                        window.location = url;
                    }
                });
                
                
            });
</script>



