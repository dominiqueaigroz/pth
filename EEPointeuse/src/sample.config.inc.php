<?php
/**
 * Copier ce fichier dans le répertoire
 * /server/config
 * Le renommer config.inc.php
 * @remark Le fichier config.inc.php n'est pas synchroniser dans git
 */
// Le Type de la base de donnée
define ('EDB_DBTYPE', "mysql");
// Adresse de l'hôte
define ('EDB_HOST', "localhost");
// Numéro de Port
define ('EDB_PORT', "3306");
// Nom de la base de donnée
define ('EDB_DBNAME', "eetimechecking");
// Nom du compte admin de la base de donnée
define ('EDB_USER', "mettre le username");
// Mot de passe du compte admin
define ('EDB_PASS', "mettre le password");

?>