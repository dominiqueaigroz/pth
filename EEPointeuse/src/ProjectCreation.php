<?php
require_once './server/inc.all.php';

$projectCode = EProjectManager::generateInternalProjectKey();
$projectName = "";
$hours = 100;
$dateEnd = "";
$desc = "";

if (isset($_REQUEST['ProjectName']))
    $projectName = $_REQUEST['ProjectName'];
if (isset($_REQUEST['Hours']))
{
    if (strlen($_REQUEST['Hours'])>0)
        $hours = intval($_REQUEST['Hours']);
    else
        $hours = "";
}
if (isset($_REQUEST['finishDate']))
    $dateEnd = $_REQUEST['finishDate'];
if (isset($_REQUEST['description']))
    $desc = $_REQUEST['description'];

// Test si valide pour créer le projet
if (strlen($projectCode) > 0 && 
    strlen($projectName) > 0 &&
    strlen($dateEnd) > 0 &&
    strlen($desc) > 0 &&
    strlen($hours) > 0 ) {

    if (EProjectManager::addProject($projectCode, $projectName, $desc, $hours, $dateEnd)) {
        $_POST["notif"] = '<br><div class="alert alert-success">Votre projet a bien été crée</div>';
        header('refresh:2;url=ProjectCreation.php');
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Creation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
<?php
getNavbar();
?> 
    <body>

        <div class="main">
            <h1 class="titlePage">Administration</h1> 
<?php
if (isset($_POST["notif"])) {
    echo $_POST["notif"];
}
?>

            <div class="middleTableDiv">
                <div class="middleTable">

                    <h1 class="titleTable">Creation :</h1>
                    <form>

                        <table cellspacing="10" cellpadding="10">
                            <tr>
                                <td>Nom du projet(*):</td>
                                <td class="rightTD"><input name="ProjectName" type="text" value="<?php echo $projectName;?>"></td>
                            </tr>
                            <tr>
                                <td>Heures prévues(*):</td>
                                <td class="rightTD"><input name="Hours" type="number" value="<?php echo $hours;?>"></td>
                            </tr>
                            <tr>
                                <td><a id="studentList" href="addUserToProject.php?projectCode=<?php echo $projectCode; ?>&role=2">Listes des élèves(*)</a></td>
                                <td class="rightTD"><a id="managerList" href="addUserToProject.php?projectCode=<?php echo $projectCode; ?>&role=4">Professeurs</a></td>
                            </tr>
                            <tr>
                                <td>Date de rendue(*):</td>
                                <td class="rightTD"><input name="finishDate" type="date" value="<?php echo $dateEnd;?>"></td>
                            </tr>
                            <tr>
                                <td colspan="2">Decription(*):</td>
                            </tr>
                            <tr>
                                <td colspan="2"><textarea name="description"><?php echo $desc;?></textarea></td>
                            </tr>
                            <tr>
                                <td class="rightTD">
                                    <input type="submit" name="save" value="Enregistrer">                          
                                    <input type="button" name="" value="Annuler">
                                </td>
                            </tr>
                        </table>
                    </form>

                </div>
            </div>
        </div>
        <script type="text/javascript">
            var popupStudentWindow = null;
            var popupmanagerWindow = null;

            $(document).ready(function () {


                $('#studentList').click(function (e) {
                    if (popupStudentWindow !== null && !popupStudentWindow.closed) {
                        popupStudentWindow.focus()
                    } else {
                        popupStudentWindow = window.open($(this).attr('href'), 'Ajouter des élèves...', 'height=450px,width=500px');
                        if (window.focus) {
                            if (popupStudentWindow)
                                popupStudentWindow.focus()
                        }
                    }
                    // Ne pas exécuter le lien du href
                    return false;
                });
                $('#managerList').click(function (e) {
                    if (popupmanagerWindow !== null && !popupmanagerWindow.closed) {
                        popupmanagerWindow.focus()
                    } else {
                        popupmanagerWindow = window.open($(this).attr('href'), 'Ajouter des professeurs...', 'height=450px,width=500px');
                        if (window.focus) {
                            if (popupmanagerWindow)
                                popupmanagerWindow.focus()
                        }
                    }
                    // Ne pas exécuter le lien du href
                    return false;
                });

            });
        </script>
    </body>
</html>
