<?php
//define('_BYPASS_CHECKSESSION_', TRUE);
require_once './server/inc.all.php';
/** @remark On doit être au minimum User pour visualiser cette page */
define('ROLE', EROLE_USER);
require_once './server/check.role.php';

//$email = "gawen.ackrm@eduge.ch";
//$projectCode = "JAOSMI8923NA78";
$email = (isset($_GET[VARS_URL_EMAIL])) ? filter_input(INPUT_GET, VARS_URL_EMAIL) : ESession::getInstance()->getEmail();
$projectCode = (isset($_GET[VARS_URL_PROJECT_CODE])) ? filter_input(INPUT_GET, VARS_URL_PROJECT_CODE) : null;
$month = (isset($_GET[VARS_URL_MONTH])) ? filter_input(INPUT_GET, VARS_URL_MONTH) : null;
$from = null;
$to = null;
if (strlen($month) > 0) {
    $from = $month . '-1';
    $to = $month . '-30';
}
$result = EDeltaManager::getRecordWithDelta($projectCode, $email, $from, $to);
$strTotalMois = 'Aucun pointage';
$cpt = 0;
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Consultation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body>
        <?php
        getNavbar();
        ?>
        <div class="main">
            <h1 class="titlePage">Récap' des heures</h1>

            <h1 class="titleTable"><?= (strlen($email) > 0) ? "Nombre d'heures faites par $email" : "Nombre d'heures faites par tous les élèves" ?></h1>
            <table class="mainTable" >
                <thead>
                <td>Mois</td>
                </td>
                <td>Delta Δ</td>
                </thead>
                <tbody class="tableBody">
                    <?php
                    if (count($result) > 0) {
                        $arrDeltaSemaine = array();
                        $arrDeltaMois = array();
                        $nbSemaine = 1;
                        $nextWeekNumber = getWeekNumber($result[0]->dt->format('Y-m-d'));
                        $weekNumber = $nextWeekNumber;
                        foreach ($result as $delta):
                            $weekNumber = $nextWeekNumber;
                            //Si on est au dernier tour de boucle on set à $weekNumber pour afficher la  dernière semaine
                            $nextWeekNumber = getWeekNumber($result[($cpt < count($result) - 1) ? $cpt + 1 : count($result) - 1]->dt->format('Y-m-d'));
                            $cpt++;


                            if ($delta->delta != false):
                                $deltaTime = ($delta->delta->invert) ? '-' : '+';
                                if ($delta->delta->h < 1)
                                    $deltaTime .= $delta->delta->i . 'min ' . $delta->delta->s . 'sec';
                                else
                                    $deltaTime .= $delta->delta->h + $delta->delta->d * 24 . 'h ' . $delta->delta->i . 'min ' . $delta->delta->s . 'sec';
                                $date = $delta->dt->format('d ') . date('F', mktime(0, 0, 0, $delta->dt->format('m'))) . $delta->dt->format(' 20y');
                                array_push($arrDeltaSemaine, $delta);
                                array_push($arrDeltaMois, $delta);
                                ?>

                                <tr>
                                    <td><a href="RecordByDay.php?email=<?= $email ?>&day=<?= $delta->dt->format('Y-m-d') ?>"><?= $date ?></a></td>
                                    <td><?= $deltaTime ?></td>
                                </tr>
                                <?php
                                if ($weekNumber !== $nextWeekNumber || $result[($cpt < count($result) - 1) ? $cpt + 1 : count($result) - 1]->dt->format('Y') !== $delta->dt->format('Y') || array_search($delta, $result) == count($result) - 1):
                                    $totalSemaine = EDeltaManager::calculateDeltaTime($arrDeltaSemaine);
                                    $strTotalSemaine = ($totalSemaine->invert) ? '-' : '+';
                                    if ($totalSemaine->h < 1)
                                        $strTotalSemaine .= $totalSemaine->i . 'min ' . $totalSemaine->s . 'sec';
                                    else
                                        $strTotalSemaine .= $totalSemaine->h + $totalSemaine->d * 24 . 'h ' . $totalSemaine->i . 'min ' . $totalSemaine->s . 'sec';
                                    ?>
                                    <tr>
                                        <td><?= 'SEMAINE ' . $nbSemaine ?></td>
                                        <td><?= $strTotalSemaine ?></td>
                                    </tr>
                <?php
                $totalDeltaMois = array();
                $totalDeltaSemaine = array();
                $arrDeltaSemaine = array();
                $nbSemaine++;
            endif;
        endif;
    endforeach;
    //Si arrDeltaSemaine n'est pas vide, c'est qu'il reste un total d'une semaine à 
    if (count($arrDeltaSemaine) > 0) {
        $totalSemaine = EDeltaManager::calculateDeltaTime($arrDeltaSemaine);
        $strTotalSemaine = ($totalSemaine->invert) ? '-' : '+';
        $strTotalSemaine .= $totalSemaine->h + $totalSemaine->d * 24 . 'h ' . $totalSemaine->i . 'min ' . $totalSemaine->s . 'sec';
        ?>
                            <tr>
                                <td><?= 'SEMAINE ' . $nbSemaine ?></td>
                                <td><?= $strTotalSemaine ?></td>
                            </tr>
        <?php
    }
    $totalMois = EDeltaManager::calculateDeltaTime($arrDeltaMois);
    if ($totalMois !== false) {
        $strTotalMois = ($totalMois->invert) ? '-' : '+';
        $strTotalMois .= $totalMois->h + $totalMois->d * 24 . 'h ' . $totalMois->i . 'min ' . $totalMois->s . 'sec';
    }
}
?>
                </tbody>
                <tfoot class="tableFooter">
                    <tr>
                        <td>
                            Δ MOIS
                        </td>
                        <td><?= $strTotalMois ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </body>
</html>

