<?php
define('_SERVER_', true);
require_once './server/inc.all.php';

if (isset($_GET[VARS_URL_PROJECT_CODE])) {
    $projectCode = $_GET[VARS_URL_PROJECT_CODE];
} else {
    //@TODO Séléction projet
    $projectCode = '4N1MJS14I5JTSEV3L';
}


$users = array();
$usersInProject = array();
$roleLabel = "Aucun rôle correspondant.";
$role = -1;
if (strlen($_GET['role']) > 0) {
    $role = intval($_GET['role']);
    switch ($role) {
        case EROLE_ADMIN: //admin
        case EROLE_PM: //chef de proj
        case EROLE_USER:  //élève
        case EROLE_UNKNOWN:  //unkown
            $users = EUserManager::getUsersFromRole($role);
            $r = EUserManager::getRoleName($role);
            if ($r !== false)
                $roleLabel = $r->labelRole;
            break;
        case 4:
            //admin + chef de projet
            $adminUsers = EUserManager::getUsersFromRole(EROLE_ADMIN);
            $rAdmin = EUserManager::getRoleName(EROLE_ADMIN);
            $pmUsers = EUserManager::getUsersFromRole(EROLE_PM);
            $rPM = EUserManager::getRoleName(EROLE_PM);
            $roleLabel = $rAdmin->labelRole . " & " . $rPM->labelRole;
            $users = array_merge($adminUsers, $pmUsers);
            break;
        default:
            break;
    }
    // On récupère les user du projet
    switch ($role) {
        case EROLE_USER:  //élève
        case EROLE_UNKNOWN:  //unkown
            $usersInProject = EProjectManager::getUsers($projectCode);
            break;
        case EROLE_ADMIN: //admin
        case EROLE_PM: //chef de proj
        case 4:
            //admin + chef de projet
            $usersInProject = EProjectManager::getManagers($projectCode);
            break;
        default:
            break;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Chef de projet</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>

    <body>
        <div class="main">
            <div class="middleTableDiv">
                <div class="addUserTable" >

                    <h1 class="titleTable"><?= $roleLabel ?></h1>

                    <div id="tableAddStudent">
                        <table id="tableManager">
                            <tr>
                                <td>Email</td>
                                <td>Sélection</td>
                            </tr>
                            <?php
                            $i = 0;
                            foreach ($users as $email) {
                                $bState = (in_array($email->email, $usersInProject)) ? "checked" : "";
                                ?>

                                <tr id="<?= $i ?>">
                                    <td><?= $email->email ?></td>
                                    <td><input type="checkbox" class="checkBoxProject" <?= $bState ?> value="<?= $email->email ?>"></td>
                                </tr>
                                <?php
                                $i += 1;
                            }
                            ?>
                        </table>
                    </div>
                    <table cellspacing="10" cellpadding="10">
                        <tr>

                            <td class="rightTD">
                                <input type="button" id="save" value="Enregistrer">
                            </td>
                        </tr>
                    </table>
                    <div id="searchUser">
                        <p>Rechercher un utilisateur</p>
                        <input type="text" id="searchBar" onkeyup="getUsers()"/>
                        <table id="searchResult">
                            <!--<tr>
                                <td>skyler.wht@eduge.ch</td>
                                <td><a href="#">Ajouter</a></td>
                            </tr>-->
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <script>

            $(document).ready(function () {
                $('#save').click(function () {

                    var users = new Array();
                    $('.checkBoxProject').each(function (i) {
                        if ($(this).prop('checked')) { // checked
                            users.push($(this).val());
                        }  // #endif
                    });  // #end each


                    //ajax
                    $.ajax({
                        method: 'POST',
                        url: 'server/ajax/users2Project.php',
                        // Passe en paramètre l'utilisateur concerné et
                        // le tableau des jours travaillés
                        data: {'projCode': "<?= $projectCode ?>", 'role': <?= $role ?>, 'users': users},
                        dataType: 'json',
                        success: function (data) {
                            var msg = '';

                            switch (data.ReturnCode) {
                                case 0 : window.close();
                                    break;
                                case 1 : // problème de paramètre
                                case 2 : // problème de sauvegarde
                                default:
                                    msg = data.Message;
                                    break;
                            }

                            if (msg.length > 0)
                                $("#infoMsg").html(msg);
                        },
                        error: function (jqXHR) {
                            var msg = '';
                            switch (jqXHR.status) {
                                case 404 :
                                    msg = "page pas trouvée. 404";
                                    break;
                                case 200 :
                                    msg = "probleme avec json. 200";
                                    msg += "</BR>réponse: " + jqXHR.responseText;
                                    break;

                            } // End switch

                            if (msg.length > 0)
                                $("#infoMsg").html(msg);
                        }
                    }); // :End of ajax 

                });
            }); // #end document ready


            /**
             * Ajoute une ligne à la table de résultats de la recherche
             * @param {string} textToAppend
             * @param {string} id
             */
            function appendToSearchResultsTable(textToAppend, id = null) {
                if (id !== null) {
                    id = 'id="' + id + '"';
                }
                $('#searchResult').append('<tr><td>' + textToAppend + '</td><td><a href="#" class="add" ' + id + '>' + 'Ajouter' + '</a></td>');
            }
            /**
             * Ajoute une ligne à la table des managers
             * @param {string} textToAppend
             * @param {string} id
             */
            function appendToProjectEmailTable(textToAppend, id = null) {
                if (id !== null) {
                    id = 'id="' + id + '"';
                }
                $('#tableManager').append('<tr><td><p>' + textToAppend + '</p></td><td><input type="checkbox" class="checkBoxProject" value="' + textToAppend + '"></td></tr>');
                $('.add').on('click', function (e) {
                    addManager(e.target.id, textToAppend);
                });
            }
            /**
             * Supprime toutes les lignes d'un tableau
             * @param {string} tableSelector
             */
            function clearTable(tableSelector) {
                $('#' + tableSelector).empty();
            }
        </script>
    </body>
</html>
