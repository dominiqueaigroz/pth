<?php
//define('_BYPASS_CHECKSESSION_', TRUE);
require_once './server/inc.all.php';
/** @remark On doit être au minimum User pour visualiser cette page */
define('ROLE', EROLE_USER);
require_once './server/check.role.php';


//$email = "gawen.ackrm@eduge.ch";
//$projectCode = "JAOSMI8923NA78";
if (ESession::getInstance()->getRole() <= EROLE_PM || defined('_BYPASS_CHECKSESSION_')) {
    $email = (isset($_GET[VARS_URL_EMAIL])) ? filter_input(INPUT_GET, VARS_URL_EMAIL) : null;
} else {
    $email = ESession::getInstance()->getEmail();
}
$projectCode = (isset($_GET[VARS_URL_PROJECT_CODE])) ? filter_input(INPUT_GET, VARS_URL_PROJECT_CODE) : null;
$month = (isset($_GET[VARS_URL_MONTH])) ? filter_input(INPUT_GET, VARS_URL_MONTH) : null;
$from = null;
$to = null;
if (strlen($month) > 0) {
    $from = $month . '-1';
    $to = $month . '-30';
}
$strTotalMois = 'Aucun pointage';
$result = EDeltaManager::getRecordWithDelta($projectCode, $email, $from, $to);
$cpt = 0;
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Consultation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body>
        <?php
getNavbar();
?>
        <div class="main">
            <h1 class="titlePage">Récap' des heures par mois</h1>

            <h1 class="titleTable"><?= (strlen($email) > 0) ? "Nombre d'heures faites par $email" : "Nombre d'heures faites par tous les élèves" ?></h1>
            <table class="mainTable" >
                <thead>
                <td>Mois</td> 
                <td>Delta Δ</td>
                </thead>
                <tbody class="tableBody">
                    <?php
                    $arrDeltaMois = array();
                    $totalDeltaMois = array();
                    $monthNumber = null;
                    $previousMonth = null;
                    foreach ($result as $delta):
                        if ($delta->delta != false):
                            if ($monthNumber === null) {
                                $monthNumber = (isset($monthNumber)) ? $monthNumber : $delta->dt->format('Y-m');
                                $previousMonth = $monthNumber;
                            }
                            $cpt++;
                            $previousMonth = $monthNumber;
                            $monthNumber = $delta->dt->format('Y-m');
                            if ($previousMonth !== $monthNumber):

                                $totalMois = EDeltaManager::calculateDeltaTime($arrDeltaMois);
                                if ($totalMois !== false) {
                                    $strTotalMois = ($totalMois->invert) ? '-' : '+';
                                    $strTotalMois .= $totalMois->format('%H:%I:%S');
                                    $month = $delta->dt->format('Y-m');
                                    ?>
                                <td><a href='RecapDeltaEleve.php?email=<?= $email ?>&month=<?= $month ?>'><?= date('F', mktime(0, 0, 0, $delta->dt->format('m'))) ?></a></td>
                                <td><?= $strTotalMois ?></td>
                                </tr>
                                <?php
                                $arrDeltaMois = array();
                            }
                        endif;
                        array_push($arrDeltaMois, $delta);
                        array_push($totalDeltaMois, $delta);
                    endif;
                endforeach;
                if (count($arrDeltaMois) != 0):
                    $totalMois = EDeltaManager::calculateDeltaTime($arrDeltaMois);
                    $strTotalMois = ($totalMois->invert) ? '-' : '+';
                    $strTotalMois .= $totalMois->format('%h:%i:%s');
                    $month = $result[$cpt]->dt->format('Y-m');
                    ?>
                    <tr>            
                        <td><a href='RecapDeltaEleve.php?email=<?= $email ?>&month=<?= $month ?>'><?= date('F', mktime(0, 0, 0, $result[$cpt]->dt->format('m'))) ?></a></td>
                        <td><?= $strTotalMois ?></td>
                    </tr><?php
            endif;
            $totalMois = EDeltaManager::calculateDeltaTime($totalDeltaMois);
                ?>
                </tbody>
                <tfoot class="tableFooter">
                    <tr>
                        <td>
                            Δ MOIS
                        </td>
                        <?php
                        if ($totalMois !== false) {
                            $strTotalMois = ($totalMois->invert == 1) ? '-' : '+';
                            $strTotalMois .= $totalMois->format('%h:%i:%s');
                            ?>
<?php } ?>
                        <td><?= $strTotalMois ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </body>
</html>

