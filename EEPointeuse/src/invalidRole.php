<?php
require_once './server/inc.all.php';
$redirectTime = 2500;
?>

<!DOCTYPE html>

<html>
    <head>
        <title>Consultation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>
    <body onload="redirect(<?= $redirectTime ?>)" style="text-align:center;">
        <div class="main">
            <h1>Rôle invalide</h1>
            <h2>Vous serez redirigé dans <?= $redirectTime / 1000 ?> s</h2>
        </div>
        <script>
            function redirect(time) {
                setTimeout(function () {
                    window.location = '<?= substr(getHomePage(ESession::getInstance()->getRole()), 1)?>';
                }, <?=$redirectTime?>);
            }
        </script>
    </body>
</html>

