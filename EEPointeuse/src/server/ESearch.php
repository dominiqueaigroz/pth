<?php

/**
 * Description of ESearch
 * Cette fonction sert à faire une recherche avec des
 * paramètres
 * 
 * @author BURNANDL
 */
class ESearch {
    
    /**
     * La méthode principal qui permet de
     * faire la recherche
     * (doit être appelé après la méthode prepare)
     * @param ESearchParam $searchParam Contient les paramètres pour la recherche.
     * @return array of ESearchResult Contient un tableau de résultat.
     */
    public static function search($searchParam)
    {
        
        $sql = "SELECT USERS.EMAIL_PK, ROLES.LABEL, PROJECTS.LABEL, EVENTS.DT_PK FROM USERS, ROLES, PROJECTS, EVENTS WHERE USERS.ROLES_CODE_PK = ROLES.CODE_PK AND USERS.EMAIL_PK = EVENTS.USERS_EMAIL_PK AND PROJECTS.CODE_PK = EVENTS.PROJECTS_CODE_PK";
        // Ai-je des paramètres de filtre?
        if (isset($searchParam)){
            if(strlen($searchParam->dtFrom) > 0)
            {
                $sql .= " AND events.DT_PK > " . $searchParam->dtFrom;
            }
            if(strlen($searchParam->dtTo) > 0)
            {
               $sql .= " AND events.DT_PK < " . $searchParam->dtTo; 
            }
            if(strlen($searchParam->role) > 0)
            {
                $sql .= " AND roles.CODE_PK=" . $searchParam->role;
            }
            if(strlen($searchParam->filter) > 0)
            {
                $filter = $searchParam->filter;
                $sql .= " AND USERS.EMAIL_PK LIKE \"%$filter%\" OR PROJECTS.LABEL LIKE \"%$filter%\" OR PROJECTS.DESCRIPTION LIKE \"%$filter%\" OR ROLES.LABEL LIKE \"%$filter%\"";
            }
        }
        
        $sql .= " ORDER BY EVENTS.DT_PK";
        
        $stmt = EDatabase::prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        $response = array();

        foreach ($result as $rec)
        {
            $searchResult = new ESearchResult(validateString($rec["USERS.EMAIL_PK"]),
                                              validateString($rec["PROJECTS.LABEL"]),
                                              validateString($rec["EVENTS.DT_PK"]),
                                              validateString($rec["ROLES.LABEL"]));
            array_push($response, $searchResult);
        }
        // Retourne le tableau de résultat
        return $response;

    }
    
    /**
     * Validé une chaine de caractère venant de la BD
     * @param string $val Contenu du champ a validé normalement type string
     * @return string contenu du champ ou chaine vide si le champ est invalide
     */
    private static function validateString($val)
    {
        return isset($val) ? $val : "";
    }
    
    
}