<?php
/**
 * Description of EFilter
 * Cette classe contient les informations sur les filtres
 * @author Administrateur
 */
class EFilter {
    /** @var string Le nom du filtre*/
    public $name;
    /** @var string L'email du propriétaire du filtre*/
    public $ownerEmail;
    /** @var string La date de départ du filtre*/
    public $from;
    /** @var string La date de fin du filtre*/
    public $to;
    
}
