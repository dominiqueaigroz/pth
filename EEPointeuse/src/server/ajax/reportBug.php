<?php
define("_SERVER_", TRUE);

require_once './../../server/inc.all.php';


// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');


$type = "";
if (isset($_POST["type"]))
    $type = $_POST["type"];
$description = "";
if (isset($_POST["description"]))
    $description = $_POST["description"];


$bugReport = new EBugReport();
$bugReport->type = intval($type);
$bugReport->description = $description;

if (EBugManager::saveBug($bugReport))
{
    echo '{ "ReturnCode": 0}';
    exit();
}

// Fail
echo '{ "ReturnCode": 2, "Message": "Erreur d\'insertion du bug report"}';
exit();

