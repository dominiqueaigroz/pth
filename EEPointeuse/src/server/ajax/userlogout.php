<?php
define('_SERVER_', TRUE);

require_once './../../server/inc.all.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

    // On va remplir la session
    ESession::getInstance()->destroy();
    // OK
    echo '{ "ReturnCode": 0, "URL": "index.html"}';
    exit();
?>