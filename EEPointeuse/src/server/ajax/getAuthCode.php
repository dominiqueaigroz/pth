<?php
/*
 * Page pour récupérer le code de connexion
 */
define("_SERVER_", TRUE);

require_once './../../server/inc.all.php';

header('Content-Type: application/json');

$key = ECheckingCodeManager::getTimeCheckingCode();
if ($key !== false) {
    $result = array(
        'key' => $key,
        'returnCode' => '0',
        'message' => ''
    );
} else {
    $result = array(
        'returnCode' => '1',
        'message' => 'Erreur'
    );
}
echo json_encode($result);