<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define('_SERVER_', TRUE);
require_once './../../server/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère les paramètres
$role = "";
if (isset($_POST['role']))
    $role = $_POST['role'];
$projectCode = "";
if (isset($_POST['projCode']))
    $projectCode = $_POST['projCode'];
$users = array();
if (strlen($role) > 0 && strlen($projectCode) > 0) {
    
    switch (intval($role)) {
        case EROLE_USER:  //élève
        case EROLE_UNKNOWN:  //unkown
           $users = EProjectManager::getUsers($projectCode);
            break;
        case EROLE_ADMIN: //admin
        case EROLE_PM: //chef de proj
        case 4:
            //admin + chef de projet
           $users = EProjectManager::getManagers($projectCode);
            break;
        default:
            break;
    }
    $jsn = json_encode($users, JSON_UNESCAPED_UNICODE); //JSON_UNESCAPED_UNICODE nécessaire !
    $code = json_last_error();
    if ($code != JSON_ERROR_NONE ||
            $jsn === false || $jsn === NULL) {
        echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json (' . $code . ')}';
        exit();
    }
    // OK
    echo '{ "ReturnCode": 0, "Data": ' . $jsn . ' }';
    exit();
}


// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Il manque des paramètres"}';