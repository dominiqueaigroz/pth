<?php
define('_SERVER_', TRUE);
require_once './../../server/inc.all.php';
header('Content-Type: application/json');
if (isset($_GET[VARS_URL_EMAIL]) && isset($_GET[VARS_URL_PROJECT_CODE])) {
    $email = filter_input(INPUT_GET, VARS_URL_EMAIL);
    $projectCode = filter_input(INPUT_GET, VARS_URL_PROJECT_CODE);
}
if (isset($email) && isset($projectCode)) {
    $result['resultCode'] = EProjectManager::deleteManager($projectCode, $email);
    echo json_encode($result);
}else{
    $result['resultCode'] = 0;
    $result['message'] = "Pas assez de paramètres";
    echo json_encode($result);
}
