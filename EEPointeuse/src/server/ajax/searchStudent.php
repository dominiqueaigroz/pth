<?php
define('_SERVER_', TRUE);
require_once './../../server/inc.all.php';
header('Content-Type: application/json');
if (isset($_GET[VARS_URL_EMAIL])) {
    $email = filter_input(INPUT_GET, VARS_URL_EMAIL);
}
if(isset($email)){
    $result = EUserManager::getUsersContaining($email);
    echo json_encode($result);
}else{
    $result['resultCode'] = 0;
    $result['message'] = "Pas assez de paramètres";
    echo json_encode($result);
}
