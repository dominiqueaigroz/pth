<?php
define('__SERVER__', true);
require_once './../../server/inc.all.php';
$args = array();
foreach ($_GET as $key => $value) {
    $paramValue = filter_input(INPUT_GET, $key);
    if (strlen($paramValue) > 0) {
        $args[$key] = $paramValue;
    }
}
$filterOwner = ESession::getInstance()->getEmail();
if (isset($args[VARS_URL_METHOD]) && (isset($args[VARS_URL_FILTER_NAME]) || isset($args[VARS_URL_NEW_NAME])) && strlen($filterOwner) > 0) {
    if ($args[VARS_URL_METHOD] == METHOD_ADD_FILTER && isset($args[VARS_URL_FROM]) && isset($args[VARS_URL_TO]) && isset($args[VARS_URL_NEW_NAME])) {
        $result = array();
        $result['code'] = (EHelperFilter::createFilter($args[VARS_URL_NEW_NAME], $filterOwner, $args[VARS_URL_FROM], $args[VARS_URL_TO]));
        echo json_encode($result);
    } else if ($args[VARS_URL_METHOD] == METHOD_RENAME_FILTER && isset($args[VARS_URL_NEW_NAME]) && isset($args[VARS_URL_FILTER_NAME])) {
        if (strlen($args[VARS_URL_NEW_NAME]) > 2) {
            $result = array();
            $result['code'] = (EHelperFilter::renameFilter($args[VARS_URL_FILTER_NAME], $args[VARS_URL_NEW_NAME], $filterOwner));
            echo json_encode($result);
        }
    } else if ($args[VARS_URL_METHOD] == METHOD_REMOVE_FILTER && isset($args[VARS_URL_FILTER_NAME])) {
        $result = array();
        $result['code'] = (EHelperFilter::deleteFilter($args[VARS_URL_FILTER_NAME], $filterOwner));
        echo json_encode($result);
    } else if ($args[VARS_URL_METHOD] == METHOD_REMOVE_USER && isset ($args[VARS_URL_FILTER_NAME]) && isset ($args[VARS_URL_EMAIL])){
        $result = array();
        $result['code'] = (EHelperFilter::removeUserFromFilter($args[VARS_URL_EMAIL], $args[VARS_URL_FILTER_NAME], $filterOwner));
        echo json_encode($result);
    } else if ($args[VARS_URL_METHOD] == METHOD_ADD_USER && isset ($args[VARS_URL_FILTER_NAME]) && isset ($args[VARS_URL_EMAIL])){
        $result = array();
        $result['code'] = (EHelperFilter::addUserToFilter($args[VARS_URL_EMAIL], $args[VARS_URL_FILTER_NAME], $filterOwner));
        echo json_encode($result);
    }
} else if (isset($args[VARS_URL_FILTER_NAME]) && strlen($filterOwner) > 0) {
    if (isset($args[VARS_URL_FROM]) && isset($args[VARS_URL_TO])) {
        EHelperFilter::updateDateRange($args[VARS_URL_FROM], $args[VARS_URL_TO], $args[VARS_URL_FILTER_NAME], $filterOwner);
    } else {
        $emails = EHelperFilter::getEmailsInFilter($args[VARS_URL_FILTER_NAME], $filterOwner);
        echo json_encode($emails);
    }
} else if (count($args) === 0) {
    $filters = EHelperFilter::getFilters();
    echo json_encode($filters);
}else if ($args[VARS_URL_METHOD] === METHOD_GET_ALL_EMAIL){
    $emails = EUserManager::getAllStudentsEmail();
    echo json_encode($emails);
}