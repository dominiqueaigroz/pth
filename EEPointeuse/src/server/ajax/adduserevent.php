<?php
define('_SERVER_', TRUE);

require_once './../../server/inc.all.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère les paramètres
$email = "";
if (isset($_POST[VARS_URL_EMAIL]))
    $email = $_POST[VARS_URL_EMAIL];
$projectCode = "";
if (isset($_POST[VARS_URL_PROJECT_CODE]))
    $projectCode = $_POST[VARS_URL_PROJECT_CODE];

if (strlen($email) > 0 && strlen($projectCode) > 0) {
    
    if (EUserManager::checkUserRole($email) == false){
        // Si j'arrive ici, c'est pas bon
        echo '{ "ReturnCode": 3, "Message": "Problème de contrôle du rôle utilisateur"}';
        exit();
        
    }
    $retValue = EEventManager::insertEventForUser($email, $projectCode);
    if ($retValue === false) {
        // Si j'arrive ici, c'est pas bon
        echo '{ "ReturnCode": 2, "Message": "Erreur d\'insertion de l\'event"}';
        exit();
    }
    // OK
    echo '{ "ReturnCode": 0, "DateTime": "' . $retValue . '"}';
    exit();
}


// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Il manque des paramètres"}';


?>