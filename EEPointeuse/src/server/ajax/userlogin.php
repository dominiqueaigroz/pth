<?php
define('_SERVER_', TRUE);

require_once './../../server/inc.all.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json;charset=utf-8');

// Je récupère les paramètres
$email = "";
if (isset($_POST[VARS_URL_EMAIL]))
    $email = $_POST[VARS_URL_EMAIL];
// Le nom complet
$fullname = "";
if (isset($_POST[VARS_URL_FULLNAME]))
    $fullname = $_POST[VARS_URL_FULLNAME];
$image = "";
if(isset($_POST[VARS_URL_IMAGE]))
    $image = $_POST[VARS_URL_IMAGE];




if (strlen($email) > 0) {
    
    if (EUserManager::checkUserRole($email) === false){
        // Si j'arrive ici, c'est pas bon
        echo '{ "ReturnCode": 3, "Message": "Problème de contrôle du rôle utilisateur"}';
        exit();
        
    }
    $role = EUserManager::getRole2($email);
    if ($role === false){
        // Si j'arrive ici, c'est pas bon
        echo '{ "ReturnCode": 3, "Message": "Problème de contrôle du rôle utilisateur"}';
        exit();
        
    }
    $url = getHomePage($role);
    // On va remplir la session
    ESession::getInstance()->setInfo($email, $role, $fullname, $image);
    // OK
    echo '{ "ReturnCode": 0, "URL": "' . $url . '"}';
    exit();
}


// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Il manque des paramètres"}';


?>