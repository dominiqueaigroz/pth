<?php
define('_SERVER_', TRUE);

require_once './../../server/inc.all.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère les paramètres
$user = "";
if (isset($_POST['user']))
    $user = $_POST['user'];
$weekDays = array();
if (isset($_POST['days']))
    $weekDays = $_POST['days'];

if (count($weekDays) == 5) {
    $w = new EWorkingDays();
    if (strlen($user) > 0)
        $w->email = $user;
    else
        $w->email = EWD_ALLUSERS;

    foreach ($weekDays as $day) {
        $m = ($day['morning'] == "true") ? true : false;
        $a = ($day['afternoon'] == "true") ? true : false;
        $i = intval($day['index']);

        $w->Weekdays[$i] = getCode($m, $a);
    }
    // Remplir le tableau
    if (EHelperDay::SetWorkingDays($w) == false) {
        // Si j'arrive ici, c'est pas bon
        echo '{ "ReturnCode": 3, "Message": "Problème d\'insertion des jours"}';
        exit();
    }
    // OK
    echo '{ "ReturnCode": 0 }';
    exit();
}


// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Le tableau des jours est incohérent"}';

/**
 * Retourne le code EWD_ correspondant en fonction du matin et de l'après-midi
 * @param boolean $morning  True si travaillé le matin
 * @param boolean $after    True si travaillé l'après-midi
 * @return EWD_NONE         Pas travaillé
 *         EWD_MORNING      Uniquement le matin
 *         EWD_AFTERNOON    Uniquement l'après-midi
 *         EWD_FULLDAY      Toute la journée
 */
function getCode($morning, $after) {
    $code = EWD_NONE;
    if ($morning == true && $after == false) {
        $code = EWD_MORNING;
    }
    if ($morning == false && $after == true) {
        $code = EWD_AFTERNOON;
    }
    if ($morning == true && $after == true) {
        $code = EWD_FULLDAY;
    }

    return $code;
}

?>