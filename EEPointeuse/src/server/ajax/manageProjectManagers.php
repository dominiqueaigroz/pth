<?php

define('__SERVER__', true);
require_once './../../server/inc.all.php';
$args = array();
foreach ($_GET as $key => $value) {
    $paramValue = filter_input(INPUT_GET, $key);
    if (strlen($paramValue) > 0) {
        $args[$key] = $paramValue;
    }
}
if (isset($args[VARS_URL_PROJECT_CODE]) && isset($args[VARS_URL_EMAIL])) {
    if ($args[VARS_URL_METHOD] == METHOD_REMOVE_MANAGER) {
        echo EProjectManager::deleteManager($args[VARS_URL_PROJECT_CODE], $args[VARS_URL_EMAIL]);
    }else if($args[VARS_URL_METHOD] == METHOD_ADD_MANAGER){
        echo EProjectManager::addManager($args[VARS_URL_PROJECT_CODE], $args[VARS_URL_EMAIL]);
    }
}else if(isset($args[VARS_URL_EMAIL])){
    $result = EUserManager::getUsersContaining($args[VARS_URL_EMAIL]);
    echo json_encode($result);
}