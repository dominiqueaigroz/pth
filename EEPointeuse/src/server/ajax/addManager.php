<?php

define('_SERVER_', TRUE);
require_once './../../server/inc.all.php';
if (isset($_GET[VARS_URL_EMAIL]) && isset($_GET[VARS_URL_PROJECT_CODE]) && isset($_GET[VARS_URL_ROLE])) {
    $email = filter_input(INPUT_GET, VARS_URL_EMAIL);
    $r = filter_input(INPUT_GET, VARS_URL_ROLE);
    $role = intval($r);
    $projectCode = filter_input(INPUT_GET, VARS_URL_PROJECT_CODE);
}
if (isset($email) && isset($projectCode)) {
    if ($role === 1 || $role === 0)
        $result['resultCode'] = EProjectManager::addManager($projectCode, $email);
    if ($role === 2)
        $result['resultCode'] = EProjectManager::addUser($projectCode, $email);
    echo json_encode($result);
}else {
    $result['resultCode'] = 0;
    $result['message'] = "Pas assez de paramètres";
    echo json_encode($result);
}
