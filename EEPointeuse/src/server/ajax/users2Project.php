<?php

define('_SERVER_', TRUE);
require_once './../../server/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère les paramètres
$role = "";
if (isset($_POST['role']))
    $role = $_POST['role'];
$projectCode = "";
if (isset($_POST['projCode']))
    $projectCode = $_POST['projCode'];
$users = "";
if (isset($_POST['users']))
    $users = $_POST['users'];

if (strlen($role) > 0 && strlen($projectCode) > 0) {
    
    switch (intval($role)) {
        case EROLE_USER:  //élève
        case EROLE_UNKNOWN:  //unkown
            EProjectManager::setUserOnProject($users, $projectCode);
            break;
        case EROLE_ADMIN: //admin
        case EROLE_PM: //chef de proj
        case 4:
            //admin + chef de projet
            EProjectManager::setManagerOnProject($users, $projectCode);
            break;
        default:
            break;
    }
    echo '{"ReturnCode": 0}';
}else{


// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Il manque des paramètres"}';
}
/*
if (isset($_GET[VARS_URL_EMAIL]) && isset($_GET[VARS_URL_PROJECT_CODE]) && isset($_GET[VARS_URL_ROLE])) {
    $email = filter_input(INPUT_GET, VARS_URL_EMAIL);
    $r = filter_input(INPUT_GET, VARS_URL_ROLE);
    $role = intval($r);
    $projectCode = filter_input(INPUT_GET, VARS_URL_PROJECT_CODE);
}
if (isset($email) && isset($projectCode)) {
    if ($role === 1 || $role === 0)
        $result['resultCode'] = EProjectManager::addManager($projectCode, $email);
    if ($role === 2)
        $result['resultCode'] = EProjectManager::addUser($projectCode, $email);
    echo json_encode($result);
}else {
    $result['resultCode'] = 0;
    $result['message'] = "Pas assez de paramètres";
    echo json_encode($result);
}
*/
