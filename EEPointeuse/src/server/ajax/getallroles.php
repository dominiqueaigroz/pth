<?php

require_once './../../server/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$arrResult = EUserManager::getAllRoles();

$jsn = json_encode($arrResult, JSON_UNESCAPED_UNICODE); //JSON_UNESCAPED_UNICODE nécessaire !
$code = json_last_error();
if ($code != JSON_ERROR_NONE ||
        $jsn === false || $jsn === NULL) {
    echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json (' . $code . ')}';
    exit();
}
// OK
echo '{ "ReturnCode": 0, "Data": ' . $jsn . ' }';
exit();


// Fail
echo '{ "ReturnCode": 1, "Message": "Il manque des paramètres"}';
