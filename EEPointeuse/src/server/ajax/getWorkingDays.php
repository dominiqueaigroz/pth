<?php
define('_SERVER_', TRUE);

require_once './../../server/inc.all.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère les paramètres
$user = "";
if (isset($_POST['user']))
    $user = $_POST['user'];

$workingDaysUser = EHelperDay::GetWorkingDays($user);
if ($workingDaysUser === false) {
        // Si j'arrive ici, c'est pas bon
        echo '{ "ReturnCode": 3, "Message": "Problème de récuparation de la fonction GetWorkingDays"}';
        exit();
    }
    
    
$result = json_encode($workingDaysUser);
if ($result ===FALSE) {
     echo '{ "ReturnCode": 2, "Message": "Problème d\'encodage"}';
     exit();
}
// OK
    echo '{ "ReturnCode": 0 , "Result":'.$result.'}';
?>