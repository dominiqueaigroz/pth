<?php

require_once './../../server/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$email = "";
if (isset($_POST['email']))
    $email = $_POST['email'];

// from date (par défaut la date du jour)
$idRole = "";
if (isset($_POST['idRole']))
    $idRole = $_POST['idRole'];

if (strlen($email) > 0 && strlen($idRole) > 0 ){
    $res = EUserManager::setRole($email, $idRole);
    if($res){
    echo '{ "ReturnCode": 0, "Data": "ok" }';
    exit();
}

// Fail
echo '{ "ReturnCode": 1, "Message": "Il manque des paramètres"}';
}