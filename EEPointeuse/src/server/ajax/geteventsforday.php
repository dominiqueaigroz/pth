<?php
define('_SERVER_', TRUE);

require_once './../../server/inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère les paramètres
// project code (par défaut assigné au projet global)
$projectCode = ECTS_USERTIMESHEET_PROJECTCODE;
if (isset($_POST['projCode']))
    $projectCode = $_POST['projCode'];

// email (par défaut aucun)
$email = "";
if (isset($_POST['email']))
    $email = $_POST['email'];

// from date (par défaut la date du jour)
$fromDate = date("Y-m-d");
if (isset($_POST[VARS_URL_DATE_FROM]))
    $fromDate = $_POST[VARS_URL_DATE_FROM];

// to date (par défaut la même date que la from)
$toDate = $fromDate;
if (isset($_POST['dtTo']))
    $toDate = $_POST['dtTo'];

if (strlen($projectCode) > 0 &&
        strlen($fromDate) > 0 &&
        strlen($toDate) > 0) {
    $arrResult = EEventManager::getRecordEvents($projectCode, $email, $fromDate, $toDate, TRUE);
    $jsn = json_encode($arrResult, JSON_UNESCAPED_UNICODE); //JSON_UNESCAPED_UNICODE nécessaire !
    $code = json_last_error();
    if ($code != JSON_ERROR_NONE ||
        $jsn === false || $jsn === NULL){
        echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json (' . $code . ')}';
        exit();
    }
    // OK
    echo '{ "ReturnCode": 0, "Data": '. $jsn .' }';
    exit();
}

// Fail
echo '{ "ReturnCode": 1, "Message": "Il manque des paramètres"}';


