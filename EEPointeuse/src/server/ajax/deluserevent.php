<?php
define('_SERVER_', TRUE);


require_once './../../server/inc.all.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère les paramètres
$email = "";
if (isset($_POST['user']))
    $email = $_POST['user'];
$projectCode = "";
if (isset($_POST['projCode']))
    $projectCode = $_POST['projCode'];
$dtEvent = "";
if (isset($_POST['dt']))
    $dtEvent = $_POST['dt'];

if (strlen($email) > 0 && strlen($projectCode) > 0 && strlen($dtEvent) > 0) {
    
    if (EEventManager::deleteEvent($projectCode, $email, $dtEvent) == false){
        // Si j'arrive ici, c'est pas bon
        echo '{ "ReturnCode": 2, "Message": "Impossible de marquer l\'enregistrement supprimé"}';
        exit();
    }
    // Extraire la date uniquement
    $date = substr($dtEvent, 0, 10);
    $dt = new DateTime($dtEvent);
    $attended = eventCountOnDay($dt, $email);
    $cnt = EEventManager::getCountEvent($projectCode, $email, $date);
    
    // OK
    echo '{ "ReturnCode": 0, "AttendedCount": '.$attended.', "EventCount": '.$cnt.'}';
    exit();
}


// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Il manque des paramètres"}';


?>