<?php

class EBugReport{
    /** le type de bug **/
    public $type;
    
    /** la descripition du bug **/
    public $descripition;
    
    /** l'état du bug, par défaut open **/
    public $state = BUG_STATE_OPEN;
}
