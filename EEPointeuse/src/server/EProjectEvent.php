<?php


/**
 * Cette classe contient les informations
 * sur les pointages
 * 
 * @remark Première étape est de faire un objet
 *         qui contient les données uniquement.
 *         Pas de méthodes pour accéder ou manipuler
 *         l'objet.
 */
class EProjectEvent implements JsonSerializable{
    
    /** @var string L'email de la personne */
    public $email;
    
    /** @var dateTime Le datetime du record */
    public $dt;
    
    /** @var string Le code correspondant au projet */
    public $projectCode;
    
    /** @var boolean Un booléen qui dit que le record est:
     *                  - 0 = Not Deleted
     *                  - 1 = Deleted
     */
    public $deleted;
    
    public function jsonSerialize() {
        return get_object_vars($this);
    }    
    
}
