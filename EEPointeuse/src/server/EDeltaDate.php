<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Cette classe contient les informations
 * sur le delta à une date donnée
 * 
 * @remark Première étape est de faire un objet
 *         qui contient les données uniquement.
 *         Pas de méthodes pour accéder ou manipuler
 *         l'objet.
 */
class EDeltaDate{
    
    /**
     * Constructeur par défaut
     * @param DateTime $InDt    La date de l'intervale
     * @param DateInterval $InDelta L'intervale
     */
    public function __construct($InDt = NULL, $InDelta = NULL)
    {
        $this->dt = $InDt;
        $this->delta = $InDelta;
    }
    /**
     * Test si l'objet est valide
     * @return boolean  True si valide
     */
    public function IsValid(){
        return $this->dt != NULL && $this->delta != NULL;
    }
    /** @var dateTime Le datetime du delta. */
    public $dt;
    
    /** @var DateInterval Le delta en intervalle de temps. */
    public $delta;
}