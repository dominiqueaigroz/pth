<?php

@session_start();
//define("_SERVER_",true);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/** @remark On met le format pour les dates. On devrait normalement récupérer la locale de l'utilisateur */
date_default_timezone_set('Europe/Paris');
setlocale(LC_TIME, 'fr_FR.utf8','fra');

require_once __DIR__ . '/constants.php';

require_once __DIR__ . '/ESession.php';

// On ne check pas la session si _SERVER_ ou _BYPASS_CHECKSESSION_ est défini et TRUE
if ((defined("_SERVER_") == FALSE || _SERVER_ == FALSE) && (defined("_BYPASS_CHECKSESSION_") == FALSE || _BYPASS_CHECKSESSION_ == FALSE)) {
    if (ESession::getInstance()->check()){
        // Si on a un email défini dans l'url, on check si on est pas admin
        // et on autorise pas de visualiser un autre email.
        if (isset($_GET[VARS_URL_EMAIL]) && ESession::getInstance()->getRole() >= EROLE_PM){
            if (filter_input(INPUT_GET, VARS_URL_EMAIL) !== ESession::getInstance()->getEmail()){
                header('Location: '. getRootURL() . URL_INVALID_ROLE);
                exit();
            }
        }
    }
}
require_once __DIR__ . '/db/EDatabase.php';
require_once __DIR__ . '/EPerson.php';
require_once __DIR__ . '/EProject.php';
require_once __DIR__ . '/EDeltaDate.php';
require_once __DIR__ . '/EDeltaUserProject.php';
require_once __DIR__ . '/EWorkingDays.php';
require_once __DIR__ . '/EFilter.php';
require_once __DIR__ . '/ESearch.php';
require_once __DIR__ . '/EProjectEvent.php';
require_once __DIR__ . '/ERole.php';
require_once __DIR__ . '/EBugReport.php';
require_once __DIR__ . '/Manager/EDeltaManager.php';
require_once __DIR__ . '/Manager/EDateManager.php';
require_once __DIR__ . '/Manager/EEventManager.php';
require_once __DIR__ . '/Manager/EProjectManager.php';
require_once __DIR__ . '/Manager/EUserManager.php';
require_once __DIR__ . '/Manager/ECheckingCodeManager.php';
require_once __DIR__ . '/Manager/Helpers.php';
require_once __DIR__ . '/Manager/EHelperDay.php';
require_once __DIR__ . '/Manager/EHelperFilter.php';
require_once __DIR__ . '/Manager/EBugManager.php';


