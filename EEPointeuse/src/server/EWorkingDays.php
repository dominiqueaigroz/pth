<?php

/**
 * Description of EWorkingDays
 * Cette classe sert de structure pour les plage de de travails 
 * 
 * @author BOREL-JAQJ
 */
class EWorkingDays {
    
    /** @var string Une chaine de caractère qui contient l’email d’un utilisateur **/
    public $email;
    
    /** Un tableau dont l’index est le jour de la semaine et chaque jour contient :
     * EWD_FULLDAY : travail toute la journée.
     * EWD_MORNING : travail le matin.
     * EWD_AFTERNOON : travail l’après-midi.
     * EWD_NONE : travail pas de la journée.
    **/
    public $Weekdays;
    
    /**
     * Test si l'objet est valide.
     * @return boolean  True si l'objet est valide, autrement invalide
     */
    public function isValid(){
        return ($this->Weekdays[EWD_MONDAY] != EWD_NONE) || 
               ($this->Weekdays[EWD_MONDAY] != EWD_NONE) || 
               ($this->Weekdays[EWD_MONDAY] != EWD_NONE) || 
               ($this->Weekdays[EWD_MONDAY] != EWD_NONE) || 
               ($this->Weekdays[EWD_MONDAY] != EWD_NONE);
    }
}
