<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Cette classe contient les informations
 * sur une personne (utilisateur) du système
 * de pointage.
 * 
 * @remark Première étape est de faire un objet
 *         qui contient les données uniquement.
 *         Pas de méthodes pour accéder ou manipuler
 *         l'objet.
 */
class EPerson{
    
    /** @var string L'email de la personne */
    public $email;
    
    /** 
     * @var integer Le rôle de la personne :
     *          - EROLE_ADMIN
     *          - EROLE_PM
     *          - EROLE_USER
     *          - EROLE_UNKNOWNED
     */
    public $role;
    
}