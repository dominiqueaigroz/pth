<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//URL pointant sur la page d'authentification des utilisateurs
define ('URL_AUTENTIFICATION', "/index.html");
//URL pointant vers la page d'avertissement pour une action invalide
define('URL_INVALID_ROLE', "/invalidRole.php");

//project de base
define ('ECTS_USERTIMESHEET_PROJECTCODE', "JAOSMI8923NA78");

// Les rôles des personnes
define('EROLE_ADMIN', 0);
define('EROLE_PM', 1);
define('EROLE_USER', 2);
define('EROLE_UNKNOWN', 3);

// Les url de redirection en fonction du rôle
define('URL_HOME_ADMIN', "/EventDay.php");
define('URL_HOME_PM', "/EventDay.php");
define('URL_HOME_USER', "/RecapDeltaEleve.php");
define('URL_HOME_UNKNOWN', "/RoleUnassigned.php");


//nombre d'heures faites pour l'élève
define("ECTS_HOURPERWEEK", 30);
define("ECTS_HOURPEMONTH", 30 * 5);
define("ECTS_HOURPERDAY", 8);
define("ECTS_MINUTEPERDAY", 0);

date_default_timezone_set("Europe/Zurich");


//Les index pour les jours
define("EWD_INVALID", -1);
define("EWD_MONDAY", 0);
define("EWD_TUESDAY", 1);
define("EWD_WEDNESDAY", 2);
define("EWD_THURSDAY", 3);
define("EWD_FRIDAY", 4);

/** @brief travail pas de la journée. */
define("EWD_NONE", 0);
/** @brief travail le matin. */
define("EWD_MORNING", 1);
/** @brief travail l’après-midi. */
define("EWD_AFTERNOON", 2);
/** @brief travail toute la journée. */
define("EWD_FULLDAY", 3);

//Nom de l'email général
define("EWD_ALLUSERS","-");

/** @brief Variables d'argument au sein de l'url. */
define('VARS_URL_EMAIL', "email");
define('VARS_URL_ROLE', "role");
define('VARS_URL_PROJECT_CODE', 'projectCode');
define('VARS_URL_MONTH', 'month');
define('VARS_URL_DAY', 'day');
define('VARS_URL_DATE_EVENT', 'dtEvent');
define('VARS_URL_DATE_FROM', 'dtFrom');
define('VARS_URL_FULLNAME', 'fullName');
define('VARS_URL_IMAGE', 'image');
define('VARS_URL_METHOD', 'method');
define('VARS_URL_FILTER_NAME', 'filterName');
define('VARS_URL_FILTER_OWNER', 'filterOwner');
define('VARS_URL_NEW_NAME', 'newName');
define('VARS_URL_FROM', 'from');
define('VARS_URL_TO', 'to');

/** @brief Méthodes pour les filtres*/
define('METHOD_GET_ALL_EMAIL', 'getAllEmails');
define('METHOD_ADD_FILTER', 'add');
define('METHOD_REMOVE_FILTER', 'delete');
define('METHOD_REMOVE_USER', 'removeUser');
define('METHOD_ADD_USER', 'addUser');
define('METHOD_RENAME_FILTER', 'renameFilter');
define('METHOD_REMOVE_MANAGER', 'removeManager');
define('METHOD_ADD_MANAGER', 'addManager');
/**
 * @brief En développement, il est possible de by-passer le check de la session.
 *        Il suffit de définir cette variable en début de fichier que vous voulez débugger
 *        define("_BYPASS_CHECKSESSION_", TRUE);
 */
 
/**
 * @brief Pour les fichiers php sur le serveur il ne faut pas checker la session.
 *        Il suffit de placer cette ligne en début de fichier 
 *        define("_SERVER_", TRUE);
 * 
 *        avant l'inclusion de '/server/inc.all.php';
 */

/** @brief Status des bug report **/
define('BUG_STATE_OPEN', '1');
define('BUG_STATE_SOLVED', '2');

