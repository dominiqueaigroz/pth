<?php

/**
 * Description of ESearchParam
 * Cette classe est utilisé comme paramètre
 * de la recherche
 * 
 * @author BURNANDL
 */
class ESearchParam {
    /** @var string le filtre de la recherche*/
    public $filter;
    
    /** @var dateTime La date de début **/
    public $dtFrom;
    
    /** @var dateTime La date de fin **/
    public $dtTo;    

    /** 
     * @var integer Le rôle de la personne :
     *          - EROLE_ADMIN
     *          - EROLE_PM
     *          - EROLE_USER
     *          - EROLE_UNKNOWNED
     */
    public $role;    
    
    /**
     * Constructor
     * @param string $InFilter  Contient l'email. Defaut est vide.
     * @param string $InDtFrom  Contient le nom du projet. Defaut est vide.
     * @param string $InDtTo    Contient une date. sous la forme YYYY-MM-DD HH:MM:SS. Defaut est vide.
     * @param integer $InRole   Contient le type de role. Defaut est EROLE_UNKNOWNED.
     */
    public function __construct($InFilter = "", $InDtFrom = "", $InDtTo = "", $InRole = EROLE_UNKNOWNED)
    {
        $this->filter = $InFilter;
        $this->dtFrom = $InDtFrom;
        $this->dtTo = $InDtTo;
        $this->role = $InRole;
    }
}
