<?php

/**
 * Description of ECheckingCodeManager
 *
 * @author ACKERMANNG, NATALEM
 */
class ECheckingCodeManager {

    /**
     * Renvoie le code de pointage
     * @return {string} $code
     */
    public static function getTimeCheckingCode() {
        $code = self::createTimeCheckingCode();
        if (self::setTimeCheckingCode($code))
            return $code;
        return false;
    }

    /**
     * Rentre le code de pointage dans la table
     * @param {string} $code
     * @return {bool}
     */
    public static function setTimeCheckingCode($code) {
        try {
            $stmt = EDatabase::prepare('UPDATE `PROJECTS_INFOS` SET `INTERNAL_PROJECT_KEY` = :code WHERE PROJECTS_CODE_PK = :defaultProject;');
            $stmt->execute(array(
                'code' => $code,
                'defaultProject' => ECTS_USERTIMESHEET_PROJECTCODE)
            );
            return true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        return false;
    }

    /**
     * Génére un code de pointage
     * @param {int} $len La longueur du code
     * @return {string} Le code
     */
    public static function createTimeCheckingCode($len = 16) {
        $code = '';
        for ($i = 0; $i < $len; $i++) {
            $charType = rand(1, 3);
            switch ($charType) {
                case 1:
                    $minRange = 33;
                    $maxRange = 47;
                    break;
                case 2:
                    $minRange = 58;
                    $maxRange = 64;
                    break;
                case 3:
                    $minRange = 123;
                    $maxRange = 126;
                    break;
                default :
                    break;
            }
            $code .= chr(rand($minRange, $maxRange));
        }
        return $code;
    }

}
