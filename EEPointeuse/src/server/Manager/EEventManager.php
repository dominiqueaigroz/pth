<?php

class EEventManager {

    /**
     * Fonction qui permet de "supprimer" un event en fonction du code du projet, de l'email de l'utilisateur et de la date car la champ DELETED qui est de base à 0 passera à 1 (=supprimé) 
     * @param {string} $projectCode
     * @param {string} $email
     * @param {datetime} $dt
     * @return {boolean} true si supprimé avec succès, false si erreur s'est produite
     */
    public static function deleteEvent($projectCode, $email, $dt) {
        $sqlDeleteProject = "UPDATE EVENTS SET DELETED = 1 WHERE PROJECTS_CODE_PK = :c AND USERS_EMAIL_PK = :u AND DT_PK = :d";
        $stmt = EDatabase::prepare($sqlDeleteProject);
        if ($stmt->execute(array(
                    "c" => $projectCode,
                    "u" => $email,
                    "d" => $dt
                ))) {
            return true;
        } {
            //si fail
            return false;
        }
    }

    /*
     * Cette fonction nous retourne tous les événements de pointage entre deux dates. Avec la possibilité
     * de filter pour un projet et/ou un email.
     * @param {string} $projectCode (Optional) Contient code projet. Par défault est null.
     * @param {string} $email (Optional) L'adresse email de l'utilisateur. Par défaut est null.
     * @param {string} $from (Optional) La date de début du filtre. Au format "YYYY-MM-DD". Par défaut est "".
     * @param {string} $to (Optional) La date de fin du filtre. Au format "YYYY-MM-DD". Par défaut est "".
     * @param {boolean} $includeDeleted (Optional) Inclure ou non les enregistrements marqués supprimé. Par défaut est false.
     * @return <array of EProjectEvent>  Un tableau d'objet EProjectEvent.
     */

    public static function getRecordEvents($projectCode = "", $email = "", $from = "", $to = "", $includeDeleted = false) {

        $now = new DateTime();
        $sql = "SELECT DT_PK, USERS_EMAIL_PK, PROJECTS_CODE_PK, DELETED FROM EVENTS";
        $filter = "";
        $arrFields = array();
        if (strlen($email) > 0) {
            $filter .= " USERS_EMAIL_PK = :e";
            $arrFields['e'] = $email;
        }
        if (strlen($projectCode) > 0) {
            if (strlen($filter) > 0)
                $filter .= " AND ";
            $filter .= " PROJECTS_CODE_PK = :p";
            $arrFields['p'] = $projectCode;
        }
        // Si le from et to sont remplis
        if (strlen($from) > 0 && strlen($to) > 0) {
            if (strlen($filter) > 0)
                $filter .= " AND ";
            //On ajoute 17h pour éviter que le between ne prenne la date à minuit
            //checker si il y a que le from ou que le to sinon remplacer la requette par where DT_PK = :f / where DT_PK = :t

            $filter .= " DT_PK BETWEEN :f AND DATE_ADD(:t, INTERVAL 17 Hour)";
            $arrFields['f'] = $from;
            $arrFields['t'] = $to;
        }
        else // autrement j'ai soit le from soit le to rempli
        if (strlen($from) > 0) { // Est-ce le from?
            if (strlen($filter) > 0)
                $filter .= " AND ";
            $filter .= "DT_PK BETWEEN :f AND DATE_ADD('" . $now->format('Y-m-d') . "', INTERVAL 0 DAY)";
            $arrFields['f'] = $from;
        }
        else
        if (strlen($to) > 0) { // Est-ce le to?
            if (strlen($filter) > 0)
                $filter .= " AND ";
            $filter .= "DT_PK BETWEEN DATE_ADD('2000-01-01', INTERVAL 0 DAY) AND DATE_ADD(:t, INTERVAL 1 DAY)";
            $arrFields['t'] = $to;
        }
        if ($includeDeleted == FALSE) {
            if (strlen($filter) > 0)
                $filter .= " AND ";
            $filter .= " DELETED = :d";
            $arrFields['d'] = $includeDeleted;
        }

        if (strlen($filter) > 0) {
            $sql .= " WHERE " . $filter;
        }

        /* @remark Ne fonctionne pas pour certains traitements
          $sql .= " ORDER BY DT_PK, USERS_EMAIL_PK ASC";
         */
        $sql .= " ORDER BY USERS_EMAIL_PK , DT_PK ASC";

        try {
            $stmt = EDatabase::prepare($sql);
            $stmt->execute($arrFields);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }

        $arrResult = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            // Créer l'object
            $evt = new EProjectEvent();
            $evt->dt = new DateTime($row["DT_PK"]);
            $evt->email = $row["USERS_EMAIL_PK"];
            $evt->projectCode = $row["PROJECTS_CODE_PK"];
            $evt->deleted = $row["DELETED"];
            array_push($arrResult, $evt);
        }

        // Retourne le tableau des événements
        return $arrResult;
    }
    /**
     * Fonction qui permet d'ajouter à la base le timestamp de l'élève sur un projet donnée lors de la connexion EEL
     * @param string $projectCode Contient code projet
     * @param string $email L'adresse email de la personne
     * @return DateTime retourne le date et l'heure sous forme de string si correctement inséré, autrement retourne false
     */
    public static function insertEventForUser($email, $projectCode) { //eproject manager
        $dt = date("Y-m-d H:i:s");
        $sqlinsertEventForUser = "INSERT INTO EVENTS (USERS_EMAIL_PK, DT_PK, PROJECTS_CODE_PK, DELETED) VALUES (:e,:d, :p, 0)";
        $stmt = EDatabase::prepare($sqlinsertEventForUser);
        if ($stmt->execute(array(
                    "e" => $email,
                    "d" => $dt,
                    "p" => $projectCode
                ))) {
            return $dt;
        }

        //si fail
        return false;
    }

    /**
     * Fonction qui permet de "supprimer" un event en fonction du code du projet, de l'email de l'utilisateur et de la date car la champ DELETED qui est de base à 0 passera à 1 (=supprimé) 
     * @param {string} $projectCode Contient le code projet.
     * @param {string} $email L'adresse email de l'utilisateur.
     * @param {string} $dt La date des événements à rechercher, au format "YYYY-MM-DD"
     * @return {integer} Le nombre d'événements. FALSE si erreur s'est produite
     */
    public static function getCountEvent($projectCode, $email, $dt) {
        
        $sql = "SELECT COUNT(*) as cnt FROM EVENTS WHERE DELETED = 0 AND PROJECTS_CODE_PK = :c AND USERS_EMAIL_PK = :u AND DT_PK LIKE :d";
        try {
            $stmt = EDatabase::prepare($sql);
            $stmt->execute(array(
                        "c" => $projectCode,
                        "u" => $email,
                        "d" => $dt.'%'
                    ));
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            //fail
            return false;
        }
        if ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            return intval($row["cnt"]);
        }        
        //fail
        return false;
    }

}
