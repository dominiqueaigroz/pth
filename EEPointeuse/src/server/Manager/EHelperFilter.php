<?php

/**
 * EHelperFilter est l'API pour les filtres
 */
class EHelperFilter {

    /**
     * Cette fonction crée un filtre
     * @param {string} $filterName
     * @param {string} $from
     * @param {string} $to
     * @return bool
     */
    public static function createFilter($filterName, $ownerEmail, $from, $to) {
        try {
            $stmt = EDatabase::prepare('INSERT INTO FILTERS (`NAME`,`OWNER_EMAIL`,`FROM`,`TO`) VALUES(:name,:ownerEmail,:from,:to)');
            $ownerEmail = ESession::getInstance()->getEmail();
            $stmt->execute(array(
                        'name' => $filterName,
                        'ownerEmail' => $ownerEmail,
                        'from' => $from,
                        'to' => $to)
            );
            return true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Cette fonction mets une plage horaire à jour
     * @param {date} $from
     * @param {date} $to
     */
    public static function updateDateRange($from, $to, $filterName, $filterOwner) {
        try {
            $stmt = EDatabase::prepare('UPDATE `FILTERS` SET `FROM` = :from, `TO` = :to WHERE `NAME` = :filterName AND OWNER_EMAIL = :owner');
            $stmt->execute(array(
                'from' => $from,
                'to' => $to,
                'filterName' => $filterName,
                'owner' => $filterOwner
            ));
            return true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Cette fonction supprime un filtre
     * @param {string} $filterName
     * @return bool
     */
    public static function deleteFilter($filterName, $owner) {
        try {
            $stmt = EDatabase::prepare('DELETE FROM `FILTERS` WHERE `NAME` = :name AND `OWNER_EMAIL` = :owner;');
            $stmt->execute(array('name' => $filterName,
                ':owner' => $owner));
            return true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Cette fonction renomme une fonction
     * @param {string} $oldName
     * @param {string} $newName
     * @return bool
     */
    public static function renameFilter($oldName, $newName, $ownerName) {
        try {
            $stmt = EDatabase::prepare('UPDATE `FILTERS` SET `NAME` = :new WHERE `NAME` = :old AND OWNER_EMAIL = :owner');
            $stmt->execute(array(
                'old' => $oldName,
                'new' => $newName,
                'owner' => $ownerName
            ));
            return true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Cette fonction ajoute un utilisateur à un filtre
     * @param {string} $userEmail
     * @param {string} $filter
     * @return bool
     */
    public static function addUserToFilter($emailToAdd, $filterName, $ownerEmail) {

        try {
            $stmt = EDatabase::prepare('INSERT INTO `FILTERS_EMAIL`(`EMAIL`,`FILTERS_NAME`,`FILTERS_OWNER_EMAIL`)'
                            . 'VALUES(:emailToAdd,:filterName,:ownerEmail)');
            $stmt->execute(array(
                'emailToAdd' => $emailToAdd,
                "filterName" => $filterName,
                "ownerEmail" => $ownerEmail));
            return true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Cette fonction retire un utilisateur d'un filtre
     * @param {string} $userEmail
     * @param {string} $filterName
     * @return bool
     */
    public static function removeUserFromFilter($userEmail, $filterName, $ownerEmail) {

        try {
            $stmt = EDatabase::prepare('DELETE FROM `FILTERS_EMAIL` WHERE `EMAIL` = :email AND `FILTERS_NAME` = :filter;');
            $stmt->execute(array(
                ':email' => $userEmail,
                ':filter' => $filterName
            ));
            return true;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Cette fonction retourne un tableau d'EFilter contenant tous les filtres
     * @return {array} Un tableau d'EFilter
     */
    public static function getFilters() {
        try {
            $stmt = EDatabase::prepare('SELECT `NAME`,`OWNER_EMAIL`,`FROM`,`TO` FROM `FILTERS`');
            $stmt->execute();
            $results = $stmt->fetchAll();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $filters = array();
        foreach ($results as $result) {
            $filter = new EFilter();
            $filter->name = $result['NAME'];
            $filter->ownerEmail = $result['OWNER_EMAIL'];
            $filter->from = $result['FROM'];
            $filter->to = $result['TO'];
            array_push($filters, $filter);
        }

        return $filters;
    }

    /**
     * Retourne les emails des utilisateurs dans le filtre spécifié
     * @param {string} $filterName
     * @param {string} $filterOwner
     */
    public static function getEmailsInFilter($filterName, $filterOwner) {
        $stmt = EDatabase::prepare('SELECT `EMAIL` FROM `FILTERS_EMAIL` WHERE FILTERS_NAME = :name AND FILTERS_OWNER_EMAIL = :email');
        try {
            $stmt->execute(array(
                "name" => $filterName,
                "email" => $filterOwner
            ));
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $results = $stmt->fetchAll();
        $emails = array();
        foreach ($results as $result) {
            array_push($emails, $result['EMAIL']);
        }
        return $emails;
    }

}
