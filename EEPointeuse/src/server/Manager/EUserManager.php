<?php

//require_once './../inc.all.php';

/**
 * Description of EUserManager
 *
 * @author BURNANDL
 */
class EUserManager {

    /**
     * Fonction qui retourne le role en fonction du email.
     * @param {string} $email L'email de l'utilisateur
     * @return {array} Tableau d'ERole
     */
    public static function getRole($email) {
        $stmt = EDatabase::prepare("SELECT r.CODE_PK, r.LABEL, u.EMAIL_PK FROM USERS AS u, ROLES AS r WHERE u.ROLES_CODE_PK = r.CODE_PK AND u.EMAIL_PK = :e");
        $arrRole = array();
        $stmt->execute(array(
            "e" => $email
        ));
        $result = $stmt->fetchAll();
        foreach ($result as $role) {
            $ERole = new ERole();
            $ERole->idRole = $role["CODE_PK"];
            $ERole->labelRole = $role["LABEL"];
            $ERole->email = $role["EMAIL_PK"];
            array_push($arrRole, $ERole);
        }
        return $arrRole;
    }

    /**
     * Fonction qui retourne le role en fonction du email.
     * @param {string} $email L'email de l'utilisateur
     * @return {integer} Le role ou false si erreur
     */
    public static function getRole2($email) {
        $stmt = EDatabase::prepare("SELECT r.CODE_PK, r.LABEL, u.EMAIL_PK FROM USERS AS u, ROLES AS r WHERE u.ROLES_CODE_PK = r.CODE_PK AND u.EMAIL_PK = :e");
        $arrRole = array();
        $stmt->execute(array(
            "e" => $email
        ));
        $result = $stmt->fetchAll();
        if (count($result) == 1) {
            return $result[0]["CODE_PK"];
        }
        return false;
    }

    /**
     * Fonction qui retourne les roles
     * @return {objet PDO} L'id des rôles ainsi que leurs labels
     */
    public static function getAllRoles() {
        $stmt = EDatabase::prepare("SELECT CODE_PK, LABEL FROM ROLES ORDER BY CODE_PK");
        $stmt->execute();
        $result = $stmt->fetchAll();
        if (count($result) > 0) {
            $arrRole = array();
            foreach ($result as $role) {
                $ERole = new ERole();
                $ERole->idRole = $role["CODE_PK"];
                $ERole->labelRole = $role["LABEL"];
                array_push($arrRole, $ERole);
            }
            return $arrRole;
        }
        // Si fail
        return FALSE;
    }

    /**
     * Fonction qui change le role d'un utilisateur
     * @return {boolean} TRUE si ok
     */
    public static function setRole($email, $idRole) {
        $sqlgetEmailUser = "UPDATE USERS SET ROLES_CODE_PK=:r WHERE EMAIL_PK=:e";
        try {
            $stmt = EDatabase::prepare($sqlgetEmailUser);
            $stmt->execute(array(
                "e" => $email,
                "r" => $idRole
            ));
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Fonction qui retourne les pointages d'un élève donné pour un jour donné
     * @param {string} $email L'email de l'utilisateur 
     * @param {date} $date la date choisie 
     * @return array
     */
    public static function getRecordByDateUserByProject($email, $date, $project) {
        $stmt = EDatabase::prepare("SELECT DT_PK FROM EVENTS WHERE USERS_EMAIL_PK='" . $email . "'AND PROJECTS_CODE_PK='" . $project . "'  AND DT_PK LIKE'" . $date . "%'");
        $stmt->execute();
        $tblRecordUserDate = array();
        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $ProjectEvent = new EProjectEvent();
            $ProjectEvent->dt = $rec["DT_PK"];
            $ProjectEvent->email = $email;
            array_push($tblRecordUserDate, $ProjectEvent);
        }
        return $tblRecordUserDate;
    }

    /**
     * Fonction qui retourne tous les pointages pour un élève donné
     * @param {string} $email L'email de l'utilisateur 
     * @return array les pointages de l'élève dans un tableau
     */
    public static function getRecordByUserByProject($email, $project) {

        $stmt = EDatabase::prepare("SELECT DT_PK FROM EVENTS WHERE USERS_EMAIL_PK='" . $email . "'AND PROJECTS_CODE_PK='" . $project . "'");
        $stmt->execute();
        $tblRecordUser = array();
        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $ProjectEvent = new EProjectEvent();
            $ProjectEvent->dt = $rec["DT_PK"];
            $ProjectEvent->email = $email;
            array_push($tblRecordUser, $ProjectEvent);
        }
        return $tblRecordUser;
    }

    /**
     * Fonction qui retourne tout les emails par ordres alphabétique. 
     * @return tout les emails par odres alphabétique
     */
    public static function getAllUserEmails() {
        $stmt = EDatabase::prepare("SELECT `EMAIL_PK` FROM `USERS` ORDER BY EMAIL_PK");
        $stmt->execute();
        $tblRecordUser = array();
        $emails = array();
        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            array_push($emails, $rec['EMAIL_PK']);
        }
        return $emails;
    }

    /**
     * Fonction qui retourne tous les emails des élèves par ordres alphabétique. 
     * @return tous les emails des élèves par ordres alphabétique.
     */
    public static function getAllStudentsEmail() {
        $stmt = EDatabase::prepare("SELECT `EMAIL_PK` FROM `users` WHERE ROLES_CODE_PK = 2 order by EMAIL_PK ");
        $stmt->execute();
        $tblRecordUser = array();
        $emails = array();
        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            array_push($emails, $rec['EMAIL_PK']);
        }
        return $emails;
    }

    /**
     * Fonction qui retourne tous les emails contenant le string 
     * @return tous les emails des élèves correspondant au string.
     */
    public static function getUsersContaining($userName) {
        $stmt = EDatabase::prepare("SELECT `EMAIL_PK` FROM `users` WHERE EMAIL_PK LIKE CONCAT('%', :s, '%') order by EMAIL_PK ");
        $stmt->execute(array(
            's' => $userName
        ));
        $emails = array();
        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            array_push($emails, $rec['EMAIL_PK']);
        }
        return $emails;
    }

    /**
     * Fonction qui retourne tous les emails des chefs de projet par ordres alphabétique. 
     * @return tous les emails des chefs de projet par ordres alphabétique.
     */
    public static function getAllManagersEmail() {
        $stmt = EDatabase::prepare("SELECT `EMAIL_PK` FROM `users` WHERE ROLES_CODE_PK = 1 order by EMAIL_PK ");
        $stmt->execute();
        $tblRecordUser = array();
        $emails = array();
        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            array_push($emails, $rec['EMAIL_PK']);
        }
        return $emails;
    }

    /**
     * Vérifie si l'utilisateur existe, s'il n'existe pas, lui ajoute un rôle par defaut.
     * @param type $email l'email de l'utilisateur
     * @return boolean False si une erreur s'est produite
     */
    public static function checkUserRole($email) {
        $sqlgetEmailUser = "SELECT COUNT(*) AS CNT FROM USERS WHERE EMAIL_PK = :e";
        try {
            $stmt = EDatabase::prepare($sqlgetEmailUser);
            $stmt->execute(array(
                "e" => $email
            ));
            $rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        } catch (PDOException $e) {
            return false;
        }
        // On récupère le nombre qui est sous forme de string
        // et on le converti en integer
        $counter = intval($rec["CNT"]);
        // Si on en a pas, on va insérer l'utilisateur
        if ($counter <= 0) {
            $sqlInsertUser = "INSERT INTO USERS (EMAIL_PK, ROLES_CODE_PK) VALUES (:e,:r)";
            /* @remark Remplacé par le code sur une ligne 
              $stmt = EDatabase::prepare($sqlInsertUser);
              $stmt->execute(array(
              "e" => $email,
              "r" => EROLE_UNKNOWN
              ));
             */

            try {

                EDatabase::prepare($sqlInsertUser)->execute(array("e" => $email,
                    "r" => EROLE_UNKNOWN));
            } catch (PDOException $e) {
                return false;
            }
        }
        // Si on arrive ici, c'est ok
        return true;
    }

    /**
     * Fonction retournant les utilisateurs d'un certain rôle
     * @param int $idRole Id du role
     * @return arrResult Tableau d'objet EPerson contenant les informations de l'utilisateur
     */
    public static function getUsersFromRole($idRole) {
        $sqlGetUsersFromRole = "SELECT EMAIL_PK, ROLES_CODE_PK FROM USERS WHERE ROLES_CODE_PK = :r";
        try {
            $stmt = EDatabase::prepare($sqlGetUsersFromRole);
            $stmt->execute(array(
                "r" => $idRole
            ));
            $arrResult = array();
            while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                $person = new EPerson;
                $person->email = $rec["EMAIL_PK"];
                $person->role = $rec["ROLES_CODE_PK"];
                array_push($arrResult, $person);
            }
        } catch (PDOException $e) {
            return false;
        }
        return $arrResult;
    }
    /**
     * Fonction retourant le label d'un rôle en fonction de son id
     * @param int $idRole Id du role
     * @return Objet ERole contenant les informations de l'utilisateur ou FALSE en cas d'erreur
     */
    public static function getRoleName($idRole) {
        $sqlGetUsersFromRole = "SELECT CODE_PK, LABEL FROM ROLES WHERE CODE_PK = :r";
        try {
            $stmt = EDatabase::prepare($sqlGetUsersFromRole);
            $stmt->execute(array(
                "r" => $idRole
            ));
            while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                $role = new ERole;
                $role->idRole = $rec["CODE_PK"];
                $role->labelRole = $rec["LABEL"];
                return $role;
            }
        } catch (PDOException $e) {
            return false;
        }
        return false;
    }
    

}
