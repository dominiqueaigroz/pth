<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/server/config/email.param.php';
//require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';$
require_once '../swiftmailer5/lib/swift_required.php';

 
/**
 * Sert à gerer les templates et a envoyer les emails
 */
class EEmailManager {
    
    /**
     * @brief   Class Constructor - Créer une nouvelle instance de cette classe
     *          On la met en privé pour que personne puisse créer une nouvelle instance via ' = new EEmailManager();'
     */
    private function __construct() {
        
    }

    /**
     * Cette fonction sert à l'envoie d'email
     * @param string $email   l'email du destinataire
     * @param ETemplateEmail $template le template de l'email
     */
    public static function sendEmail($email, $template){
        // Send the email
        $transport = (new Swift_SmtpTransport(EMAIL_SERVER, EMAIL_PORT, EMAIL_TRANSPORT))
        ->setUsername(EMAIL_EMAIL) // Email du compte avec lequel on envoie les emails
        ->setPassword(EMAIL_PASSWORD); // Mot de passe du compte avec lequel on envoie les emails

        try {
            $mailer = new Swift_Mailer($transport);

            $message = (new Swift_Message($template->subject))
            ->setFrom(array(EMAIL_EMAIL => 'Entreprise Ecole')) // Entreprise Ecole est un allias qui s'affichera à la place de l'email dans la boite de réception
            ->setTo(array($email))
            ->setBody($template->body,'text/html');

            $result = $mailer->send($message);

        } catch (Swift_TransportException $e) {
            var_dump($e);
            echo $e->getMessage();
            exit();
        }
    }
    
    public static function getTemplateTypes($typeCode=NULL) {
        $sql = "SELECT code, label FROM templates_type ";
        
        if($typeCode!== NULL)
        {
            $sql .= "WHERE code=" . $typeCode;
        }
        
        try{
            $stmt = EDatabase::prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
        } catch(PDOException $e)
        {
            echo "PDOException Error: " . $e->getMessage();
            return FALSE;
        }
        return $result;
    }
    
    /**
     * Retourne un tableau de Template d’email 
     * @param int $TemplateCode
     * @return ETemplate[]/boolean
     */
    public static function getTemplates($TemplateCode=NULL) {
        $sql = "SELECT templates.subject, templates.body, templates_type.label "
                . "FROM templates INNER JOIN templates_type ON templates.templates_type_code=templates_type.code ";
        
        if($TemplateCode !== NULL)
             $sql .= "WHERE templates_type.code=" . $TemplateCode;
        
        try{
            $stmt = EDatabase::prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
        } catch(PDOException $e)
        {
            echo "PDOException Error: " . $e->getMessage();
            return FALSE;
        }
        if (count($result) > 0) {
            $templates = array();
            foreach ($result as $value) {
                $template = new ETemplate();
                $template->name = isset($value["label"]) ? $value["label"] : NULL;
                $template->body = isset($value["body"]) ? $value["body"] : NULL;
                $template->subject = isset($value["subject"]) ? $value["subject"] : NULL;
                $templates[] = $template;
            }
            return $templates;
        }
        return FALSE;
    }
    
    /**
     * Modifie (ajoute si non creer) le template de l’email.
     * @param ETemplate $template
     * @return boolean
     */
    public static function setTemplate($template) {
        $sql = "INSERT INTO templates(templates_type_code, subject, body)"
                . "VALUES (" . $template->type . ", \"" . $template->subject . "\", \"" . $template->message . "\")"
                . "ON DUPLICATE KEY UPDATE subject=\"" . $template->subject . "\", body=\"" . $template->body . "\"" ;
        try{
            $stmt = EDatabase::prepare($sql);
            $stmt = $stmt->execute();
        } catch (PDOException $e)
        {
            echo "PDOException Error: " . $e->getMessage();
            return FALSE;
        }
        return TRUE;
    }
}