<?php

/**
 * Description of EHelperDay
 * Class permettant de modifier toute les données par rapport au working days.
 * @author BOREL-JAQJ
 */
class EHelperDay {

    /**
     * Ajoute les horaires de présence par semaine dans la base de donnée
     * @param {EWorkingDays} $workingDay objet contenant les 4 possibilité de constante pour décrire les présences des 5 jours de travail de la semaine et l'email. 
     * @author BOREL-JAQJ
     */
    public static function AddWorkingDays($workingDay) {

        $sqlAddWorkingDay = "INSERT INTO WORKING_DAYS (EMAIL,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY)
                             VALUES (:e, :m, :t, :w, :th, :f)";
        try {
            $stmt = EDatabase::prepare($sqlAddWorkingDay);
            if ($stmt->execute(array(
                        "e" => $workingDay->email,
                        "m" => $workingDay->Weekdays[EWD_MONDAY],
                        "t" => $workingDay->Weekdays[EWD_TUESDAY],
                        "w" => $workingDay->Weekdays[EWD_WEDNESDAY],
                        "th" => $workingDay->Weekdays[EWD_THURSDAY],
                        "f" => $workingDay->Weekdays[EWD_FRIDAY]
                    ))) {
                return TRUE;
            }
        } catch (PDOException $e) {
            return false;
        }
        return FALSE;
    }

    /**
     * Met à jour les jours de travails
     * @param {EWorkingDays} $workingDay objet contenant les 4 possibilité de constante pour décrire les présences des 5 jours de travail de la semaine et l'email. 
     * @author BOREL-JAQJ
     */
    private static function UpdateWorkingDays($workingDay) {
        $sqlUpdateWorkingDay = "UPDATE WORKING_DAYS
                             SET MONDAY= :m ,TUESDAY= :t ,WEDNESDAY= :w ,THURSDAY= :th ,FRIDAY= :f
                             WHERE EMAIL= :e ";

        try {
            $stmt = EDatabase::prepare($sqlUpdateWorkingDay);
            if ($stmt->execute(array(
                        "e" => $workingDay->email,
                        "m" => $workingDay->Weekdays[EWD_MONDAY],
                        "t" => $workingDay->Weekdays[EWD_TUESDAY],
                        "w" => $workingDay->Weekdays[EWD_WEDNESDAY],
                        "th" => $workingDay->Weekdays[EWD_THURSDAY],
                        "f" => $workingDay->Weekdays[EWD_FRIDAY]
                    ))) {
                return TRUE;
            }
        } catch (PDOException $e) {
            return false;
        }
        return FALSE;
    }

    /**
     * Récupére tout les jours de travails
     * @author BOREL-JAQJ
     */
    public static function GetAllWorkingDays() {
        
    }

    /**
     * Test l'existance de working days pour l'email donnée
     * @param string $email (Optional) L'email de l'utilisateur désiré. Défaut est "-" pour tous.
     * @return boolean True si les working days existe pour l'utilisateur.
     * @author BOREL-JAQJ
     */
    public static function WorkingDayExists($email = EWD_ALLUSERS) {

        $sqlGetWorkingDay = "SELECT MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY FROM WORKING_DAYS WHERE EMAIL= :e";

        try {
            $stmt = EDatabase::prepare($sqlGetWorkingDay);
            $stmt->execute(array("e" => $email));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) == 0) {
                return false;
            }
        } catch (PDOException $e) {
            return false;
        }
        // Aucun
        return true;
    }

    /**
     * Récupére les jours de travail par semaine d'un utilisateur, si vide selectionne l'email "-"
     * @param string $email (Optional) L'email de l'utilisateur désiré. Défaut est "-" pour tous.
     * @return EWorkingDays Retourne l'objet EWorkingDays en fonction de l'utilisateur
     * @author BOREL-JAQJ
     */
    public static function GetWorkingDays($email = EWD_ALLUSERS) {

        $sqlGetWorkingDay = "SELECT MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY FROM WORKING_DAYS WHERE EMAIL= :e";

        try {
            $stmt = EDatabase::prepare($sqlGetWorkingDay);
            $stmt->execute(array("e" => $email));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (count($result) == 0) {
                $wd = new EWorkingDays();
                $wd->email = $email;
                $wd->Weekdays = array(EWD_NONE, EWD_NONE, EWD_NONE, EWD_NONE, EWD_NONE);
                return $wd;
            }

            if (count($result) == 1) {
                $wd = new EWorkingDays();
                $wd->email = $email;
                $w1 = intval($result[0]['MONDAY']);
                $w2 = intval($result[0]['TUESDAY']);
                $w3 = intval($result[0]['WEDNESDAY']);
                $w4 = intval($result[0]['THURSDAY']);
                $w5 = intval($result[0]['FRIDAY']);
                $wd->Weekdays = array($w1, $w2, $w3, $w4, $w5);
                return $wd;
            }
        } catch (PDOException $e) {
            return false;
        }
        // Problème
        return false;
    }
    /**
     * Supprime tout les jours de travails
     * @author BOREL-JAQJ
     */
    public static function ClearAllWorkingDays() {
        try {
            $stmt = EDatabase::prepare("DELETE FROM WORKING_DAYS");
            $stmt->execute();
        } catch (PDOException $e) {
            return FALSE;
        }
        // done
        return TRUE;
    }

    /**
     * Supprime les jours de travails par semaine d'un utilisateur, si vide selectione l'email "-"
     * @param {EWorkingDays} $workingDay objet contenant les 4 possibilité de constante pour décrire les présences des 5 jours de travail de la semaine. 
     * @param {string} $email Variable contenant l'email de l'utilisateur désiré.
     * @author BOREL-JAQJ
     */
    public static function ClearWorkingDays($email = EWD_ALLUSERS) {
        try {
            EDatabase::prepare("DELETE FROM WORKING_DAYS WHERE EMAIL=:e")->execute(array("e" => $email));
        } catch (PDOException $e) {
            return FALSE;
        }
        // done
        return TRUE;
    }

    /**
     * Définit les périodes de travails. Update si elle sont existente, ajoute si innexistante.
     * @param {EWorkingDays} $workingDay objet contenant les 4 possibilité de constante pour décrire les présences des 5 jours de travail de la semaine et l'email de l'utilisateur. 
     * @author BOREL-JAQJ
     */
    public static function SetWorkingDays($workingDay) {

        if (self::WorkingDayExists($workingDay->email) == false) {
            return self::AddWorkingDays($workingDay);
        } else {
            // Autrement, mise à jour
            return self::UpdateWorkingDays($workingDay);
        }
    }

    /**
     * Définit les date de vacance. Supprime les existantes avant.
     * @param {array of DateTime} $arr Tableau contenant les différents jour de vacances/congé. 
     * @return boolean Retourne True si les jous ont été ajoutés, autrement False.
     * @author BOREL-JAQJ
     */
    public static function SetHolidays($arr) {

        try {
            EDatabase::prepare("DELETE  FROM HOLIDAYS")->execute();
        } catch (PDOException $e) {
            return FALSE;
        }
        return self::AddHolidays($arr);
    }

    /**
     * Fonction qui ajoute les date de vacance dans la base de donnée
     * @param {array of DateTime} $arr Tableau contenant les différents jour de vacances/congé. 
     * @return boolean Retourne True si les jous ont été ajoutés, autrement False.
     * @author BOREL-JAQJ
     */
    public static function AddHolidays($arr) {

        $sqlSetFriday = "INSERT INTO HOLIDAYS (DT) VALUES";


        for ($y = 0; $y < sizeof($arr); $y++) {
            if ($y > 0)
                $sqlSetFriday .= ",";
            $sqlSetFriday .= "('" . $arr[$y] . "')";
        }

        try {
            $stmt = EDatabase::prepare($sqlSetFriday);
            if ($stmt->execute()) {
                return TRUE;
            } {
                return FALSE;
            }
        } catch (PDOException $e) {
            return FALSE;
        }
    }

    /**
     * Fonction qui récupère toutes les dates de la base.
     * @return un tableau d'objet DateTime.
     * @author BOREL-JAQJ
     */
    public static function GetHolidays() {
        $sqlGetWorkingDay = "SELECT * FROM EETIMECHECKING.HOLIDAYS";

        $arr = array();
        try {
            $stmt = EDatabase::prepare($sqlGetWorkingDay);
            $stmt->execute();

            while (($rec = $stmt->fetch(PDO::FETCH_ASSOC))) {
                $d = new DateTime($rec['DT']);
                array_push($arr, $d);
            }
        } catch (PDOException $e) {
            return false;
        }
        // Le tableau qui contient les dates
        return $arr;
    }
    /**
     * Fonction qui vérifie si le jour donnée est un jour de vacance.
     * @param DateTime $dt
     * @return boolean|array
     */
    public static function IsHoliday($dt) {
        $sqlGetWorkingDay = "SELECT DT FROM EETIMECHECKING.HOLIDAYS where DT=:d";
        try {
            $stmt = EDatabase::prepare($sqlGetWorkingDay);
            $stmt->execute(array(
                "d" => $dt
            ));

            
        } catch (PDOException $e) {
            return false;
        }
        // Le tableau qui contient les dates
        return $arr;
    }
   
}
