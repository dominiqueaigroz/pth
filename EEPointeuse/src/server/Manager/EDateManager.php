<?php

//require_once './../inc.all.php';


/**
 * Description of EDateManager
 *
 * @author BURNANDL
 */
class EDateManager {

    /**
     * Cette fonction nous retourne tous les deltas pour un projet à l'aide du code du projet
     * @param {string} $projectCode Contient code projet
     * @param {string} $email (Optional) L'adresse email de l
     * 'utilisateur. Par défaut on ne filtre pas par utilisateur.
     * @return <array of EDeltaDate>  Un tableau d'objet EDeltaDate.
     */
    public static function getRecordWithDeltaByProject($projectCode, $email = '') {
        $sql = "SELECT DT_PK FROM EVENTS WHERE PROJECTS_CODE_PK = :PROJECTS_CODE_PK ORDER BY DT_PK";
        $arrFields = array("PROJECTS_CODE_PK" => $projectCode);
        if (strlen($email) > 0) {
            $sql = "SELECT DT_PK FROM EVENTS WHERE PROJECTS_CODE_PK = :PROJECTS_CODE_PK AND USERS_EMAIL_PK = :USERS_EMAIL_PK ORDER BY DT_PK";
            $arrFields = array(
                "PROJECTS_CODE_PK" => $projectCode,
                "USERS_EMAIL_PK" => $email);
        }

        //    $conn = getConnexion();
        //    $stmt = $conn->prepare($sql);
        //    $stmt->execute($arrFields);

        try {
            $stmt = EDatabase::prepare($sql);
            $stmt->execute($arrFields);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }

        // Pour détecter le changement de jours
        $previousDate = '';
        $arrDates = array();
        $arrDeltas = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            // Récupère la date de l'enregistrement
            $dt = new DateTime($row["DT_PK"]);
            $dateStr = $dt->format('Y-m-d');
            // Lorsque l'on change de date
            if (strlen($previousDate) > 0 &&
                    $previousDate != $dateStr) {
                // 
                $delta = getDeltaOnDate($arrDates);
                // Créer l'object
                $deltaDate = new EDeltaDate();
                // J'initialise avec la première date qui est
                // dans le tableau du fait quelles sont toutes
                // identiques.
                $deltaDate->dt = $arrDates[0];
                // On mets le delta qu'on vient de calculer
                $deltaDate->delta = $delta;
                array_push($arrDeltas, $deltaDate);
                // On réinitialise notre tableau de dates
                $arrDates = array();
            }
            array_push($arrDates, $dt);
            $previousDate = $dateStr;
        }
        // Pour traiter les dernières dates 
        if (count($arrDates) > 0) {
            $delta = getDeltaOnDate($arrDates);
            $deltaDate = new EDeltaDate();
            $deltaDate->dt = $arrDates[0];
            $deltaDate->delta = $delta;
            array_push($arrDeltas, $deltaDate);
        }

        // Retourne le tableau des deltas
        return $arrDeltas;
    }

    /**
     * Cette fonction calcule le delta entre plusieurs datetime.
     * 
     * @param {array of DateTime} $arr Contient les enregistrements de type DateTime
     * @return DateInterval ou FALSE si une erreur s'est produite
     */
    public static function getDeltaOnDate($arr) {
        if (count($arr) != 4)
            return FALSE;

        $delta1 = $arr[0]->diff($arr[1], false);
        $delta2 = $arr[2]->diff($arr[3], false);
        $delta3 = addDateIntervals($delta1, $delta2);
        $targetTime = getStandardTimePerDay();
        $delta4 = addDateIntervals($delta3, $targetTime);

        return $delta4;
    }

    /**
     * Retourne un objet DateInterval initialisé avec
     * le temps standard par jour.
     * @param {boolean} $inv (Optional) Inverser le temps ? Défaut est True.
     * @return DateInterval 
     */
    public static function getStandardTimePerDay($inv = true) {
        $interval = new DateInterval("P0M");
        $interval->h = ECTS_HOURPERDAY;
        $interval->i = ECTS_MINUTEPERDAY;
        $interval->invert = $inv;
        return $interval;
    }

    /**
     * Cette fonction additionne deux dates intervalle.
     * @param {DateInterval} $dateInt1  La première date intervalle
     * @param {DateInterval} $dateInt2  La seconde date intervalle
     * @return {DateInterval} Un object qui contient l'addition des deux DateInterval
     */
    public static function addDateIntervals(DateInterval $dateInt1, DateInterval $dateInt2) {
        // On initialize une date en 00 heures, 00 minutes, 00 secondes
        $im = new DateTime("00:00");
        // On clone notre temps initial
        $start = clone $im;
        // On ajoute les deux DateInterval
        $im->add($dateInt1);
        $im->add($dateInt2);
        // On soustrait le résultat des additions au temps du départ
        // Aini on prend en compte la différence possible en terme de jours
        $diff = $start->diff($im, false);
        return $diff;
    }

    /**
     * Cette fonction additionne les deltas des jours pour en faire un pour le mois
     * @param {DateInterval} $delta  La première date intervalle
     * @return {DateInterval} Un object qui contient l'addition des deltas pour le mois
     */
    public static function calculateDeltaTime($delta) {
        $dtTotal = new DateTime("00:00");
        $dtNow = new DateTime(date('Y-m-d'));
        $result = [];
        foreach ($delta as $dd) {
            if ($dd->delta !== false) {
                if ($dd->delta->invert) {
                    $dtTotal->sub(new DateInterval($dd->delta->format('PT%hH%iM%sS')));
                } else {
                    $dtTotal->add(new DateInterval($dd->delta->format('PT%hH%iM%sS')));
                }
            }
            //$nbHeures sera égal à la différence entre $dtNow et $dtTotal
            //Si le delta est négatif, $dtTotal sera égal à une date avant le jour actuel à minuit
            //(Si le delta est = à 1h, $dtTotal sera égal à la date actuelle sans l'heure (23.11.2017 à minuit) - 1h
            //Donc 22.11.2017 23:00:00)
            $deltaDiff = $dtNow->diff($dtTotal);
        }
        return $deltaDiff;
    }

}
