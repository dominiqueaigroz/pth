<?php

class EDeltaManager {

    /**
     * Cette fonction additionne les deltas des jours pour en faire un pour le mois
     * @param {DateInterval} $delta  La première date intervalle
     * @return {DateInterval} Un object qui contient l'addition des deltas pour le mois
     */
    static function calculateDeltaTime($deltas) {
        // On initialize une date en 00 heures, 00 minutes, 00 secondes
        $im = new DateTime("00:00");
        // On clone notre temps initial
        $start = clone $im;
        foreach ($deltas as $d) {
            if ($d->delta !== FALSE)
                $im->add($d->delta);
        }
        // On soustrait le résultat des additions au temps du départ
        // Aini on prend en compte la différence possible en terme de jours
        $diff = $start->diff($im, false);
        return $diff;
    }

    /**
     * Fonction qui permet de recevoir les infomations concernant les deltas par mois
     * @param string $projectCode Code du projet. (Optional) Par défaut aucun.
     * @param string $email                   Email de l'élève. (Optional) Par défaut aucun.
     * @param string $from                    Date de début. (Optional) Par défaut aucune.
     * @param string $to                        Date de fin. (Optional) Par défaut aucune.
     * @param boolean $includeDeleted  Choix de si on récupère les events deleted ou pas (Optional) par défaut false
     * @return EDeltaUserProject  Objet EDeltaUserProject avec email, projectCode ainsi qu'un tableau de EDeltaDate
     */
    public static function getDeltasByMonthForUser($projectCode = "", $email = "", $from = null, $to = null, $includeDeleted = FALSE) {

        $sql = "SELECT DT_PK,USERS_EMAIL_PK FROM EVENTS";
        $filter = "";
        $arrFields = array();
        if (strlen($email) > 0) {
            $filter .= " USERS_EMAIL_PK = :e";
            $arrFields['e'] = $email;
        }
        if (strlen($projectCode) > 0) {
            if (strlen($filter) > 0)
                $filter .= " AND ";
            $filter .= " PROJECTS_CODE_PK = :p";
            $arrFields['p'] = $projectCode;
        }
        if (strlen($from) > 0 && strlen($to) > 0) {
            if (strlen($filter) > 0)
                $filter .= " AND ";
//On ajoute 17h pour éviter que le between ne prenne la date à minuit
            $filter .= " DT_PK BETWEEN DAY(:f) AND DAY(DATE_ADD(:t, INTERVAL 1 Day)";
            $arrFields['f'] = $from;
            $arrFields['t'] = $to;
        }
        if ($includeDeleted === FALSE) {
            if (strlen($filter) > 0)
                $filter .= " AND ";
            $filter .= " DELETED = :d";
            $arrFields['d'] = 0;
        }
        if (strlen($filter) > 0) {
            $sql .= " WHERE " . $filter . " ORDER BY USERS_EMAIL_PK, DT_PK ASC";
        }

        try {
            $stmt = EDatabase::prepare($sql);
            $stmt->execute($arrFields);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }

        // Pour détecter le changement de jours
        $standardTime = getStandardTimePerDay();
        $standardTimeHalf = getStandardTimePerHalfDay();
        $previousDate = '';
        $arrDates = array();
        $arrDeltas = array();
        $arrResult = array();
        $previousEmail = "";
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            // Récupère la date de l'enregistrement
            $dt = new DateTime($row["DT_PK"]);
            $em = $row["USERS_EMAIL_PK"];

            $dateStr = $dt->format('Y-m-d');

            // Quand on change de jour ou de user
            if (strlen($dateStr) > 0 && count($arrDates) > 0 &&
                    ($previousDate != $dateStr || $em != $previousEmail)) {

                // récupère le nombre de pointage attendu
                $attendedCount = eventCountOnDay($arrDates[0], $previousEmail);
                // Si le nombre de pointage attendu correspond
                // à ceux qu'on a dans le tableau, on peut calculer
                // un delta
                if ($attendedCount == count($arrDates)) {
                    // On va calculer le delta de tous les enregistrements
                    // qu'on a dans le tableau qui doivent être du même jour
                    $deltaDate = new EDeltaDate();
                    $deltaDate->dt = $arrDates[0];
                    $delta = EDeltaManager::getTotalDeltaEvents($arrDates);
                    $deltaDate->delta = addDateIntervals($delta, ($attendedCount > 2) ? $standardTime : $standardTimeHalf);
                    array_push($arrDeltas, $deltaDate);
                } else {
                    // Quand on a pas le nombre de pointage attendu
                    $deltaDate = new EDeltaDate();
                    $deltaDate->dt = $arrDates[0];
                    $deltaDate->delta = FALSE;
                    array_push($arrDeltas, $deltaDate);
                }
                // A la fin, après changement de jour, on efface le tableau
                $arrDates = array();
            } // Endif: date différente
            // On va stocker l'enregistrement dans le tableau des dates
            array_push($arrDates, $dt);


            // Quand on change d'utilisateur
            if ($em != $previousEmail && count($arrDeltas) > 0) {
                //création de la classe EDeltaUserProject
                //+ attribution des valeurs
                $deltaUserProject = new EDeltaUserProject();
                $deltaUserProject->email = $previousEmail;
                $deltaUserProject->date = $arrDeltas[0]->dt;
                $deltaUserProject->projectCode = $projectCode;
                if ($arrDeltas[0]->isValid())
                    $deltaUserProject->deltas = EDeltaManager::calculateDeltaTime($arrDeltas);
                else
                    $deltaUserProject->deltas = FALSE;
                array_push($arrResult, $deltaUserProject);
                // Clear des deltas
                $arrDeltas = array();
            } // Endif: user différent
            // On conserve le mail courant pour détecter les changements
            $previousEmail = $em;
            // et la date
            $previousDate = $dateStr;
        }
        // Retourne le tableau d'objet EDeltaUserProject
        return $arrResult;
    }

    /*
     * Cette fonction nous retourne tous les deltas pour un projet à l'aide du code du projet
     * @param {string} $projectCode Contient code projet
     * @param {string} $email (Optional) L'adresse email de l'utilisateur. Par défaut on ne filtre pas par utilisateur.
     * @return <array of EDeltaDate>  Un tableau d'objet EDeltaDate.
     */

    public static function getRecordWithDelta($projectCode = '', $email = '', $from = '', $to = '', $orderBy = 'DT_PK', $includeDeleted = FALSE) {

        $sql = "SELECT DT_PK,USERS_EMAIL_PK FROM  EVENTS";
        $filter = "";
        $arrFields = array();
        if (strlen($email) > 0) {
            $filter .= " USERS_EMAIL_PK = :e";
            $arrFields['e'] = $email;
        }
        if (strlen($projectCode) > 0) {
            if (strlen($filter) > 0)
                $filter .= " AND ";
            $filter .= " PROJECTS_CODE_PK = :p";
            $arrFields['p'] = $projectCode;
        }
        if (strlen($from) > 0 && strlen($to) > 0) {
            if (strlen($filter) > 0)
                $filter .= " AND ";
//On ajoute 17h pour éviter que le between ne prenne la date à minuit
            $filter .= " DT_PK BETWEEN :f AND DATE_ADD(:t, INTERVAL 17 Hour)";
            $arrFields['f'] = $from;
            $arrFields['t'] = $to;
        }
        if ($includeDeleted === FALSE) {
            if (strlen($filter) > 0)
                $filter .= " AND ";
            $filter .= " DELETED = :d";
            $arrFields['d'] = 0;
        }
        if (strlen($filter) > 0) {
            $sql .= " WHERE" . $filter;
        }
        $sql .= " ORDER BY ";
        if (strlen($orderBy) > 0)
            $sql .= $orderBy;
        else
            $sql .= 'DT_PK';
        try {
            $stmt = EDatabase::prepare($sql);
            $stmt->execute($arrFields);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }

        // Pour détecter le changement de jours
        $previousDate = '';
        $arrDates = array();
        $arrDeltas = array();
        $arrResult = array();
        $standardTime = getStandardTimePerDay();
        $standardTimeHalf = getStandardTimePerHalfDay();
        $previousEmail = "";

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            // Récupère la date de l'enregistrement
            $dt = new DateTime($row["DT_PK"]);
            $em = $row["USERS_EMAIL_PK"];
            $dateStr = $dt->format('Y-m-d');


            // Lorsque l'on change de date
            if (strlen($previousDate) > 0 &&
                    $previousDate != $dateStr) {
                if ($attendedCount == count($arrDates)) {
                    // On va calculer le delta de tous les enregistrements
                    // qu'on a dans le tableau qui doivent être du même jour
                    $deltaDate = new EDeltaDate();
                    $deltaDate->dt = $arrDates[0];
                    $delta = EDeltaManager::getTotalDeltaEvents($arrDates);
                    $deltaDate->delta = addDateIntervals($delta, ($attendedCount > 2) ? $standardTime : $standardTimeHalf);
                    array_push($arrDeltas, $deltaDate);
                } else {
                    // Quand on a pas le nombre de pointage attendu
                    $deltaDate = new EDeltaDate();
                    $deltaDate->dt = $arrDates[0];
                    $deltaDate->delta = FALSE;
                    array_push($arrDeltas, $deltaDate);
                }
                // On réinitialise notre tableau de dates
                $arrDates = array();
            }
            array_push($arrDates, $dt);
            $attendedCount = eventCountOnDay($arrDates[0], $previousEmail);

            $previousDate = $dateStr;
            $previousEmail = $em;
        }
        // Pour traiter les dernières dates 
        if (count($arrDates) > 0) {
            $delta = EDeltaManager::getTotalDeltaEvents($arrDates);
            $deltaDate = new EDeltaDate();
            $deltaDate->dt = $arrDates[0];
            if ($delta !== FALSE)
                $deltaDate->delta = addDateIntervals($delta, ($attendedCount > 2) ? $standardTime : $standardTimeHalf);
            else
                $deltaDate->delta = FALSE;
            array_push($arrDeltas, $deltaDate);
        }

// Retourne le tableau des deltas
        return $arrDeltas;
    }

    /**
     * Cette fonction calcule le delta entre plusieurs datetime.
     * @remark Cette fonction est marquée obsolète. Veuillez utiliser getTotalDeltaEvents()
     * 
     * @param {array of DateTime} $arr Contient les enregistrements de type DateTime
     * @return DateInterval ou FALSE si une erreur s'est produite
     * 
     */
    public static function getDeltaFromArray($arr) {
        echo "ATTENTION FONCTION OBSOLETE: Veuillez utiliser EDeltaManager::getTotalDeltaEvents().";
        if (count($arr) != 4)
            return FALSE;

        $delta1 = $arr[0]->diff($arr[1], false);
        $delta2 = $arr[2]->diff($arr[3], false);
        $delta3 = addDateIntervals($delta1, $delta2);
        $targetTime = getStandardTimePerDay();
        $delta4 = addDateIntervals($delta3, $targetTime);

        return $delta4;
    }
    
    /**
     * Cette fonction sort tout les event d'un code projet donnÃ©
     * @param {string} $project contient le code du projet en format string
     * @return {array of DateTime} ou false si il y a une erreur
     */
    public static function getRecordFromProject($project) {
        try {
            $tblDelta = array();
            $stmt = EDatabase::prepare('SELECT DT_PK FROM EETIMECHECKING.EVENTS where PROJECTS_CODE_PK=:ProjectCode order by DT_PK;');
            $stmt->execute(array(
                ':ProjectCode' => $project
            ));
            while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                $delta = $rec['DT_PK'];
                $time = strtotime($delta);
                $deltaDate = date("Y-m-d H:i:s", $time);
                $tastos = new DateTime($deltaDate);
                array_push($tblDelta, $tastos);
            }
            return $tblDelta;
        } catch (PDOException $e) {
            return false;
        }
    }

    /**
     * Cette fonction calcule le delta entre plusieurs datetime.
     * 
     * @param {array of DateTime} $arr Contient les enregistrements de type DateTime
     * @return DateInterval ou FALSE si une erreur s'est produite
     */
    public static function getTotalDeltaEvents($arr) {
        $cnt = count($arr);
        if ($cnt < 2)
            return FALSE;
        // On initialize une date en 00 heures, 00 minutes, 00 secondes
        $im = new DateTime("00:00");
        // On clone notre temps initial
        $start = clone $im;

        // On démarre sur le second élément du tableau
        $i = 1;
        while ($i < $cnt) {
            $d = $arr[$i - 1]->diff($arr[$i], false);
            $im->add($d);
            $i += 2; // On passe par deux éléments
        }
        // On soustrait le rÃ©sultat des additions au temps du départ
        // Aini on prend en compte la diffÃ©rence possible en terme de jours
        $diff = $start->diff($im, false);

        return $diff;
        /*
          $delta3 = addDateIntervals($delta1, $delta2);
          $targetTime = getStandardTimePerDay();
          $delta4 = addDateIntervals($delta3, $targetTime);
         */
    }

}
