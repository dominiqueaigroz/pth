<?php

/**
 * Description of EProjectManager
 * 
 *
 * @author BURNANDL
 */
class EProjectManager {

    /**
     * Fonction qui ajoute un élève dans un projet à l'aide de l'email et du code du projet
     * @param {string} $projectCode
     * @param {string} $email
     * @return {boolean} true si ajouté avec succès, false si erreur s'est produite
     */
    public static function addUser($projectCode, $email) {
        $sqlInsertUserInProject = "INSERT INTO USERS_HAS_PROJECTS (USERS_EMAIL_PK, PROJECTS_CODE_PK) VALUES (:e,:p)";
        $stmt = EDatabase::prepare($sqlInsertUserInProject);
        if ($stmt->execute(array(
                    "e" => $email,
                    "p" => $projectCode
                ))
        ) {
            return true;
        } {
            //si fail
            return false;
        }
    }

    /**
     * Fonction qui ajoute un manager dans un projet à l'aide de l'email et du code du projet
     * @param {string} $projectCode
     * @param {string} $email
     * @return {boolean} true si ajouté avec succès, false si erreur s'est produite
     */
    public static function addManager($projectCode, $email) {
        $sqlInsertUserInProject = "INSERT INTO PROJECTS_HAS_MANAGERS (USERS_EMAIL_PK, PROJECTS_CODE_PK) VALUES (:e,:p)";
        $stmt = EDatabase::prepare($sqlInsertUserInProject);
        if ($stmt->execute(array(
                    "e" => $email,
                    "p" => $projectCode
                ))
        ) {
            return true;
        } {
            //si fail
            return false;
        }
    }

    /**
     * Fonction qui supprime une élève dans un projet à l'aide de l'email de l'élève et du code du projet
     * @param {string} $projectCode
     * @param {string} $email
     * @return {boolean} true si supprimé avec succès, false si erreur s'est produite
     */
    public static function deleteUser($projectCode, $email) {
        $sqlDeleteUserInProject = "DELETE FROM PROJECTS_HAS_USERS WHERE PROJECTS_CODE_PK = :PROJECTS_CODE_PK AND USERS_EMAIL_PK = :USERS_EMAIL_PK";
        $stmt = EDatabase::prepare($sqlDeleteUserInProject);
        if ($stmt->execute(array(
                    "USERS_EMAIL_PK" => $email,
                    "PROJECTS_CODE_PK" => $projectCode
                ))
        ) {
            return true;
        }
        //si fail
        return FALSE;
    }

    /**
     * Enlève un manager d'un projet
     * @param {string} $projectCode
     * @param {string} $email
     * @return {boolean}
     */
    public static function deleteManager($projectCode, $email) {
        $sqlDeleteUserInProject = "DELETE FROM PROJECTS_HAS_MANAGERS WHERE PROJECTS_CODE_PK = :PROJECTS_CODE_PK AND USERS_EMAIL_PK = :USERS_EMAIL_PK";
        $stmt = EDatabase::prepare($sqlDeleteUserInProject);
        if ($stmt->execute(array(
                    "USERS_EMAIL_PK" => $email,
                    "PROJECTS_CODE_PK" => $projectCode
                ))
        ) {
            return true;
        }
        //si fail
        return FALSE;
    }

    /**
     * Fonction qui permet de créer un projet avec un code, un label et une description
     * @param {string} $projectCode Code du projet
     * @param {string} $label Label du projet
     * @param {string} $description Description du projet
     * @param {datetime} $forecastedHour Heure(s) qui doive(nt) être faites
     * @param {datetime} $forecastedEndDate Date $ laquelle le projet devrait se terminer
     * @param {string} $internal_project_key La clé d'authentification du projet
     * @param {datetime} $start_date Date de début du projet
     * @param {datetime} $end_date Date de fin du projet
     * @return {boolean} true si ajouté avec succès, false si erreur s'est produite
     */
    public static function addProject($projectCode, $label, $description, $forecastedHour, $forecastedEndDate, $start_date = NULL, $end_date = NULL, $status_code = 3) {
        try {
            // On démarre les transactions afin d'être capable de revenir à ce point
            // si on a une erreur qui survient sur le deuxième insert.
            EDatabase::beginTransaction();
            $sql = "INSERT INTO PROJECTS (CODE_PK, LABEL, PROJECTS_STATUS_CODE_PK) VALUES (:CODE_PK,:LABEL, :PROJECTS_STATUS_CODE_PK)";
            $stmt = EDatabase::prepare($sql);
            $stmt->execute(array(
                "CODE_PK" => $projectCode,
                "LABEL" => $label,
                "PROJECTS_STATUS_CODE_PK" => $status_code
            ));
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            EDatabase::rollBack();
            return false;
        }

        try {
            $sql = "INSERT INTO PROJECTS_INFOS (PROJECTS_CODE_PK, FORECASTED_PROJECT_HOURS,FORECASTED_END_DATE, DESCRIPTION,START_DATE,END_DATE) "
                    . "VALUES (:PROJECTS_CODE_PK, :FORECASTED_PROJECT_HOURS, :FORECASTED_END_DATE, :DESCRIPTION, :START_DATE, :END_DATE)";
            $stmt = EDatabase::prepare($sql);

            $startDate = NULL;
            if ($start_date !== NULL)
                $startDate = $start_date->date;

            $endDate = NULL;
            if ($end_date !== NULL)
                $endDate = $end_date->date;


            $stmt->execute(array(
                "PROJECTS_CODE_PK" => $projectCode,
                "FORECASTED_PROJECT_HOURS" => $forecastedHour,
                "FORECASTED_END_DATE" => $forecastedEndDate,
                "DESCRIPTION" => $description,
                "START_DATE" => $startDate,
                "END_DATE" => $endDate
            ));
            // Tout s'est bien passé, on peut commiter les deux transactions
            EDatabase::commit();
            return true;
        } catch (PDOException $e) {
            // Ici le roll back permet d'annuler l'insertion de la première transaction.
            EDatabase::rollBack();
            echo "Error: " . $e->getMessage();
            return false;
        }

        //si fail
        return false;
    }

    /**
     * Fonction qui permet de modifier les paramètres du projet
     * @param {string} $projectCode Code du projet
     * @param {string} $label Label du projet
     * @param {string} $description Description du projet
     * @param {datetime} $forecastedHour Heure(s) qui doive(nt) être faites
     * @param {datetime} $forecastedEndDate Date $ laquelle le projet devrait se terminer
     * @param {string} $internal_project_key La clé d'authentification du projet
     * @param {datetime} $start_date Date de début du projet
     * @param {datetime} $end_date Date de fin du projet
     * @return {boolean} true si ajouté avec succès, false si erreur s'est produite
     */
    public static function modifyProject($projectCode, $label, $description, $forecastedHour, $forecastedEndDate, $start_date = null, $end_date = null, $status_code = 3) {
        $sqlModifyProject = "UPDATE PROJECTS, PROJECTS_INFOS SET PROJECTS.LABEL = :LABEL, PROJECTS.PROJECTS_STATUS_CODE_PK = :PROJECTS_STATUS_CODE_PK
            , PROJECTS_INFOS.FORECASTED_PROJECT_HOURS = :FORECASTED_PROJECT_HOURS,
            PROJECTS_INFOS.FORECASTED_END_DATE = :FORECASTED_END_DATE, PROJECTS_INFOS.DESCRIPTION = :DESCRIPTION, 
        PROJECTS_INFOS.START_DATE = :START_DATE, PROJECTS_INFOS.END_DATE = :END_DATE WHERE PROJECTS_INFOS.PROJECTS_CODE_PK = :CODE_PK AND PROJECTS.CODE_PK = :CODE_PK";
        $stmt = EDatabase::prepare($sqlModifyProject);

        $endDate = NULL;
        if ($end_date !== NULL)
            $endDate = $end_date->date;
        
        $startDate = NULL;
        if ($start_date !== NULL)
            $startDate = $start_date->date;

        if ($stmt->execute(array(
                    "CODE_PK" => $projectCode,
                    "DESCRIPTION" => $description,
                    "LABEL" => $label,
                    "PROJECTS_STATUS_CODE_PK" => $status_code,
                    "FORECASTED_PROJECT_HOURS" => $forecastedHour,
                    "FORECASTED_END_DATE" => $forecastedEndDate,
                    "START_DATE" => $startDate,
                    "END_DATE" => $endDate
                        )
                )
        ) {
            return true;
        } 
        //si fail
        return false;
    }
    /**
 * Cette fonction nous envoie les informations sur un projet
 * @return <array>  Un tableau contenant les éléments de la base de données
 */
public static function getProjectInfos($code) {
    try {
        $connect = EDatabase::getInstance();
        $req = $connect->prepare("SELECT * FROM PROJECTS p, PROJECTS_INFO pi WHERE PROJECTS_CODE_PK = CODE_PK AND CODE_PK = :e");
        $req->execute(array(
            "e" => $code));
        $result = $req->fetch(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        return FALSE;
    }
    return $result;
}

    /**
     * Fonction qui permet de supprimer un projet sans événement(pointages) en fonction du code du projet ou s'il y a des records, le champs deleted sera set à 1
     * @param {string} $projectCode
     * @return {boolean} true si supprimé avec succès, false si erreur s'est produite
     */
    public static function deleteProject($projectCode) {
        $sqlGetProject = "SELECT PROJECTS_CODE_PK FROM EVENTS WHERE PROJECTS_CODE_PK = :CODE_PK";
        $sqlDeleteProject = "DELETE FROM PROJECTS WHERE CODE_PK = :CODE_PK";
        $sqlSet1DeletedProject = "UPDATE PROJECTS SET DELETED = 1 WHERE CODE_PK = :CODE_PK";
        $sqlDeleteProjectInfo = "DELETE FROM PROJECTS_INFOS WHERE PROJECTS_CODE_PK = :CODE_PK";
        $stmtGetProject = EDatabase::prepare($sqlGetProject);
        if ($stmtGetProject->execute(array(
                    "CODE_PK" => $projectCode
                ))) {
            $a = $stmtGetProject->fetchAll();
            if (count($a) === 0) {
                $stmtDeleteProjectInfo = EDatabase::prepare($sqlDeleteProjectInfo);
                if ($stmtDeleteProjectInfo->execute(array(
                            "CODE_PK" => $projectCode
                        ))) {
                    $stmtDeleteProject = EDatabase::prepare($sqlDeleteProject);
                    $stmtDeleteProject->execute(array(
                        "CODE_PK" => $projectCode
                    ));
                }
                return true;
            } else {
                $stmtDeleteProject = EDatabase::prepare($sqlSet1DeletedProject);
                $stmtDeleteProject->execute(array(
                    "CODE_PK" => $projectCode
                ));
                return true;
            }
        } {
            //si fail
            return false;
        }



        $stmt = EDatabase::prepare($sqlDeleteProject);
        if ($stmt->execute(array(
                    "CODE_PK" => $projectCode
                ))) {
            return true;
        } {
            //si fail
            return false;
        }
    }

    /**
     * Fonction qui permet de tester si l'utilisateur est dans un projet.
     * @param string $projectCode Contient code projet
     * @param string $email L'adresse email de la personne
     * @return True ou False.
     */
    public static function IsUserOnProject($projectCode, $email) {
        $sql = "SELECT * FROM .USERS_HAS_PROJECTS up,USERS u,PROJECTS p WHERE up.PROJECTS_CODE_PK = p.CODE_PK and up.USERS_EMAIL_PK = u.EMAIL_PK and u.EMAIL_PK =:EMAIL and CODE_PK = :PROJECTCODE ";
        $stmt = EDatabase::prepare($sql);
        $stmt->execute(array(
            "EMAIL" => $email,
            "PROJECTCODE" => $projectCode
        ));
        $result = $stmt->fetchAll($fetch_style = PDO::FETCH_ASSOC);
        if (sizeof($result[0]) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Fonction Récupère la liste des projets pour un utilisateur
     * @param string $email L'adresse email de la personne
     * @return array of EProject retourne un tableau d'objet projet, sinon retourne FALSE
     */
    public static function getUserProjects($email) {
        $arr = array();
        $stmt = EDatabase::prepare("SELECT p.CODE_PK, p.LABEL, p.DESCRIPTION FROM projects p, users_has_projects up WHERE p.CODE_PK = up.PROJECTS_CODE_PK  and up.USERS_EMAIL_PK = :USERS_EMAIL_PK");
        if ($stmt->execute(array("USERS_EMAIL_PK" => $email))) 
        {
            while (($rec = $stmt->fetch())) {
                $project= new EProject($rec['CODE_PK'], $rec['LABEL'], $rec['DESCRIPTION'], null, null, null, null, null);
                array_push($arr, $project);
            }
        }
        return $arr;
        /*
          {
          //si fail
          return false;
          }
         */
    }

    /**
     * Cette fonction nous retourne tous les deltas pour un projet à l'aide du code du projet
     * @param {string} $projectCode Contient code projet
     * @param {string} $email (Optional) L'adresse email de l
     * 'utilisateur. Par défaut on ne filtre pas par utilisateur.
     * @return <array of EDeltaDate>  Un tableau d'objet EDeltaDate.
     */
    public static function getRecordWithDeltaByProject($projectCode, $email = '') {
        $sql = "SELECT DT_PK FROM EVENTS WHERE PROJECTS_CODE_PK = :PROJECTS_CODE_PK ORDER BY DT_PK";
        $arrFields = array("PROJECTS_CODE_PK" => $projectCode);
        if (strlen($email) > 0) {
            $sql = "SELECT DT_PK FROM events WHERE PROJECTS_CODE_PK = :PROJECTS_CODE_PK AND USERS_EMAIL_PK = :USERS_EMAIL_PK ORDER BY DT_PK";
            $arrFields = array(
                "PROJECTS_CODE_PK" => $projectCode,
                "USERS_EMAIL_PK" => $email);
        }

        try {
            $stmt = EDatabase::prepare($sql);
            $stmt->execute($arrFields);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }

        // Pour détecter le changement de jours
        $previousDate = '';
        $arrDates = array();
        $arrDeltas = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            // Récupère la date de l'enregistrement
            $dt = new DateTime($row["DT_PK"]);
            $dateStr = $dt->format('Y-m-d');
            // Lorsque l'on change de date
            if (strlen($previousDate) > 0 &&
                    $previousDate != $dateStr) {
                // 
                $delta = getDeltaOnDate($arrDates);
                // Créer l'object
                $deltaDate = new EDeltaDate();
                // J'initialise avec la première date qui est
                // dans le tableau du fait quelles sont toutes
                // identiques.
                $deltaDate->dt = $arrDates[0];
                // On mets le delta qu'on vient de calculer
                $deltaDate->delta = $delta;
                array_push($arrDeltas, $deltaDate);
                // On réinitialise notre tableau de dates
                $arrDates = array();
            }
            array_push($arrDates, $dt);
            $previousDate = $dateStr;
        }
        // Pour traiter les dernières dates 
        if (count($arrDates) > 0) {
            $delta = getDeltaOnDate($arrDates);
            $deltaDate = new EDeltaDate();
            $deltaDate->dt = $arrDates[0];
            $deltaDate->delta = $delta;
            array_push($arrDeltas, $deltaDate);
        }

        // Retourne le tableau des deltas
        return $arrDeltas;
    }

    /**
     * Retourne un objet DateInterval initialisé avec
     * le temps standard par jour.
     * @param {boolean} $inv (Optional) Inverser le temps ? Défaut est True.
     * @return DateInterval 
     */
    public static function getStandardTimePerDay($inv = true) {
        $interval = new DateInterval("P0M");
        $interval->h = ECTS_HOURPERDAY;
        $interval->i = ECTS_MINUTEPERDAY;
        $interval->invert = $inv;
        return $interval;
    }

    /**
     * Fonction qui retourne les pointages d'un élève donné pour un jour donné
     * @param {string} $email L'email de l'utilisateur 
     * @param {date} $date la date choisie 
     * @return array
     */
    public static function getRecordByDateUser($email, $date) {
        $stmt = EDatabase::prepare("SELECT DT_PK from EVENTS where USERS_EMAIL_PK='" . $email . "'AND DT_PK LIKE'" . $date . "%'");
        $stmt->execute();
        $tblRecordUserDate = array();
        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $ProjectEvent = new EProjectEvent();
            $dt = new DateTime($rec["DT_PK"]);
            $ProjectEvent->dt = $dt;
            $ProjectEvent->email = $email;
            array_push($tblRecordUserDate, $ProjectEvent);
        }
        return $tblRecordUserDate;
    }

    /**
     * Fonction qui retourne tous les pointages pour un élève donné
     * @param {string} $email L'email de l'utilisateur 
     * @return {array of EProjectEvent} les pointages de l'élève dans un tableau
     */
    public static function getEventByUser($email) {

        $stmt = EDatabase::prepare("SELECT DT_PK from EVENTS where USERS_EMAIL_PK='" . $email . "'");
        $stmt->execute();
        $tblRecordUser = array();

        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $ProjectEvent = new EProjectEvent();
            $ProjectEvent->dt = $rec["DT_PK"];
            $ProjectEvent->email = $email;
            array_push($tblRecordUser, $ProjectEvent);
        }
        return $tblRecordUser;
    }

    /**
     * Fonction qui retourne les pointages d'un élève donné pour un jour donné
     * @param {string} $email L'email de l'utilisateur 
     * @param {date} $date la date choisie 
     * @return array
     */
    public static function getEventByDateUserByProject($email, $date, $project) {
        $stmt = EDatabase::prepare("SELECT DT_PK from EVENTS WHERE USERS_EMAIL_PK='" . $email . "'AND PROJECTS_CODE_PK='" . $project . "'  AND DT_PK LIKE'" . $date . "%'");
        $stmt->execute();
        $tblRecordUserDate = array();
        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $ProjectEvent = new EProjectEvent();
            $ProjectEvent->dt = $rec["DT_PK"];
            $ProjectEvent->email = $email;
            array_push($tblRecordUserDate, $ProjectEvent);
        }
        return $tblRecordUserDate;
    }

    /**
     * Fonction qui retourne les utilisateurs dans un projet
     * @param {string} $projCode Le code du projet
     */
    public static function getUsers($projCode) {
        try {
            $stmt = EDatabase::prepare('SELECT `USERS_EMAIL_PK` FROM `PROJECTS_HAS_WORKERS` WHERE `PROJECTS_CODE_PK` = :p');
            $stmt->execute(array(
                'p' => $projCode
            ));
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return False;
        }
        $tblUsers = array();
        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $email = $rec['USERS_EMAIL_PK'];
            array_push($tblUsers, $email);
        }
        return $tblUsers;
    }

    /**
     * Fonction qui retourne les managers dans un projet
     * @param {string} $projCode Le code du projet
     * return 
     */
    public static function getManagers($projCode) {
        try {
            $stmt = EDatabase::prepare('SELECT `USERS_EMAIL_PK` FROM `PROJECTS_HAS_MANAGERS` WHERE `PROJECTS_CODE_PK` = :p');
            $stmt->execute(array(
                'p' => $projCode
            ));
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return False;
        }
        $tblUsers = array();
        while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $email = $rec['USERS_EMAIL_PK'];
            array_push($tblUsers, $email);
        }
        return $tblUsers;
    }

 /**
     * Cette fonction nous retourne tous les projets sur lequel l'utilisateur est enregistré
     * @param string $email L'email de l'utilisateur 
     * @param boolean $allProject Si true, alors on récupère tous les projet. Défaut est False.
     * @return EProject tableau d'Objet EProject
     */
    public static function getProjects($email, $allProjects = false) {

        $arrParam = array();
        $sql = "";
        
        if ($allProjects == true)
        {
            $sql = "SELECT uhp.USERS_EMAIL_PK, uhp.PROJECTS_CODE_PK,  inf.FORECASTED_PROJECT_HOURS, inf.FORECASTED_END_DATE, inf.DESCRIPTION, inf.INTERNAL_PROJECT_KEY , inf.START_DATE , inf.END_DATE , 
                    pro.CODE_PK, pro.LABEL, pro.PROJECTS_STATUS_CODE_PK, pro.DELETED 
                    FROM USERS_HAS_PROJECTS AS uhp, PROJECTS AS pro, PROJECTS_INFOS AS inf 
                    WHERE pro.CODE_PK = inf.PROJECTS_CODE_PK AND uhp.PROJECTS_CODE_PK = pro.CODE_PK";
        }
        else 
        {
            $sql = "SELECT uhp.USERS_EMAIL_PK, uhp.PROJECTS_CODE_PK,  inf.FORECASTED_PROJECT_HOURS, inf.FORECASTED_END_DATE, inf.DESCRIPTION, inf.INTERNAL_PROJECT_KEY , inf.START_DATE , inf.END_DATE , 
                    pro.CODE_PK, pro.LABEL, pro.PROJECTS_STATUS_CODE_PK, pro.DELETED 
                    FROM USERS_HAS_PROJECTS AS uhp, PROJECTS AS pro, PROJECTS_INFOS AS inf 
                    WHERE pro.CODE_PK = inf.PROJECTS_CODE_PK AND uhp.PROJECTS_CODE_PK = pro.CODE_PK AND uhp.USERS_EMAIL_PK = :EMAIL_PK";
            $arrParam = array("EMAIL_PK" => $email);
        }
        
        $stmt = EDatabase::prepare($sql);
        if ($stmt->execute($arrParam)) {
            $result = $stmt->fetchAll();
            $arResult = array();
            foreach ($result as $r) {
                $EProject = new EProject($r["CODE_PK"], $r["LABEL"], $r["DESCRIPTION"], $r["FORECASTED_PROJECT_HOURS"], $r["FORECASTED_END_DATE"], $r["INTERNAL_PROJECT_KEY"], $r["START_DATE"], $r["END_DATE"]);
                array_push($arResult, $EProject);
            }
            return $arResult;
        }
        return false;
    }
  

    /**
     * Retourne la clef interne du projet spécifié
     * @param {string} $projCode
     * @return {string}
     */
    public static function getProjectInternalKey($projCode) {
        try {
            $stmt = EDatabase::prepare('SELECT i.INTERNAL_PROJECT_KEY,p.LABEL FROM PROJECTS p, PROJECTS_INFOS i WHERE i.PROJECTS_CODE_PK = :code AND i.PROJECTS_CODE_PK = p.CODE_PK');
            $stmt->execute(array(
                'code' => $projCode)
            );
            $result = $stmt->fetch();
            $project = new EProject($projCode, $result['LABEL'], null, null, null, $result['INTERNAL_PROJECT_KEY'], null, null);
            return $project;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * Génére une clef interne
     * @param {int} $len La longueur de la clef
     * @return {string} La clef
     */
    public static function generateInternalProjectKey($len = 16) {
        $key = '';
        for ($i = 0; $i < $len; $i++) {
            $charType = rand(1, 3);
            switch ($charType) {
                case 1:
                    $minRange = 48;
                    $maxRange = 57;
                    break;
                case 2:
                    $minRange = 65;
                    $maxRange = 90;
                    break;
                case 3:
                    $minRange = 97;
                    $maxRange = 122;
                    break;
                default :
                    break;
            }
            $key .= chr(rand($minRange, $maxRange));
        }
        return $key;
    }

    /**
     * Retourne tous les projets actifs (non terminés) sauf celui par défaut
     */
    public static function getAllActiveProjects() {
        try {
            $stmt = EDatabase::prepare('SELECT p.CODE_PK, p.LABEL, i.DESCRIPTION, i.FORECASTED_PROJECT_HOURS, i.FORECASTED_END_DATE, i.INTERNAL_PROJECT_KEY, i.START_DATE, i.END_DATE FROM PROJECTS p, PROJECTS_INFOS i '
                            . 'WHERE p.CODE_PK = i.PROJECTS_CODE_PK AND NOW() < END_DATE;');
            $stmt->execute();
            $projects = array();
            while ($rec = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                if (self::isNonDefaultProject($rec['INTERNAL_PROJECT_KEY'])) {
                    $project = new EProject($rec['CODE_PK'], $rec['LABEL'], $rec['DESCRIPTION'], $rec['FORECASTED_PROJECT_HOURS'], $rec['FORECASTED_END_DATE'], $rec['INTERNAL_PROJECT_KEY'], $rec['START_DATE'], $rec['END_DATE']);
                    array_push($projects, $project);
                }
            }
            return $projects;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * Vérifie si un projet n'est pas par défauts
     * @param {string} $internalKey
     * @return bool
     */
    public static function isNonDefaultProject($internalKey) {
        $pattern = "/[A-Za-z0-9]/";
        return preg_match($pattern, $internalKey);
    }

    /**
     * Fonction insérant des élèves sur le projet
     * @param type $users   Tableau d'emails 
     */
    public static function setUserOnProject($users, $projectCode) {
        
            $stmt = EDatabase::prepare("DELETE FROM `PROJECTS_HAS_WORKERS` WHERE PROJECTS_CODE_PK=:p");
            $stmt->execute(array(
                "p" => $projectCode
            ));
        
        
        foreach ($users as $user) {
            $stmt = EDatabase::prepare("INSERT INTO `PROJECTS_HAS_WORKERS` (USERS_EMAIL_PK, PROJECTS_CODE_PK) VALUES (:e,:p)");
            $stmt->execute(array(
                "e" => $user,
                "p" => $projectCode
            ));
        } {
            return true;
        }
        //si fail
        return false;
    }

     /**
     * Cette fonction nous retourne toute les informations d'un projet grâce à son code.
     * @param $projectcode code du projet
     * @return L'objet EProject ou FALSE si une erreur survient
     */
    public static function getInfoProject($projectcode) {
        try {
            $stmt = EDatabase::prepare('SELECT * FROM EETIMECHECKING.PROJECTS_INFOS pi,EETIMECHECKING.PROJECTS p where pi.PROJECTS_CODE_PK=p.CODE_PK and pi.PROJECTS_CODE_PK=:ProjectCode');
            $stmt->execute(array(
                ':ProjectCode' => $projectcode
            ));
            $r = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
            return new EProject($r["CODE_PK"], $r["LABEL"], $r["DESCRIPTION"], intval($r["FORECASTED_PROJECT_HOURS"]), $r["FORECASTED_END_DATE"], $r["INTERNAL_PROJECT_KEY"], $r["START_DATE"], $r["END_DATE"]);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return false;
        }
        return false;
    }
    public static function getManagerProject($email) {
        try {
            $projects = array();
            $stmt = EDatabase::prepare('SELECT * FROM eetimechecking.projects p,eetimechecking.projects_has_managers pm,projects_infos pi WHERE p.CODE_PK = pm.PROJECTS_CODE_PK  and pi.PROJECTS_CODE_PK = p.CODE_PK and pm.USERS_EMAIL_PK =:USERS_EMAIL_PK;');
            $stmt->execute(array(
                ':USERS_EMAIL_PK' => $email
            ));
             while ($r = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $EProject = new EProject($r["CODE_PK"], $r["LABEL"], $r["DESCRIPTION"], $r["FORECASTED_PROJECT_HOURS"], $r["FORECASTED_END_DATE"], $r["INTERNAL_PROJECT_KEY"], $r["START_DATE"], $r["END_DATE"]);
            array_push($projects, $EProject);
             }
        } catch (PDOException $e) {
            return false;
        }
        return $projects;
    }
    
        /**
     * Fonction insérant un chef de projet / admin en tant que chef de projet
     * @param type $users   Tableau d'emails 
     * @param type $projectCode Code du projet
     */
    public static function setManagerOnProject($users, $projectCode) {
        
            $stmt = EDatabase::prepare("DELETE FROM `PROJECTS_HAS_MANAGERS` WHERE PROJECTS_CODE_PK=:p");
            $stmt->execute(array(
                "p" => $projectCode
            ));
        
        
        
        foreach ($users as $user) {
            $stmt = EDatabase::prepare("INSERT INTO `PROJECTS_HAS_MANAGERS` (USERS_EMAIL_PK, PROJECTS_CODE_PK) VALUES (:e,:p)");
            $stmt->execute(array(
                "e" => $user,
                "p" => $projectCode
            ));
        } {
            return true;
        }
        //si fail
        return false;
    }

}
