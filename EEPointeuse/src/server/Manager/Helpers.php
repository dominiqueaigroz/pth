<?php

//require_once './../inc.all.php';

/**
 * Retourne le nombre d'événements attendu pour une journée donnée,
 * et optionnellement pour un utilisateur en particulier
 * @param DateTime $day Le jour concerné
 * @param String $email (Optional) L'email d'un utilisateur en particulier
 *                                 ou vide si on veut l'information de
 *                                 manière générale. Défaut est vide.
 * @return int  Le nombre de pointage pour la journée donnée.
 *              ou FALSE si une erreur est rencontrée.
 */
function eventCountOnDay($day, $email = EWD_ALLUSERS) {
    // On doit tester si par hasard on a null
    if (!isset($email))
        $email = EWD_ALLUSERS;
    // Pour l'instant, on n'a pas implémenté cette fonction.
    // On va devoir aller chercher l'information pour savoir
    // le nombre de pointage attendu pour un jour et un utilisateur
    // donné
    //Test pour savoir si le jour sélectionner est un jour de vacance.
    $holidays = EHelperDay::GetHolidays();
    foreach ($holidays as $value) {
        if ($value->format('Y-m-d') == $day->format('Y-m-d')) {
            return 0;
        }
    }
    //Test pour savoir quel jours de le semaine nous sommes.
    $t = $day->getTimestamp();
    $try = intval(date("N", $t));
    $idx = EWD_INVALID;
    switch ($try) {
        case 1 : // Lundi
            $idx = EWD_MONDAY;
            break;
        case 2 : // Mardi
            $idx = EWD_TUESDAY;
            break;
        case 3 : // Mercredi
            $idx = EWD_WEDNESDAY;
            break;
        case 4 : // Jeudi
            $idx = EWD_THURSDAY;
            break;
        case 5 : // Vewndredi
            $idx = EWD_FRIDAY;
            break;
        case 6 : // Samedi
        case 7 : // Dimanche
        default:
            return 0;
    }
    $workingDay = EHelperDay::GetWorkingDays($email);
    if ($workingDay->isValid() == false) {
        $workingDay = EHelperDay::GetWorkingDays(EWD_ALLUSERS);
    }
    $workingMoment = $workingDay->Weekdays[$idx];
    if ($workingMoment == 1 || $workingMoment == 2) {
        return 2;
    }
    if ($workingMoment == 3) {
        return 4;
    }
    return 0;
}

/**
 * Cette fonction calcule le delta entre plusieurs datetime.
 * 
 * @param {array of DateTime} $arr Contient les enregistrements de type DateTime
 * @return DateInterval ou FALSE si une erreur s'est produite
 */
function getDeltaOnDate($arr) {
    echo "ATTENTION FONCTION OBSOLETE: Veuillez utiliser EDeltaManager::getTotalDeltaEvents().";
    /* @todo: à supprimer et adapter en fonction des settings */
    if (count($arr) != 2 && count($arr) != 4)
        return FALSE;

    $delta1 = $arr[0]->diff($arr[1], false);
    $targetTime = getStandardTimePerDay();
    if (count($arr) > 2) {
        $delta2 = $arr[2]->diff($arr[3], false);
        $delta3 = addDateIntervals($delta1, $delta2);
        return addDateIntervals($delta3, $targetTime);
    }

    return addDateIntervals($delta1, $targetTime);
}

/**
 * Retourne le total en secondes d'un objet date interval
 * @param DateTime $inter Une objet DateInterval valide
 * @return integer Le nombre de secondes
 */
function DateInterval2Seconds($inter) {
    return $inter->days * 86400 + $inter->h * 3600 + $inter->i * 60 + $inter->s;
}

/**
 * Retourne un objet DateInterval initialisé avec
 * le temps standard par jour.
 * @param {boolean} $inv (Optional) Inverser le temps ? Défaut est True.
 * @return DateInterval 
 */
function getStandardTimePerDay($inv = true) {
    $interval = new DateInterval("P0M");
    $interval->h = ECTS_HOURPERDAY;
    $interval->i = ECTS_MINUTEPERDAY;
    $interval->invert = $inv;
    return $interval;
}

/**
 * Retourne un objet DateInterval initialisé avec
 * le temps standard par demie-journée.
 * @param {boolean} $inv (Optional) Inverser le temps ? Défaut est True.
 * @return DateInterval 
 */
function getStandardTimePerHalfDay($inv = true) {
    // On initialize une date en 00 heures, 00 minutes, 00 secondes
    $im = new DateTime("00:00");
    // On clone notre temps initial
    $start = clone $im;
    // On récupère le nombre de secondes par jour et on divise par deux
    $secs = DateInterval2Seconds(getStandardTimePerDay($inv)) / 2;
    // On créé un date interval uniquement avec les secondes
    $interval = new DateInterval("PT" . $secs . "S");
    // Le date va additionner correctement le nombre de secondes.
    $im->add($interval);
    // On soustrait le résultat de l'addition au temps du départ
    $interval = $start->diff($im, false);
    $interval->invert = $inv;
    return $interval;
}

/**
 * Retourne un objet DateInterval initialisé avec
 * le temps standard par mois.
 * @param {boolean} $inv (Optional) Inverser le temps ? Défaut est True.
 * @return DateInterval 
 */
function getStandardTimePerMonth($inv = true) {
    $interval = new DateInterval("P0M");
    $interval->h = ECTS_HOURPEMONTH;
    $interval->invert = $inv;
    return $interval;
}

/**
 * Cette fonction additionne deux dates intervalle.
 * @param {DateInterval} $dateInt1  La première date intervalle
 * @param {DateInterval} $dateInt2  La seconde date intervalle
 * @return {DateInterval} Un object qui contient l'addition des deux DateInterval
 */
function addDateIntervals(DateInterval $dateInt1, DateInterval $dateInt2) {
    // On initialize une date en 00 heures, 00 minutes, 00 secondes
    $im = new DateTime("00:00");
    // On clone notre temps initial
    $start = clone $im;
    // On ajoute les deux DateInterval

    $im->add($dateInt1);

    $im->add($dateInt2);
    // On soustrait le résultat des additions au temps du départ
    // Aini on prend en compte la différence possible en terme de jours
    $diff = $start->diff($im, false);
    return $diff;
}

/*
 * Renvoie un numéro correspondant au numéro de la semaine de l'année
 * @param {string} $date Une date au format Y-m-d ('2017-11-30')
 * @return {int} Retourne un numéro correspondant au numéro de la semaine de l'année
 */

function getWeekNumber($date) {
    return date('W', strtotime($date));
}

/**
 * Inclue la barre de navigation en fonction du rôle de l'utilisateur
 * désigné par son email.
 * @param {string} $email L'email de l'utilisateur
 */
function getNavbar() {
    $session = ESession::getInstance();
    $role = ESession::getInstance()->getRole();
    switch ($role) {
        case 0:
            include __DIR__ . '/../../templates/navbarAdmin.php';
            break;

        case 1:
            include __DIR__ . '/../../templates/navbarProjectManager.php';
            break;

        case 2:
            include __DIR__ . '/../../templates/navbarEleve.php';
            break;
        case 3:
            if (_BYPASS_CHECKSESSION_ || defined('_BYPASS_CHECKSESSION_')) {
                include __DIR__ . '/../../templates/navbarAdmin.php';
                break;
            } else {
                echo "qui êtes-vous ?";
                break;
            }
    }
}

function checkRole() {
    if (ESession::getInstance()->getRole() > ROLE) {
        header('Location:' . getRootURL() . URL_INVALID_ROLE);
        return false;
    }
    return true;
}

function getHomePage($role) {
    switch ($role) {
        case EROLE_ADMIN:
            return getRootURL() . URL_HOME_ADMIN;
        case EROLE_PM:
            return getRootURL() . URL_HOME_PM;
        case EROLE_USER:
            return getRootURL() . URL_HOME_USER;
        case EROLE_UNKNOWN:
            break;
    }
    return getRootURL() . URL_HOME_UNKNOWN;
}

/**
 * Récupère le répertoire racine du site
 * @return string Le répertoire racine du site
 */
function getRootFolder() {
    $rootFolder = $_SERVER['DOCUMENT_ROOT'];
    $subFolder = get_cfg_var("ee_pointage_subdir");
    if ($subFolder !== FALSE && strlen($subFolder) > 0) {
        $rootFolder .= "/" . $subFolder;
    }
    return $rootFolder;
}

/**
 * Récupère l'url à partir du répertoire racine du site
 * @return string L'url à partir du racine du site
 */
function getRootURL() {
    $t = __DIR__;
    $subFolder = get_cfg_var("ee_pointage_subdir");
    if ($subFolder !== FALSE && strlen($subFolder) > 0) {
        return "/" . $subFolder;
    }
    return "";
}

/**
 * Encrypte un string
 * @param {string} $originalString
 * @return {string} Le code encrypté
 */
function EncryptString($originalString, $key) {
    $asciiKey = text2ascii($key);
    $asciiPlaintext = text2ascii($originalString);
    $keysize = count($asciiKey);
    $input_size = count($asciiPlaintext);
    $cipher = "";

    for ($i = 0; $i < $input_size; $i++) {
        $cipher .= chr($asciiPlaintext[$i] ^ $asciiKey[$i % $keysize]);
    }
    return $cipher;
}

/**
 * Décrypte un string encrypté
 * @param {string} $encryptedString
 * @return {string} Le code encrypté
 */
function DecryptString($encryptedString, $key) {

    $strLength = strlen($originalString);
    $keyLength = strlen($key);
    for ($i = 0; $i < $strLength; $i++) {
        for ($j = 0; $j < $keyLength; $j++) {
            //decrypt
            $string[$i] = $key[$j] ^ $string[$i];
        }
    }
    return $string;
}

/**
 * Convertit un string en hexadécimal
 * @param {string} Le string
 * @return {string} Le string en hexadécimal
 */
function strToHex($string) {
    $hex = '';
    for ($i = 0; $i < strlen($string); $i++) {
        $hex .= dechex(ord($string[$i]));
    }
    return $hex;
}

/**
 * Convertit de l'hexadécimal en string
 * @param {string} Le string en hexadécimal
 * @return {string} Le string 
 */
function hexToStr($hex) {
    $string = '';
    for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
        $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
    }
    return $string;
}

function text2ascii($text) {
    return array_map('ord', str_split($text));
}

function ascii2text($ascii) {
    $text = "";
    foreach ($ascii as $char)
        $text .= chr($char);
    return $text;
}
