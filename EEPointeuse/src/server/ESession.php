<?php
/*
 * API pour manager les sessions
 */
class ESession
{
    private static $instance = null;
    
    /**
     * @brief   Class Constructor - Créer une nouvelle instance de cette classe
     *          On la met en privé pour que personne puisse créer une nouvelle instance via ' = new ESession();'
     */
    private function __construct() {
        
    }

    /**
     * @brief   Comme le constructeur, on rend __clone privé pour que personne ne puisse cloner l'instance
     */
    private function __clone() {
        
    }
    
    /**
     * @brief   Retourne l'instance de la Database ou créer une connexion initiale
     * @return $objInstance;
     */
    public static function getInstance() {
        if (self::$instance === null) {
            try {

                self::$instance = new ESession();
            } catch (Exception $e) {
                echo "ESession Error: " . $e->getMessage();
            }
        }
        return self::$instance;
    }
    
    /**
     * Contrôle que la session est valide.
     * Si elle ne l'est pas, redirige vers l'url spécifiée
     * @param string $url (Optional) L'url à rediriger en cas de session non valide
     *                    Defaut URL_AUTENTIFICATION
     */
    public function check($url=URL_AUTENTIFICATION){
        if ($this->isValid() == FALSE){
            if (strlen($url)>0)
                header('Location: '.$url);
            return false;
        }
        return true;
    }
    
    /**
     * Détruit complètement la session, y compris les cookies.
     */
    public function destroy(){
        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();
    }
    /**
     * Test si la session est valide
     * @return boolean  True si la session est valide
     */
    public function isValid(){
        return ($this->getEmail() !== FALSE &&
                $this->getRole() !== FALSE);
    }
    /**
     * Permets de récupérer l'email dans les sessions
     * @return string/boolean Retourne string si l'email est set sinon retourne False
     */
    public function getEmail() {
        if(isset($_SESSION["Email"]))
        {
            return $_SESSION["Email"];
        }
        // Pas setté
        return FALSE;
    }
    
    /**
     * Permets de récupérer le role dans les sessions
     * @return string/boolean Retourne string si le role est set sinon retourne False
     */
    public function getRole()
    {
        if(isset($_SESSION["Role"]))
        {
            return $_SESSION["Role"];
        }
        return FALSE;
        
    }
    
    /**
     * Permets de récupérer le nom dans les sessions
     * @return string/boolean Retourne string si le nom est set sinon retourne False
     */
    public function getFullname()
    {
        if(isset($_SESSION["Fullname"]))
        {
            return $_SESSION["Fullname"];
        }
        return FALSE;
        
    }
    
    /**
     * Permets de récupérer le nom dans les sessions
     * @return string/boolean Retourne string si le nom est set sinon retourne False
     */
    public function getImage() {
        if(isset($_SESSION["Image"]))
            return $_SESSION["Image"];
        return FALSE;
    }
    /**
     * Initialize les informations de la session
     * @param string $newEmail L'email de l'utilisateur connecté
     * @param integer $newRole Son rôle
     * @param string $newFullname   (Optionnel) Le nom complet. Défaut est vide.
     * @param string $newImage  (Optionnel) Chemin de l'image
     */
    public function setInfo($newEmail,$newRole,$newFullname="", $newImage=""){
        $this->setEmail($newEmail);
        $this->setRole($newRole);
        $this->setFullname($newFullname);
        $this->setImage($newImage);
    }
    /**
     * Permet de changer l'email dans session
     * @param string $newEmail
     * @return boolean|\Exception
     */
    public function setEmail($newEmail) {
        try{
            $_SESSION["Email"] = $newEmail;
        } catch(Exception $e){
            echo "ESession Error: " . $e->getMessage();
            return FALSE;
        }
        return TRUE;
    }
    
    /**
     * Permet de changer le role dans session
     * @param integer $newRole
     * @return boolean|\Exception
     */
    public function setRole($newRole) {
        try{
            $_SESSION["Role"] = $newRole;
        } catch (Exception $e){
            echo "ESession Error: " . $e->getMessage();
            return FALSE;
        }
        return TRUE;
        
    }
    
    /**
     * Permet de changer le fullname dans session
     * @param string $newFullname
     * @return boolean|\Exception
     */
    public function setFullname($newFullname) {
        try{
            $_SESSION["Fullname"] = $newFullname;
        } catch (Exception $e) {
            echo "ESession Error: " . $e->getMessage();
            return FALSE;
        }
        return TRUE;
    }
    
    /**
     * Permet de changer l'image dans session
     * @param string $newImage
     * @return boolean\Exception
     */
    public function setImage($newImage) {
        try {
            $_SESSION["Image"] = $newImage;
        } catch (Exception $e) {
            echo 'ESession Error: ' . $e->getMessage();
            return FALSE;
        }
    }
}