<?php

/**
 * Description of ESearchResult
 * Cette classe sert de structure pour le 
 * résultats de la recherche
 * 
 * @author BURNANDL
 */
class ESearchResult {
    
    /** @var string email de la personne retournée **/
    public $email;
    
    /** 
     * @var integer Le rôle de la personne :
     *          - EROLE_ADMIN   
     *          - EROLE_PM
     *          - EROLE_USER
     *          - EROLE_UNKNOWNED
     */
    public $role;
    
    /** @var string retourne le nom du projet au moment de l'évenement **/
    public $project;
    
    /** @var string contient la date et l'heure de l'évenement sous la forme YYYY-MM-DD HH:MM:SS */
    public $date;




    /**
     * Constructor
     * @param string $InEmail   Contient l'email. Defaut est vide.
     * @param string $InProject Contient le nom du projet. Defaut est vide.
     * @param string $InDate    Contient une date. sous la forme YYYY-MM-DD HH:MM:SS. Defaut est vide.
     * @param integer $InRole   Contient le type de role. Defaut est EROLE_UNKNOWNED.
     */
    public function __construct($InEmail = "", $InProject = "", $InDate = "", $InRole = EROLE_UNKNOWNED)
    {
        $email = $InEmail;
        $role = $InRole;
        $project = $InProject;
        $date = $InDate;
    }
}
