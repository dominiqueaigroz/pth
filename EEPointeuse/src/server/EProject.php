<?php

/**
 * Cette classe contient les informations
 * sur les projets
 * 
 * 
 */
class EProject {

    /** @var string Le code du projet */
    public $code;

    /** @var string Le label du projet */
    public $label;

    /** @var string La description du projet */
    public $description;

    /** @var datetime Le nombre d'heures prévu que doivent passer les workers sur le projet */
    public $forecastedHour;

    /** @var datetime La date à laquelle le projet est sensée se terminer */
    public $forecastedEndDate;

    /** @var string La clé interne au projet lord de l'authentification */
    public $internal_project_key;

    /** @var string La date à laquelle le projet a débuté */
    public $start_date;

    /** @var string La date à laquelle le projet s'est terminé */
    public $end_date;

    /**
     * Constructeur par défaut
     * @param DateTime $code    Le code du projet
     * @param DateInterval $label Le label du projet
     * @param DateTime $description    La description du projet 
     * @param DateInterval $forecastedHour Le nombre d'heures prévu que doivent passer les workers sur le projet
     * @param DateTime $forecastedEndDate    La date à laquelle le projet est sensée se terminer
     * @param DateInterval $internal_project_key La clé interne au projet lord de l'authentification
     * @param DateTime $start_date    La date à laquelle le projet a débuté 
     * @param DateInterval $end_date La date à laquelle le projet s'est terminé
     */
    public function __construct($code,$label,$description,$forecastedHour,$forecastedEndDate,$internal_project_key, $start_date, $end_date) {
        $this->code = $code;
        $this->label = $label;
        $this->description = $description;
        $this->forecastedHour = $forecastedHour;
        $this->forecastedEndDate = $forecastedEndDate;
        $this->internal_project_key = $internal_project_key;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
    }

}
