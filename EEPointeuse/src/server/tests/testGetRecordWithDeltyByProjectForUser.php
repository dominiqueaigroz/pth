<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/server/inc.all.php';

$email = "jonathan.brljq@eduge.ch";
//$email = "gawen.ackrm@eduge.ch";
$projectCode = "XGEN02ZKP6789DBL";


$result = EDeltaManager::getRecordWithDeltaByProjectForUser($projectCode, $email);

if ($result[0] === FALSE) {
    echo "problème dans les pointages";
} else {
    foreach ($result as $key) {
        echo $key->format("%h HEURES %i MINUTES %s SECONDES " . "POUR LE PROJET $projectCode");
    }
}

