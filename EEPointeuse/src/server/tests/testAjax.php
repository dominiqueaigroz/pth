<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/server/inc.all.php';
 
// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');
$id = "";
// Je récupère l'id de guidance
if (isset($_REQUEST['id']))
    $id = $_REQUEST['id'];
 
if (strlen($id) > 0){
    // Si j'arrive ici, ouf... c'est tout bon
    echo '{ "ReturnCode": 0, "Data": "ok"}';
    exit();
     
}
 
// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Il manque le paramètre Message"}';
