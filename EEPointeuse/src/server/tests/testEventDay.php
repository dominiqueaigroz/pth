<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/server/inc.all.php';

$from = "2018-01-12"; //à récuprérer du calendrier
$to = "2018-01-12";   //à récuprérer du calendrier
//faire fonction recup email + tous les pointages du jour et on peut y naviguer à l'aide du calendrier

$result = EEventManager::getRecordEvents(ECTS_USERTIMESHEET_PROJECTCODE, null, $from, $to, TRUE);
$previousEmail = "";
$arrEvent = array();

foreach ($result as $r) {
    $b = $r->dt->format("j-m-y");

    $projEvent = new EProjectEvent();
    $projEvent->deleted = 0;
    $projEvent->dt = $r->dt;
    $projEvent->email = $r->email;

    array_push($arrEvent, $projEvent);
}
$eventStr = "";
$arrStudent = array();
$strOutput = "";
foreach ($arrEvent as $a) {
    while ($a->email != $previousEmail) {
        $b = $a->dt;
        $strOutput.= $a->email . "──||──";
        break;
    }
    $s = $a->dt;
    $strOutput .= $s->format("h:i:s") . " | ";
    if (end($arrEvent))
        $strOutput .= "<br>";

    $previousEmail = $a->email;
}
echo $strOutput;