<?php
/**
 * Classe répertoriant l'id du Rôle ainsi que son Label
 *
 * @author ACKERMANNG
 */
class ERole {
     /** @var int L'id du rôle */
    public $idRole;
    /** @var string Le nom du rôle */
    public $labelRole;
    /** @var string L'email de l'utilisateur */
    public $email;
}
