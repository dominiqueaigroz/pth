<?php

// On ne check pas la session si _SERVER_ ou _BYPASS_CHECKSESSION_ est défini et TRUE
if ((defined("_SERVER_") == FALSE || _SERVER_ == FALSE) && (defined("_BYPASS_CHECKSESSION_") == FALSE || _BYPASS_CHECKSESSION_ == FALSE)) {
    // On contrôle le rôle de l'utilisateur en fonction du rôle défini dans la page
    if (ESession::getInstance()->getRole() > ROLE) {
        header('Location:' . getRootURL() . URL_INVALID_ROLE);
        exit();
    }
}
?>