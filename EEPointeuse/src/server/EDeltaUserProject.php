<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Cette classe contient les informations
 * de l'utilisateur
 * du code de projet
 * tableau de EDeltaDate
 * sur le delta à une date donnée
 * le mois du pointage
 * 
 * @remark Première étape est de faire un objet
 *         qui contient les données uniquement.
 *         Pas de méthodes pour accéder ou manipuler
 *         l'objet.
 */
class EDeltaUserProject{
    
    /** @var string L'email de l'utilisateur */
    public $email;
    
    /** @var string Le code du projet. */
    public $projectCode;

    /** @var EDeltaDate Tableau de EDeltaDate */
    public $deltas;
    
    /** @var EDeltaDate Mois du delta */
    public $date;
    
}