<?php
require_once './server/inc.all.php';

/** @remark On doit être au minimum Project Manager pour visualiser cette page */
define('ROLE', EROLE_PM);
require_once './server/check.role.php';

$email = (isset($_GET['email'])) ? filter_input(INPUT_GET, 'email') : null;
$projectCode = (isset($_GET['projectCode'])) ? filter_input(INPUT_GET, 'projectCode') : null;
$to = (isset($_GET['to'])) ? filter_input(INPUT_GET, 'to') : null;
$from = (isset($_GET['from'])) ? filter_input(INPUT_GET, 'from') : null;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Consultation</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    </head>
    <body>
        <div id="infoMsg"></div>
        <?php
        getNavbar();
        ?> 
        <div class="main">
            <h1 class="titlePage">Administration</h1>

            <h1 class="titleTable">Gestionnaire d'erreur</h1>
            <table class="mainTable" >
                <thead>
                    <tr>
                        <td>Email</td> 
                        <td>Nombre de pointage</td> 
                        <td>Nombre de pointage attendus</td>
                        <td>Date</td> 
                        <td></td>
                    </tr>
                </thead>
                <tbody class="tableBody">
                    <?php
                    // J'ai un tableau qui contient les enregistrements en fonction des paramètres
                    $arr = EEventManager::getRecordEvents($projectCode, $email, $to, $from);
                    $prevDate = '';
                    $arrEvents = array();
                    $cpt = 0;

                    foreach ($arr as $event) {
                        // On construit une chaîne avec uniquement la date
                        // pour détecter le changement de jour
                        $dateStr = $event->dt->format('Y-m-d');
                        // Detection que la date a changé
                        if (strlen($prevDate) > 0 &&
                                $prevDate != $dateStr &&
                                count($arrEvents) > 0) {
                            // récupère le nombre de pointage attendu
                            $attended = eventCountOnDay($arrEvents[0]->dt, $event->email);
                            if ($attended != count($arrEvents)) {
                                // Ces enregistrement sont en anomalies
                                $class = ($cpt % 2) ? " even" : " odd";
                                echo '<tr class="row' . $class . '" id="' . $cpt . '">';
                                echo '<td>' . $event->email . '</td>';
                                echo '<td><span class="evtcounter">' . count($arrEvents) . '</span><span class="arrow down"></span>';
                                foreach ($arrEvents as $evt) {
                                    echo '<ul style="display:none;" id="subid_' . $cpt . '"><li class="subitem"><span>' . $evt->dt->format('H:i:s') . '</span><input type="button" data-projcode="' . $evt->projectCode . '" data-email="' . $evt->email . '" data-dt="' . $evt->dt->format('Y-m-d H:i:s') . '"class="trash"></li></ul>';
                                }
                                echo '</td>';
                                echo '<td>' . $attended . '</td>';
                                echo '<td>' . $arrEvents[0]->dt->format('Y-m-d') . '</td>';
                                echo '<td class="prevent tooltip"><span class="tooltiptext">Prévenir l\'élève par mail</span></td>';
                                //echo '<td><span class="prevent"></span></td>';
                                echo '</tr>';
                                /*
                                  foreach ($arrEvents as $evt) {
                                  echo '<tr style="display:none;" class="subrow" id="subid_' . $cpt . '">';
                                  echo '<td colspan="4">' . $evt->dt->format('H:i:s') . '</td>';
                                  echo '<td><input type="button" data-projcode="' . $evt->projectCode . '" data-email="' . $evt->email . '" data-dt="' . $evt->dt->format('Y-m-d h:i:s') . '" class="deleteButton" value="supprimer"></td></tr>';
                                  }
                                 */
                                $cpt += 1;
                            } // Endif: nombre d'événements attendus différents
                            // On réinitialise notre tableau de dates
                            $arrEvents = array();
                        } // Endif: Quand différent jour
                        $prevDate = $dateStr;
                        // On mets dans le tableau
                        array_push($arrEvents, $event);
                    }
                    ?>
                </tbody>
                <tfoot class="tableFooter">

                </tfoot>
            </table>
        </div>
        <script>
            var requestedItem = null;

            $('.mainTable tr td span.arrow').click(function (e) {
                selectRow($(this).parent().parent().attr('id'));
                if ($(this).hasClass("up")) {
                    $(this).removeClass("up").addClass("down");
                } else {
                    $(this).removeClass("down").addClass("up");
                }
            });
            function selectRow(i) {
                $('ul#subid_' + i).toggle();
            }

            $('input.trash').click(function (e) {
                // On conserver l'élément bouton sur lequel
                // on a appuyé. Après la réponse, on va supprimer
                // la ligne appartenant au bouton.
                requestedItem = $(this);
                var projectCode = $(this).data('projcode');
                var evEmail = $(this).data('email');
                var evDateTime = $(this).data('dt');

                $.ajax({
                    method: 'POST',
                    url: 'server/ajax/deluserevent.php',
                    // Passe en paramètre l'utilisateur connecté et
                    // le code projet pour le pointage standard des personnes
                    data: {'user': evEmail, 'projCode': projectCode, 'dt': evDateTime},
                    dataType: 'json',
                    success: function (data) {
                        var msg = '';

                        switch (data.ReturnCode) {
                            case 0 : // tout bon
                                if (requestedItem !== null) {
                                    if (hideEvent(requestedItem, data.AttendedCount, data.EventCount))
                                        requestedItem = null;

                                    else
                                    {
                                        $("#infoMsg").html("Impossible de supprimer visuellement. Recharger la page.");
                                    }
                                }
                                break;
                            case 1 : // problème de paramètre
                            case 2 : // problème d'insertion
                            default:
                                msg = data.Message;
                                break;
                        }
                        emailSent = "";

                        if (msg.length > 0)
                            $("#infoMsg").html(msg);
                    },
                    error: function (jqXHR) {
                        var msg = '';
                        switch (jqXHR.status) {
                            case 404 :
                                msg = "page pas trouvée. 404";
                                break;
                            case 200 :
                                msg = "probleme avec json. 200";
                                msg += "</BR>réponse: " + jqXHR.responseText;
                                break;

                        } // End switch
                        emailSent = "";

                        if (msg.length > 0)
                            $("#infoMsg").html(msg);
                    }
                });

            }); // End of click on delete button

            /**
             * Supprimer l'événement pointé par le item (le bouton supprimer)
             * @param {type} item   L'élément html button pour lequel on doit supprimer la ligne
             * @param {integer} att   Le nombre d'événements attendus
             * @param {integer} c   Le nombre d'événements réels
             * @returns {boolean} True si ok.
             */
            function hideEvent(item, att, c) {
                if (item.parent() !== null && item.parent().parent() !== null) {
                    // Retrouver le owner de l'élément bouton
                    // Normalement un <ul>
                    var p = item.parent().parent();
                    // Retrouver le owner pour la personne
                    var owner = $(p).parent().parent();
                    var r = confirm("Voulez-vous supprimer cet évènement ?");
                    // On va tester que le nombre d'événements attendus est différent
                    // du nombre réel.
                    if(r === true){
                    if (att !== c) {
                        // Retrouver le owner de cet élément pour changer le counter
                        owner.find(".evtcounter").html(c);
                        p.remove();
                    } else { // on a le nombre attendu, on supprimer toutes la section de la personne
                        owner.remove();
                        confirm("Press a button!");
                    }
                }
                    return true;
                }
                // Fail
                return false;
            }


        </script>
    </body>
</html>

