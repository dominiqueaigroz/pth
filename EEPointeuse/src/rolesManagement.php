<?php
require_once './server/inc.all.php';
/** @remark On doit être au minimum Admin pour visualiser cette page */
define('ROLE', EROLE_ADMIN);
require_once './server/check.role.php';
$allRoles = EUserManager::getAllRoles();
$a = EUserManager::getAllUserEmails();
$arrUserRoles = array();
foreach ($a as $v) {
    $roleUser = EUserManager::getRole($v);
    array_push($arrUserRoles, $roleUser);
}
$strOutput = "";
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Gestion Rôles</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/CSS_sample.css" rel="stylesheet" type="text/css"/>
        <link href="css/styleNavBar.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://apis.google.com/js/platform.js"></script>        
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    </head>
    <body>
                    <?php
            getNavBar();
            ?>
        <div class="main">
            <div id="infoMsg"></div>
            <h1 class="titlePage">Administration</h1>

            <div id="from"></div>
            <div id="head">
                <h1 class="titleTable">Gestion Rôles</h1>
            </div>
            <table class="mainTable gestionRole">
                <thead>
                    <tr>
                        <th>Email</th> 
                        <th>Rôle</th>
                    </tr>
                </thead>
                <tbody class="tableBody">
                    <tr>
                        <?php
                        foreach ($arrUserRoles as $r):
                            echo '<td align="center">';
                            echo '<span class="email" data-email="' . $r[0]->email . '">'. $r[0]->email . '</span>';
                            echo "</td>";
                            echo "<td>";
                            ?>

                    <select class="selectRoles">
                        <?php
                        foreach ($r as $d) {

                            foreach ($allRoles as $a) {
                                if ($d->idRole !== $a->idRole) {
                                    $strOutput = '<option value="' . $a->idRole . '">' . $a->labelRole . '</option>';
                                } else {
                                    $strOutput = '<option value="' . $d->idRole . '" selected="selected">' . $d->labelRole . '</option>';
                                }
                                echo $strOutput;
                            }
                        }
                        ?>
                    </select>
                    <?php
                    echo "</td>";
                    echo '</tr>';
                    echo '</div>';
                endforeach;
                ?>

                </tbody>
                <tfoot class="tableFooter">
                </tfoot>
            </table>
        </div>
    </body>
</html>
<script>
    var inProcess = false;
    $(document).ready(function () {
        /**
         * récupère tous les rôles
         * @returns {array} tableau de rôles
         */
        function setRole4User(e, r) {
            $.ajax({
                method: 'POST',
                url: 'server/ajax/setroleforuser.php',
                data: {"email": e, "idRole": r},
                success: function (d) {
                    // Reset du flag comme quoi on a envoyé la requête
                    var msg = '';
                    switch (d.ReturnCode) {
                        case 0 : // tout bon

                            break;
                    }
                    /*     case 1 : // problème de paramètre
                     case 2 : // problème d'insertion
                     default:
                     msg = d.Message;
                     break;
                     if (msg.length > 0)
                     $("#infoMsg").html(msg); */
                },
                error: function (jqXHR) {
                    // Reset du flag comme quoi on a envoyé la requête
                    inProcess = false;
                    var msg = '';
                    switch (jqXHR.status) {
                        case 404 :
                            msg = "page pas trouvée. 404";
                            break;
                        case 200 :
                            msg = "probleme avec json. 200";
                            msg += "</BR>réponse: " + jqXHR.responseText;
                            break;
                    } // End switch
                    if (msg.length > 0)
                        $("#infoMsg").html(msg);
                }
            });

            // Done
            //return res;
        }
        $('select').on('change', function () {
            var email = $(this).parent().parent().find("td span.email").data("email");
            var idRole = parseInt($(this).val());
            setRole4User(email, idRole);
        });

    });//fin document ready

</script>