



DELETE FROM `eetimechecking`.`roles`;
INSERT INTO `eetimechecking`.`roles` (`CODE_PK`, `LABEL`) VALUES ('0', 'Administrateur');
INSERT INTO `eetimechecking`.`roles` (`CODE_PK`, `LABEL`) VALUES ('1', 'Responsable de projet');
INSERT INTO `eetimechecking`.`roles` (`CODE_PK`, `LABEL`) VALUES ('2', 'Élève');
INSERT INTO `eetimechecking`.`roles` (`CODE_PK`, `LABEL`) VALUES ('3', 'Unknown');


DELETE FROM `eetimechecking`.`projects`;
INSERT INTO `eetimechecking`.`projects` (`CODE_PK`, `LABEL`, `DESCRIPTION`) VALUES ('XGEN02ZKP6789DBL', 'Relevé d\'heure', 'Ce projet répertorie les pointages de toutes les personnes.');
