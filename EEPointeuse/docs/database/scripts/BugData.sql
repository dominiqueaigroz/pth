﻿DELETE FROM  `eetimechecking`.`BUG_REPORT`;

DELETE FROM `eetimechecking`.`BUG_TYPE`;
INSERT INTO `eetimechecking`.`BUG_TYPE` (`ID_BUG_TYPE`, `LABEL`) VALUES ('1', 'Empèche de travailler');
INSERT INTO `eetimechecking`.`BUG_TYPE` (`ID_BUG_TYPE`, `LABEL`) VALUES ('2', 'Cosmétique');
INSERT INTO `eetimechecking`.`BUG_TYPE` (`ID_BUG_TYPE`, `LABEL`) VALUES ('3', 'Résultat faux');

DELETE FROM `eetimechecking`.`BUG_STATE`;
INSERT INTO `eetimechecking`.`BUG_STATE` (`LABEL`) VALUES ('Open');
INSERT INTO `eetimechecking`.`BUG_STATE` (`LABEL`) VALUES ('Solved');

